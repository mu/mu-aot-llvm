use crate::aot_mu::make_boot_image_internal;
use libloading as ll;
use mu::ast::ir::{Arc, MuID, MuName};
use mu::ast::types::{
    HybridTagMap, StructTag, StructTagMap, StructType_, HYBRID_TAG_MAP,
    HYBRID_TAG_MAP_LOC, STRUCT_TAG_MAP, STRUCT_TAG_MAP_LOC
};
use mu::vm::api::api_c::*;
use mu::vm::api::{from_CMuCtx, from_CMuVM};
use mu::vm::handle::APIHandle;
use mu::vm::VM;
use std::collections::HashMap;
use std::ffi::CStr;
use std::ops::Deref;
use std::os::unix::process::ExitStatusExt;
use std::path::PathBuf;
use std::process::Command;
use std::process::Output;
use std::slice;
use std::sync::atomic::{AtomicPtr, Ordering};
use std::sync::RwLock;
use std::{env, ptr};

#[no_mangle]
pub unsafe extern "C" fn compile_to_llvm_sharedlib(
    mvm: *mut CMuVM,
    lib_name: CMuCString,
    extra_srcs: *mut CMuCString,
    n_extra_srcs: CMuArraySize
) {
    let vm = from_CMuVM(mvm);

    let mut _arg_lib_name = from_mucstring(lib_name);
    let mut _arg_extra_srcs = from_mucstring_array(extra_srcs, n_extra_srcs);

    let mut funcs: Vec<MuID> = {
        let funcs = vm.funcs().read().unwrap();
        funcs.keys().map(|x| *x).collect()
    };

    funcs.sort();

    make_boot_image_internal(
        &vm,
        funcs,
        None,
        None,
        None,
        vec![],
        vec![],
        vec![],
        vec![],
        _arg_extra_srcs,
        _arg_lib_name,
        false
    );
}

#[no_mangle]
pub unsafe extern "C" fn compile_to_llvm_sharedlib_using_context(
    mvm: *mut CMuVM,
    lib_name: CMuCString,
    extra_srcs: *mut CMuCString,
    n_extra_srcs: CMuArraySize,
    context: CMuCString
) {
    let vm = from_CMuVM(mvm);
    let context = from_mucstring(context);

    // loading STRUCT_TAG_MAP and HYBRID_TAG_MAP from the context
    let dylib = load_from_context(context);

    let lib: ll::Library = std::mem::transmute(
        ll::os::unix::Library::open(
            Some(dylib.as_os_str()),
            libc::RTLD_NOW | libc::RTLD_GLOBAL
        )
        .unwrap()
    );

    let stm: ll::Symbol<StructTagMap> =
        lib.get("STRUCT_TAG_MAP".as_bytes()).unwrap();
    let stm_loaded = stm.load(Ordering::SeqCst);
    STRUCT_TAG_MAP_LOC.swap(stm_loaded, Ordering::SeqCst);

    let htm: ll::Symbol<HybridTagMap> =
        lib.get("HYBRID_TAG_MAP".as_bytes()).unwrap();
    let htm_loaded = htm.load(Ordering::SeqCst);
    HYBRID_TAG_MAP_LOC.swap(htm_loaded, Ordering::SeqCst);

    let vm = VM::resume_loaded_vm(vm);

    let mut _arg_lib_name = from_mucstring(lib_name);
    let mut _arg_extra_srcs = from_mucstring_array(extra_srcs, n_extra_srcs);

    let mut funcs: Vec<MuID> = {
        let funcs = vm.funcs().read().unwrap();
        funcs.keys().map(|x| *x).collect()
    };

    funcs.sort();

    make_boot_image_internal(
        &vm,
        funcs,
        None,
        None,
        None,
        vec![],
        vec![],
        vec![],
        vec![],
        _arg_extra_srcs,
        _arg_lib_name,
        false
    );
}

#[no_mangle]
pub unsafe extern "C" fn compile_to_llvm_sharedlib_with_primordial(
    mvm: *mut CMuVM,
    lib_name: CMuCString,
    extra_srcs: *mut CMuCString,
    n_extra_srcs: CMuArraySize,
    context: CMuCString,
    primordial: CMuValue
) {
    let vm = from_CMuVM(mvm);
    let context = from_mucstring(context);
    let primordial = from_handle_optional(primordial);

    // loading STRUCT_TAG_MAP and HYBRID_TAG_MAP from the context
    let dylib = load_from_context(context);

    let lib: ll::Library = std::mem::transmute(
        ll::os::unix::Library::open(
            Some(dylib.as_os_str()),
            libc::RTLD_NOW | libc::RTLD_GLOBAL
        )
        .unwrap()
    );

    let stm: ll::Symbol<StructTagMap> =
        lib.get("STRUCT_TAG_MAP".as_bytes()).unwrap();
    let stm_loaded = stm.load(Ordering::SeqCst);
    STRUCT_TAG_MAP_LOC.swap(stm_loaded, Ordering::SeqCst);

    let htm: ll::Symbol<HybridTagMap> =
        lib.get("HYBRID_TAG_MAP".as_bytes()).unwrap();
    let htm_loaded = htm.load(Ordering::SeqCst);
    HYBRID_TAG_MAP_LOC.swap(htm_loaded, Ordering::SeqCst);

    let vm = VM::resume_loaded_vm(vm);

    let mut _arg_lib_name = from_mucstring(lib_name);
    let mut _arg_extra_srcs = from_mucstring_array(extra_srcs, n_extra_srcs);

    let mut funcs: Vec<MuID> = {
        let funcs = vm.funcs().read().unwrap();
        funcs.keys().map(|x| *x).collect()
    };

    funcs.sort();

    make_boot_image_internal(
        &vm,
        funcs,
        primordial,
        None,
        None,
        vec![],
        vec![],
        vec![],
        vec![],
        _arg_extra_srcs,
        _arg_lib_name,
        false
    );
}

#[no_mangle]
pub unsafe extern "C" fn make_boot_image_from_ctx(
    ctx: *mut CMuCtx,
    whitelist: *mut CMuID,
    whitelist_sz: CMuArraySize,
    primordial_func: CMuFuncRefValue,
    primordial_stack: CMuStackRefValue,
    primordial_threadlocal: CMuRefValue,
    sym_fields: *mut CMuIRefValue,
    sym_strings: *mut CMuCString,
    nsyms: CMuArraySize,
    reloc_fields: *mut CMuIRefValue,
    reloc_strings: *mut CMuCString,
    nrelocs: CMuArraySize,
    output_file: CMuCString
) {
    let vm = from_CMuCtx(ctx);

    let stm = &vm.struct_tag_map;
    let htm = &vm.hybrid_tag_map;

    for (key, val) in stm.read().unwrap().iter() {
        let key = format!("{}", key);
        STRUCT_TAG_MAP
            .write()
            .unwrap()
            .insert(Arc::new(key), (*val).clone());
    }

    for (key, val) in htm.read().unwrap().iter() {
        let key = format!("{}", key);
        HYBRID_TAG_MAP
            .write()
            .unwrap()
            .insert(Arc::new(key), (*val).clone());
    }

    //    let vm = VM::resume_loaded_vm(vm);

    let mut _arg_output_file = from_MuCString(output_file);
    let mut _arg_whitelist = from_MuID_array(whitelist, whitelist_sz);
    let mut _arg_primordial_func = from_handle_optional(primordial_func);
    let mut _arg_primordial_stack = from_handle_optional(primordial_stack);
    let mut _arg_primordial_threadlocal =
        from_handle_optional(primordial_threadlocal);
    let mut _arg_sym_fields = from_handle_array(sym_fields, nsyms);
    let mut _arg_sym_strings = from_MuName_array(sym_strings, nsyms);
    let mut _arg_reloc_fields = from_handle_array(reloc_fields, nrelocs);
    let mut _arg_reloc_strings = from_MuName_array(reloc_strings, nrelocs);

    make_boot_image_internal(
        &vm,
        _arg_whitelist,
        _arg_primordial_func,
        _arg_primordial_stack,
        _arg_primordial_threadlocal,
        _arg_sym_fields,
        _arg_sym_strings,
        _arg_reloc_fields,
        _arg_reloc_strings,
        vec![],
        _arg_output_file,
        false
    );
}

#[inline(always)]
fn from_mucstring(cstring: CMuCString) -> String {
    debug_assert!(!cstring.is_null());
    let ffi_cstr = unsafe { CStr::from_ptr(cstring) };

    ffi_cstr.to_string_lossy().into_owned()
}

#[inline(always)]
fn from_mucstring_array<'a>(ptr: *const CMuCString, len: usize) -> Vec<String> {
    let slc = from_array_direct(ptr, len);
    slc.iter().map(|&e| from_mucstring(e)).collect::<Vec<_>>()
}

/// MuID is usize in this impl. Need conversion.
#[allow(non_snake_case)]
#[inline(always)]
fn from_MuID_array<'a>(ptr: *const CMuID, len: usize) -> Vec<MuID> {
    let slc = from_array_direct(ptr, len);
    slc.iter()
        .map(|&e| {
            debug_assert!(e != 0);
            e as MuID
        })
        .collect::<Vec<_>>()
}

#[inline(always)]
fn from_array_direct<'a, T>(ptr: *const T, len: usize) -> &'a [T] {
    if ptr.is_null() {
        unsafe { slice::from_raw_parts(ptr::null(), len) }
    } else {
        unsafe { slice::from_raw_parts(ptr, len) }
    }
}

#[inline(always)]
fn from_handle_optional<'a>(cmuvalue: CMuValue) -> Option<&'a APIHandle> {
    if cmuvalue.is_null() {
        None
    } else {
        Some(from_handle(cmuvalue))
    }
}

// APIHandle is immutable when used.
#[inline(always)]
fn from_handle<'a>(cmuvalue: CMuValue) -> &'a APIHandle {
    debug_assert!(!cmuvalue.is_null());
    unsafe { &*(cmuvalue as *const APIHandle) }
}

/// Convert into a Vec of handle refs. It is not certain whether refs are
/// represented in the same way as raw pointers. So this function will convert
/// each element. This function is only called by `new_thread_nor`. As always,
/// thread creation dominates the time.
#[inline(always)]
fn from_handle_array<'a>(
    ptr: *const CMuValue,
    len: usize
) -> Vec<&'a APIHandle> {
    let slc = from_array_direct(ptr, len);
    slc.iter()
        .map(|&e| {
            debug_assert!(!e.is_null());
            unsafe { &*(e as *const APIHandle) }
        })
        .collect::<Vec<_>>()
}

#[allow(non_snake_case)]
#[inline(always)]
fn from_MuName_array<'a>(ptr: *const CMuName, len: usize) -> Vec<MuName> {
    let slc = from_array_direct(ptr, len);
    slc.iter().map(|&e| from_MuName(e)).collect::<Vec<_>>()
}

#[allow(non_snake_case)]
#[inline(always)]
fn from_MuName(cname: CMuName) -> MuName {
    Arc::new(from_MuCString(cname))
}

#[allow(non_snake_case)]
#[inline(always)]
fn from_MuCString(cstring: CMuCString) -> String {
    debug_assert!(!cstring.is_null());
    let ffi_cstr = unsafe { CStr::from_ptr(cstring) };

    ffi_cstr.to_string_lossy().into_owned()
}

unsafe fn load_from_context(context: String) -> PathBuf {
    let mu_aot_llvm_path = match env::var("MU_LLVM_ZEBU") {
        Ok(v) => {
            trace!("MU_LLVM_ZEBU is set to: -{}-", v);
            let ret = PathBuf::from(v.clone());
            ret
        }
        Err(e) => panic!("Could not find variable 'MU_LLVM_ZEBU'")
    };

    let dylib = mu_aot_llvm_path.join(PathBuf::from(context));

    compile_context_into_object(dylib.clone());

    let dylib = compile_context_into_shared_lib(dylib);

    dylib
}

fn compile_context_into_object(file: PathBuf) -> PathBuf {
    let mut cc = Command::new(get_c_compiler());

    // position independent code
    cc.arg("-c");
    cc.arg("-fPIC");

    let mut out = file.clone();
    let mut in_file = file.clone();
    in_file.set_extension("S");
    out.set_extension("o");

    cc.arg(in_file.as_os_str());
    cc.arg("-o");
    cc.arg(out.as_os_str());

    exec_cmd(cc);

    out
}

fn compile_context_into_shared_lib(file: PathBuf) -> PathBuf {
    let mut cc = Command::new(get_c_compiler());

    // position independent code
    cc.arg("-fPIC");
    cc.arg("-shared");
    cc.arg("-Wl");

    let mut out = file.clone();
    if cfg!(target_os = "linux") {
        // just warn about unresolved symbols
        cc.arg("-Wl,--warn-unresolved-symbols");
        // necessary for handling exceptions with LLVM
        cc.arg("-lstdc++");
        out.set_extension("so");
    } else if cfg!(target_os = "macos") {
        cc.arg("-undefined");
        // allow dynamic lookup symbols
        cc.arg("dynamic_lookup");
        out.set_extension("dylib");
    }

    cc.arg("-rdynamic");

    cc.arg(format!(
        "{}",
        get_path_under_zebu(if cfg!(debug_assertions) {
            "target/debug/deps/libmu.a"
        } else {
            "target/release/deps/libmu.a"
        })
        .to_str()
        .unwrap()
    ));
    //    cc.arg("-lmu");

    cc.arg(file.as_os_str());
    cc.arg("-o");
    cc.arg(out.as_os_str());

    exec_cmd(cc);

    out
}

fn get_path_under_zebu(str: &'static str) -> PathBuf {
    use std::env;

    match env::var("MU_ZEBU") {
        Ok(v) => {
            trace!("MU_ZEBU is set to: -{}-", v);
            let mut ret = PathBuf::from(v.clone());
            ret.push(str);
            ret
        }
        Err(e) => {
            trace!("MU_ZEBU error: -{}-", e);
            PathBuf::from(str)
        }
    }
}

fn get_c_compiler() -> String {
    use std::env;

    match env::var("CC") {
        Ok(val) => val,
        Err(_) => "clang".to_string()
    }
}

/// executes the given command, checks its exit status,
/// panics if this command does not finish normally
pub fn exec_cmd(cmd: Command) -> Output {
    let output = exec_cmd_nocheck(cmd);
    if !output.status.success() {
        dbg!(&output);
        info!(
            "Failure: {}",
            String::from_utf8(output.stderr.clone()).unwrap()
        );
    }
    assert!(output.status.success());
    output
}

/// executes the given command, does not check exit status
fn exec_cmd_nocheck(mut cmd: Command) -> Output {
    info!("executing: {:?}", cmd);
    let output = match cmd.output() {
        Ok(res) => res,
        Err(e) => panic!("failed to execute: {}", e)
    };

    info!("---out---");
    info!("{}", String::from_utf8_lossy(&output.stdout));
    info!("---err---");
    info!("{}", String::from_utf8_lossy(&output.stderr));

    if output.status.signal().is_some() {
        info!(
            "terminated by a signal: {}",
            output.status.signal().unwrap()
        );
    }

    output
}
