// Copyright 2017 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

use crate::llvm_backend::*;
use mu::ast::inst::*;
use mu::ast::ir::*;
use mu::ast::op::*;
use mu::ast::ptr::*;
use mu::ast::types::*;

use llvm_sys::core::*;
use llvm_sys::prelude::*;
use llvm_sys::target::*;
use llvm_sys::LLVMAtomicOrdering;
use llvm_sys::LLVMIntPredicate;
use llvm_sys::LLVMOpcode;
use llvm_sys::LLVMRealPredicate;

use std::collections::HashMap;
use std::ffi::{CStr, CString};
use std::os::raw::{c_char, c_uint};

pub unsafe fn gen_instr_branch1(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins_args: &mut HashMap<MuID, Vec<(LLVMTypeRef, LLVMValueRef)>>,
    dest: &Destination,
    ins: &Instruction
) {
    let llvm_dest = llvm_internal_context
        .blocks_map
        .get(&dest.target.id())
        .unwrap();
    LLVMBuildBr(llvm_internal_context.builder, *llvm_dest);

    add_args_to_destination(
        llvm_internal_context,
        dest,
        dest.target.id(),
        store,
        ins_args,
        ins
    );
}

pub unsafe fn gen_instr_branch2(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins_args: &mut HashMap<MuID, Vec<(LLVMTypeRef, LLVMValueRef)>>,
    cond: &usize,
    true_dest: &Destination,
    false_dest: &Destination,
    _true_prob: &f32,
    ins: &Instruction
) {
    let llvm_cond = gen_llvm_value(
        llvm_internal_context,
        ins.ops.get(*cond).unwrap(),
        store
    );

    // if the two branches are the same create a select and the args will be
    // dependent on the result of select
    if true_dest.target.id() == false_dest.target.id() {
        add_multiple_args_to_same_destination(
            llvm_internal_context,
            llvm_cond,
            true_dest,
            false_dest,
            store,
            ins_args,
            ins
        );

        let llvm_true_dest = llvm_internal_context
            .blocks_map
            .get(&true_dest.target.id())
            .unwrap();

        LLVMBuildBr(llvm_internal_context.builder, *llvm_true_dest);
        return;
    }

    let llvm_true_dest = llvm_internal_context
        .blocks_map
        .get(&true_dest.target.id())
        .unwrap();

    let llvm_false_dest = llvm_internal_context
        .blocks_map
        .get(&false_dest.target.id())
        .unwrap();

    LLVMBuildCondBr(
        llvm_internal_context.builder,
        llvm_cond,
        *llvm_true_dest,
        *llvm_false_dest
    );

    add_args_to_destination(
        llvm_internal_context,
        true_dest,
        true_dest.target.id(),
        store,
        ins_args,
        ins
    );
    add_args_to_destination(
        llvm_internal_context,
        false_dest,
        false_dest.target.id(),
        store,
        ins_args,
        ins
    );
}

pub unsafe fn gen_instr_switch(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    curr_block: &usize,
    mut block_arguments: &mut HashMap<
        MuID,
        Vec<(LLVMBasicBlockRef, Vec<(LLVMTypeRef, LLVMValueRef)>)>
    >,
    mut ins_args: &mut HashMap<MuID, Vec<(LLVMTypeRef, LLVMValueRef)>>,
    ins: &Instruction,
    cond: &usize,
    default: &Destination,
    branches: &Vec<(usize, Destination)>
) {
    let mut overlapping_dest = false;
    let mut dests = vec![default.target.id()];
    let mut overlapping_dests = vec![];
    for (_, dest) in branches {
        if dests.contains(&dest.target.id()) {
            overlapping_dest = true;
            overlapping_dests.push(dest.target.id());
        } else {
            dests.push(dest.target.id());
        }
    }

    // if there are overlapping destinations, switch needs to branch to an
    // intermediary branch to create phi nodes correctly
    if overlapping_dest {
        gen_instr_switch_with_overlapping_blocks(
            llvm_internal_context,
            store,
            curr_block,
            &mut block_arguments,
            &mut ins_args,
            ins,
            cond,
            default,
            branches
        );
        return;
    }

    // otherwise create a normal switch
    let llvm_cond = gen_llvm_value(
        llvm_internal_context,
        &ins.ops.get(*cond).unwrap().clone(),
        store
    );

    let llvm_default_dest = llvm_internal_context
        .blocks_map
        .get(&default.target.id())
        .unwrap();

    let switch = LLVMBuildSwitch(
        llvm_internal_context.builder,
        llvm_cond,
        *llvm_default_dest,
        branches.len() as u32
    );

    add_args_to_destination(
        llvm_internal_context,
        default,
        default.target.id(),
        store,
        ins_args,
        ins
    );

    for (value, dest) in branches {
        let llvm_value = gen_llvm_value(
            llvm_internal_context,
            &ins.ops.get(*value).unwrap().clone(),
            store
        );

        let llvm_dest = llvm_internal_context
            .blocks_map
            .get(&dest.target.id())
            .unwrap();

        LLVMAddCase(switch, llvm_value, *llvm_dest);
        add_args_to_destination(
            llvm_internal_context,
            dest,
            dest.target.id(),
            store,
            ins_args,
            ins
        );
    }
}

pub unsafe fn gen_instr_switch_with_overlapping_blocks(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    label: &usize,
    block_arguments: &mut HashMap<
        MuID,
        Vec<(LLVMBasicBlockRef, Vec<(LLVMTypeRef, LLVMValueRef)>)>
    >,
    ins_args: &mut HashMap<MuID, Vec<(LLVMTypeRef, LLVMValueRef)>>,
    ins: &Instruction,
    cond: &usize,
    default: &Destination,
    branches: &Vec<(usize, Destination)>
) {
    let llvm_cond = gen_llvm_value(
        llvm_internal_context,
        &ins.ops.get(*cond).unwrap().clone(),
        store
    );

    let llvm_default = llvm_internal_context
        .blocks_map
        .get(&default.target.id())
        .unwrap();

    // create intermediary blocks to be branched to
    let default_dest_new_name =
        format!("{}_from_switch\0", default.target.name());
    let llvm_default_dest_new = LLVMInsertBasicBlock(
        *llvm_default,
        default_dest_new_name.as_str().as_ptr() as *const c_char
    );

    let switch = LLVMBuildSwitch(
        llvm_internal_context.builder,
        llvm_cond,
        llvm_default_dest_new,
        branches.len() as u32
    );

    create_branch_and_add_args_to_destination(
        llvm_internal_context,
        default,
        llvm_default_dest_new,
        store,
        block_arguments,
        ins
    );

    let llvm_curr_block = llvm_internal_context.blocks_map.get(label).unwrap();
    LLVMPositionBuilderAtEnd(llvm_internal_context.builder, *llvm_curr_block);

    for (value, dest) in branches {
        let llvm_value = gen_llvm_value(
            llvm_internal_context,
            &ins.ops.get(*value).unwrap().clone(),
            store
        );

        // create intermediary blocks to be branched to
        let dest_new_name = format!("{}_from_switch\0", dest.target.name());

        let llvm_dest = llvm_internal_context
            .blocks_map
            .get(&dest.target.id())
            .unwrap();

        let llvm_dest_new = LLVMInsertBasicBlock(
            *llvm_dest,
            dest_new_name.as_str().as_ptr() as *const c_char
        );

        LLVMAddCase(switch, llvm_value, llvm_dest_new);

        create_branch_and_add_args_to_destination(
            llvm_internal_context,
            dest,
            llvm_dest_new,
            store,
            block_arguments,
            ins
        );

        let llvm_curr_block =
            llvm_internal_context.blocks_map.get(label).unwrap();
        LLVMPositionBuilderAtEnd(
            llvm_internal_context.builder,
            *llvm_curr_block
        );
    }
}

pub unsafe fn create_branch_and_add_args_to_destination(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    dest: &Destination,
    from: LLVMBasicBlockRef,
    mut store: &mut HashMap<MuID, LLVMValueRef>,
    block_arguments: &mut HashMap<
        MuID,
        Vec<(LLVMBasicBlockRef, Vec<(LLVMTypeRef, LLVMValueRef)>)>
    >,
    ins: &Instruction
) {
    LLVMPositionBuilderAtEnd(llvm_internal_context.builder, from);

    let llvm_dest = llvm_internal_context
        .blocks_map
        .get(&dest.target.id())
        .unwrap();

    LLVMBuildBr(llvm_internal_context.builder, *llvm_dest);

    let mut args_pos = vec![];
    let mut args = vec![];

    for arg in &dest.args {
        match arg {
            DestArg::Normal(a) => args_pos.push(*a),
            _ => ()
        }
    }

    for pos in args_pos {
        args.push(ins.ops.get(pos).unwrap());
    }

    let block_args =
        process_inst_args(llvm_internal_context, &args, &mut store);

    if block_arguments.contains_key(&dest.target.id()) {
        let mut pre_existing =
            block_arguments.get_mut(&dest.target.id()).unwrap();
        pre_existing.push((from, block_args));
    } else {
        let new_values = vec![(from, block_args)];
        block_arguments.insert(dest.target.id(), new_values);
    }
}

pub unsafe fn add_args_to_destination(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    dest: &Destination,
    dest_targ_id: MuID,
    mut store: &mut HashMap<MuID, LLVMValueRef>,
    ins_args: &mut HashMap<MuID, Vec<(LLVMTypeRef, LLVMValueRef)>>,
    ins: &Instruction
) {
    let mut args_pos = vec![];
    let mut args = vec![];

    for arg in &dest.args {
        match arg {
            DestArg::Normal(a) => args_pos.push(*a),
            _ => ()
        }
    }

    for pos in args_pos {
        args.push(ins.ops.get(pos).unwrap());
    }

    let block_args =
        process_inst_args(llvm_internal_context, &args, &mut store);

    ins_args.insert(dest_targ_id, block_args);
}

pub unsafe fn add_multiple_args_to_same_destination(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    cond: LLVMValueRef,
    true_dest: &Destination,
    false_dest: &Destination,
    mut store: &mut HashMap<MuID, LLVMValueRef>,
    ins_args: &mut HashMap<MuID, Vec<(LLVMTypeRef, LLVMValueRef)>>,
    ins: &Instruction
) {
    assert_eq!(true_dest.target, false_dest.target);
    let mut true_args_pos = vec![];
    let mut true_args = vec![];

    for arg in &true_dest.args {
        match arg {
            DestArg::Normal(a) => true_args_pos.push(*a),
            _ => ()
        }
    }

    for pos in true_args_pos {
        true_args.push(ins.ops.get(pos).unwrap());
    }

    let mut false_args_pos = vec![];
    let mut false_args = vec![];

    for arg in &false_dest.args {
        match arg {
            DestArg::Normal(a) => false_args_pos.push(*a),
            _ => ()
        }
    }

    for pos in false_args_pos {
        false_args.push(ins.ops.get(pos).unwrap());
    }

    let block_args = process_inst_args_with_select(
        llvm_internal_context,
        cond,
        &true_args,
        &false_args,
        &mut store
    );
    ins_args.insert(true_dest.target.id(), block_args);
}

pub unsafe fn gen_instr_return(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    idx: &Vec<usize>,
    ins: &Instruction
) {
    if idx.len() == 0 {
        LLVMBuildRetVoid(llvm_internal_context.builder);
    } else if idx.len() > 1 {
        // TODO support returning tuples
        unimplemented!()
    } else {
        let idx = *idx.get(0).unwrap();

        let return_val = ins.ops.get(idx).unwrap();

        if !return_val.is_value() {
            // TODO what if return value is not a value?
            unimplemented!()
        }
        // get value from store
        let llvm_return_val =
            gen_llvm_value(llvm_internal_context, return_val, store);

        LLVMBuildRet(llvm_internal_context.builder, llvm_return_val);
    }
}

pub unsafe fn gen_instr_binop(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    bop: &BinOp,
    op1: &usize,
    op2: &usize
) {
    let llvm_op1 = gen_llvm_value(
        llvm_internal_context,
        ins.ops.get(*op1).unwrap(),
        store
    );
    let llvm_op2 = gen_llvm_value(
        llvm_internal_context,
        ins.ops.get(*op2).unwrap(),
        store
    );

    let value = match &ins.value {
        Some(v) => v,
        _ => unimplemented!()
    };

    if value.len() > 1 {
        unimplemented!()
    }

    let value = value.get(0);

    match bop {
        mu::ast::op::BinOp::FAdd => {
            // result
            match value {
                Some(v) => {
                    let name = v.hdr.name();
                    let id = v.hdr.id();
                    let name = CString::new(name.as_str()).unwrap();
                    let llvm_add = LLVMBuildFAdd(
                        llvm_internal_context.builder,
                        llvm_op1,
                        llvm_op2,
                        name.as_ptr()
                    );
                    store.insert(id, llvm_add);
                }
                None => unimplemented!()
            }
        }
        mu::ast::op::BinOp::FMul => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_add = LLVMBuildFMul(
                    llvm_internal_context.builder,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_add);
            }
            None => unimplemented!()
        },
        mu::ast::op::BinOp::FSub => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_add = LLVMBuildFSub(
                    llvm_internal_context.builder,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_add);
            }
            None => unimplemented!()
        },
        mu::ast::op::BinOp::Add => {
            // result
            match value {
                Some(v) => {
                    let name = v.hdr.name();
                    let id = v.hdr.id();
                    let name = CString::new(name.as_str()).unwrap();
                    let llvm_add = LLVMBuildAdd(
                        llvm_internal_context.builder,
                        llvm_op1,
                        llvm_op2,
                        name.as_ptr()
                    );
                    store.insert(id, llvm_add);
                }
                None => unimplemented!()
            }
        }
        mu::ast::op::BinOp::Sub => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_add = LLVMBuildSub(
                    llvm_internal_context.builder,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_add);
            }
            None => unimplemented!()
        },
        mu::ast::op::BinOp::Mul => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_mul = LLVMBuildMul(
                    llvm_internal_context.builder,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_mul);
            }
            None => unimplemented!()
        },
        mu::ast::op::BinOp::Udiv => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_udiv = LLVMBuildUDiv(
                    llvm_internal_context.builder,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_udiv);
            }
            None => unimplemented!()
        },
        mu::ast::op::BinOp::FDiv => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_fdiv = LLVMBuildFDiv(
                    llvm_internal_context.builder,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_fdiv);
            }
            None => unimplemented!()
        },
        mu::ast::op::BinOp::Sdiv => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_sdiv = LLVMBuildSDiv(
                    llvm_internal_context.builder,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_sdiv);
            }
            None => unimplemented!()
        },
        mu::ast::op::BinOp::Shl => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_shl = LLVMBuildShl(
                    llvm_internal_context.builder,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_shl);
            }
            None => unimplemented!()
        },
        mu::ast::op::BinOp::Lshr => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_lshr = LLVMBuildLShr(
                    llvm_internal_context.builder,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_lshr);
            }
            None => unimplemented!()
        },
        mu::ast::op::BinOp::Ashr => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_ashr = LLVMBuildAShr(
                    llvm_internal_context.builder,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_ashr);
            }
            None => unimplemented!()
        },
        mu::ast::op::BinOp::Srem => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_srem = LLVMBuildSRem(
                    llvm_internal_context.builder,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_srem);
            }
            None => unimplemented!()
        },
        mu::ast::op::BinOp::Urem => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_urem = LLVMBuildURem(
                    llvm_internal_context.builder,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_urem);
            }
            None => unimplemented!()
        },
        mu::ast::op::BinOp::And => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_and = LLVMBuildAnd(
                    llvm_internal_context.builder,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_and);
            }
            None => unimplemented!()
        },
        mu::ast::op::BinOp::Xor => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_xor = LLVMBuildXor(
                    llvm_internal_context.builder,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_xor);
            }
            None => unimplemented!()
        },
        mu::ast::op::BinOp::Or => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_xor = LLVMBuildOr(
                    llvm_internal_context.builder,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_xor);
            }
            None => unimplemented!()
        },
        _ => {
            println!("{:?}", ins);
            unimplemented!()
        }
    };
}

pub unsafe fn gen_instr_cmpop(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    cop: &CmpOp,
    op1: &usize,
    op2: &usize
) {
    let llvm_op1 = gen_llvm_value(
        llvm_internal_context,
        ins.ops.get(*op1).unwrap(),
        store
    );
    let llvm_op2 = gen_llvm_value(
        llvm_internal_context,
        ins.ops.get(*op2).unwrap(),
        store
    );

    let value = match &ins.value {
        Some(v) => v,
        _ => unimplemented!()
    };

    if value.len() > 1 {
        unimplemented!()
    }

    let value = value.get(0);
    match cop {
        mu::ast::op::CmpOp::EQ => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_eq = LLVMBuildICmp(
                    llvm_internal_context.builder,
                    LLVMIntPredicate::LLVMIntEQ,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_eq);
            }
            None => unimplemented!()
        },
        mu::ast::op::CmpOp::NE => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_ne = LLVMBuildICmp(
                    llvm_internal_context.builder,
                    LLVMIntPredicate::LLVMIntNE,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_ne);
            }
            None => unimplemented!()
        },
        mu::ast::op::CmpOp::SLT => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_slt = LLVMBuildICmp(
                    llvm_internal_context.builder,
                    LLVMIntPredicate::LLVMIntSLT,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_slt);
            }
            None => unimplemented!()
        },
        mu::ast::op::CmpOp::SLE => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_sle = LLVMBuildICmp(
                    llvm_internal_context.builder,
                    LLVMIntPredicate::LLVMIntSLE,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_sle);
            }
            None => unimplemented!()
        },
        mu::ast::op::CmpOp::SGT => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_sgt = LLVMBuildICmp(
                    llvm_internal_context.builder,
                    LLVMIntPredicate::LLVMIntSGT,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_sgt);
            }
            None => unimplemented!()
        },
        mu::ast::op::CmpOp::SGE => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_sge = LLVMBuildICmp(
                    llvm_internal_context.builder,
                    LLVMIntPredicate::LLVMIntSGE,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_sge);
            }
            None => unimplemented!()
        },
        mu::ast::op::CmpOp::UGT => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_ugt = LLVMBuildICmp(
                    llvm_internal_context.builder,
                    LLVMIntPredicate::LLVMIntUGT,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_ugt);
            }
            None => unimplemented!()
        },
        mu::ast::op::CmpOp::ULT => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_ult = LLVMBuildICmp(
                    llvm_internal_context.builder,
                    LLVMIntPredicate::LLVMIntULT,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_ult);
            }
            None => unimplemented!()
        },
        mu::ast::op::CmpOp::ULE => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_ule = LLVMBuildICmp(
                    llvm_internal_context.builder,
                    LLVMIntPredicate::LLVMIntULE,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_ule);
            }
            None => unimplemented!()
        },
        mu::ast::op::CmpOp::UGE => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_uge = LLVMBuildICmp(
                    llvm_internal_context.builder,
                    LLVMIntPredicate::LLVMIntUGE,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_uge);
            }
            None => unimplemented!()
        },
        mu::ast::op::CmpOp::FOEQ => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_foeq = LLVMBuildFCmp(
                    llvm_internal_context.builder,
                    LLVMRealPredicate::LLVMRealOEQ,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_foeq);
            }
            None => unimplemented!()
        },
        mu::ast::op::CmpOp::FONE => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_fone = LLVMBuildFCmp(
                    llvm_internal_context.builder,
                    LLVMRealPredicate::LLVMRealONE,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_fone);
            }
            None => unimplemented!()
        },
        mu::ast::op::CmpOp::FOLE => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_fole = LLVMBuildFCmp(
                    llvm_internal_context.builder,
                    LLVMRealPredicate::LLVMRealOLE,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_fole);
            }
            None => unimplemented!()
        },
        mu::ast::op::CmpOp::FOGE => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_foge = LLVMBuildFCmp(
                    llvm_internal_context.builder,
                    LLVMRealPredicate::LLVMRealOGE,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_foge);
            }
            None => unimplemented!()
        },
        mu::ast::op::CmpOp::FOLT => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_folt = LLVMBuildFCmp(
                    llvm_internal_context.builder,
                    LLVMRealPredicate::LLVMRealOLT,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_folt);
            }
            None => unimplemented!()
        },
        mu::ast::op::CmpOp::FOGT => match value {
            Some(v) => {
                let name = v.hdr.name();
                let id = v.hdr.id();
                let name = CString::new(name.as_str()).unwrap();
                let llvm_fogt = LLVMBuildFCmp(
                    llvm_internal_context.builder,
                    LLVMRealPredicate::LLVMRealOGT,
                    llvm_op1,
                    llvm_op2,
                    name.as_ptr()
                );
                store.insert(id, llvm_fogt);
            }
            None => unimplemented!()
        },
        _ => {
            println!("{:?}", ins);
            unimplemented!()
        }
    }
}
pub unsafe fn gen_instr_call(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins_args: &mut HashMap<MuID, Vec<(LLVMTypeRef, LLVMValueRef)>>,
    ins: &Instruction,
    data: &CallData,
    resume: &ResumptionData,
    is_ccall: bool
) {
    let mut args = vec![];
    let function_name = ins.ops.get(data.func).unwrap();
    let mut no_return_value = false;

    let (exp_id, call_name) = match &ins.value {
        Some(n) => {
            if n.len() > 1 {
                unimplemented!()
            }
            if n.len() == 0 {
                no_return_value = true;
                (0, Arc::new(String::from("\0")))
            } else {
                let n = n.get(0).unwrap();
                (n.hdr.id(), n.hdr.name())
            }
        }
        None => {
            no_return_value = true;
            (0, Arc::new(String::from("\0")))
        }
    };

    let llvm_function_name = match &function_name.v {
        TreeNode_::Value(v) => {
            let f_ref = match &v.v {
                Value_::Constant(Constant::FuncRef(f)) => {
                    let f_name = if !is_ccall {
                        Arc::new(mu::ast::ir::mangle_name(f.name()))
                    } else {
                        f.name()
                    };
                    let function_name = CString::new(f_name.as_str()).unwrap();
                    LLVMGetNamedFunction(
                        llvm_internal_context.module,
                        function_name.as_ptr()
                    )
                }
                Value_::Constant(Constant::ExternSym(name)) => {
                    let f_name = if !is_ccall {
                        Arc::new(mu::ast::ir::mangle_name(name.clone()))
                    } else {
                        name.clone()
                    };
                    let function_name = CString::new(f_name.as_str()).unwrap();
                    LLVMGetNamedFunction(
                        llvm_internal_context.module,
                        function_name.as_ptr()
                    )
                }
                Value_::SSAVar(id) => *store.get(id).unwrap(),
                _ => {
                    println!("{:?}", v.v);
                    unimplemented!();
                }
            };
            f_ref
        }
        _ => unimplemented!()
    };

    if llvm_function_name.is_null() {
        panic!("Could not find function.");
    }

    for op in &data.args {
        let mu_arg = ins.ops.get(*op).unwrap();
        args.push(mu_arg);
    }
    let mut llvm_typed_args =
        process_inst_args(llvm_internal_context, &args, store);
    let mut llvm_args = vec![];

    for (_, llvm_arg) in llvm_typed_args.drain(..) {
        llvm_args.push(llvm_arg);
    }

    let exception_dest = &resume.exn_dest;
    let normal_dest = &resume.normal_dest;

    add_args_to_destination(
        llvm_internal_context,
        exception_dest,
        exception_dest.target.id(),
        store,
        ins_args,
        ins
    );

    let llvm_exception_dest = llvm_internal_context
        .blocks_map
        .get(&exception_dest.target.id())
        .unwrap();

    if normal_dest.target.id() == exception_dest.target.id() {
        let new_norm = llvm_internal_context
            .orig_dup_block_map
            .get(&normal_dest.target.id())
            .unwrap();

        let llvm_normal_dest =
            llvm_internal_context.blocks_map.get(new_norm).unwrap();

        let invoke = LLVMBuildInvoke(
            llvm_internal_context.builder,
            llvm_function_name,
            llvm_args.as_mut_ptr(),
            llvm_args.len() as c_uint,
            *llvm_normal_dest,
            *llvm_exception_dest,
            call_name.as_str().as_ptr() as *const c_char
        );

        if !no_return_value {
            store.insert(exp_id, invoke);
        }

        add_args_to_destination(
            llvm_internal_context,
            normal_dest,
            *new_norm,
            store,
            ins_args,
            ins
        );
    } else {
        let llvm_normal_dest = llvm_internal_context
            .blocks_map
            .get(&normal_dest.target.id())
            .unwrap();

        let invoke = LLVMBuildInvoke(
            llvm_internal_context.builder,
            llvm_function_name,
            llvm_args.as_mut_ptr(),
            llvm_args.len() as c_uint,
            *llvm_normal_dest,
            *llvm_exception_dest,
            call_name.as_str().as_ptr() as *const c_char
        );

        if !no_return_value {
            store.insert(exp_id, invoke);
        }

        add_args_to_destination(
            llvm_internal_context,
            normal_dest,
            normal_dest.target.id(),
            store,
            ins_args,
            ins
        );
    }
}

pub unsafe fn gen_instr_exprcall(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    data: &CallData,
    _is_abort: &bool,
    is_ccall: bool
) {
    let mut args = vec![];
    let function_name = ins.ops.get(data.func).unwrap();
    let mut no_return_value = false;
    let (exp_id, call_name) = match &ins.value {
        Some(n) => {
            if n.len() > 1 {
                unimplemented!()
            }
            if n.len() == 0 {
                no_return_value = true;
                (0, Arc::new(String::from("\0")))
            } else {
                let n = n.get(0).unwrap();
                (n.hdr.id(), Arc::new(format!("{}\0", n.hdr.name())))
            }
        }
        None => unimplemented!()
    };

    let llvm_function_name = match &function_name.v {
        TreeNode_::Value(v) => {
            let f_ref = match &v.v {
                Value_::Constant(Constant::FuncRef(f)) => {
                    let f_name = if !is_ccall {
                        Arc::new(mu::ast::ir::mangle_name(f.name()))
                    } else {
                        f.name()
                    };
                    let function_name = CString::new(f_name.as_str()).unwrap();
                    LLVMGetNamedFunction(
                        llvm_internal_context.module,
                        function_name.as_ptr()
                    )
                }
                Value_::Constant(Constant::ExternSym(name)) => {
                    let f_name = if !is_ccall {
                        Arc::new(mu::ast::ir::mangle_name(name.clone()))
                    } else {
                        name.clone()
                    };
                    let function_name = CString::new(f_name.as_str()).unwrap();
                    LLVMGetNamedFunction(
                        llvm_internal_context.module,
                        function_name.as_ptr()
                    )
                }
                Value_::SSAVar(id) => *store.get(id).unwrap(),
                _ => {
                    println!("{:?}", v.v);
                    unimplemented!();
                }
            };
            f_ref
        }
        _ => unimplemented!()
    };

    if llvm_function_name.is_null() {
        panic!("Could not find function.");
    }

    for op in &data.args {
        let mu_arg = ins.ops.get(*op).unwrap();
        args.push(mu_arg);
    }
    let mut llvm_typed_args =
        process_inst_args(llvm_internal_context, &args, store);
    let mut llvm_args = vec![];

    for (_, llvm_arg) in llvm_typed_args.drain(..) {
        llvm_args.push(llvm_arg);
    }

    let call = LLVMBuildCall(
        llvm_internal_context.builder,
        llvm_function_name,
        llvm_args.as_mut_ptr(),
        llvm_args.len() as c_uint,
        call_name.as_str().as_ptr() as *const c_char
    );

    if !no_return_value {
        store.insert(exp_id, call);
    }
}

pub unsafe fn gen_instr_exprccall(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    data: &CallData,
    _is_abort: &bool
) {
    gen_instr_exprcall(
        llvm_internal_context,
        store,
        ins,
        data,
        _is_abort,
        true
    );
}

pub unsafe fn gen_instr_new_hybrid(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    mu_type: &P<MuType>,
    length: &usize
) {
    let llvm_type = gen_llvm_type(llvm_internal_context, mu_type);
    let ins_value = ins.value.clone().unwrap();
    if ins_value.len() > 1 {
        panic!("Unsupported operation.");
    };
    let id = ins_value.get(0).unwrap().hdr.id();
    let name = format!("{}\0", ins_value.get(0).unwrap().hdr.name());

    let muentry_alloc_mmtk_fn = LLVMGetNamedFunction(
        llvm_internal_context.module,
        "allocate_mem\0".as_ptr() as *const c_char
    );
    if muentry_alloc_mmtk_fn.is_null() {
        panic!("Function has not been defined.");
    }

    let hybrid_type_name = match &mu_type.v {
        MuType_::UPtr(h) => h,
        MuType_::Ref(h) => h,
        MuType_::IRef(h) => h,
        MuType_::Hybrid(_) => mu_type,
        _ => {
            println!("{:?}", mu_type.v);
            panic!("Expecting pointer type.")
        }
    };

    let hybrid_map = HYBRID_TAG_MAP.read().unwrap();
    let hybrid_ty: &HybridType_ = hybrid_map
        .get(&hybrid_type_name.get_struct_hybrid_tag().unwrap())
        .unwrap();

    let module_data_layout =
        LLVMGetModuleDataLayout(llvm_internal_context.module);

    let mut total_size = 0;
    for fix_ty in hybrid_ty.get_fix_tys() {
        let llvm_fix_ty = gen_llvm_type(llvm_internal_context, fix_ty);
        let size = LLVMABISizeOfType(module_data_layout, llvm_fix_ty);
        total_size += size;
    }
    let var_ty = hybrid_ty.get_var_ty();

    let llvm_var_ty = gen_llvm_type(llvm_internal_context, var_ty);
    let size_of_hybrid = LLVMABISizeOfType(module_data_layout, llvm_var_ty);

    let llvm_length = gen_llvm_value(
        llvm_internal_context,
        ins.ops.get(*length).unwrap(),
        store
    );
    let llvm_hybrid_size =
        LLVMConstInt(LLVMInt64Type(), size_of_hybrid, true as i32);
    let hybrid_size = LLVMBuildMul(
        llvm_internal_context.builder,
        llvm_length,
        llvm_hybrid_size,
        "hyb_size\0".as_ptr() as *const c_char
    );

    let total_size = LLVMConstInt(LLVMInt64Type(), total_size, true as i32);
    let total_size = LLVMBuildAdd(
        llvm_internal_context.builder,
        hybrid_size,
        total_size,
        "total_size\0".as_ptr() as *const c_char
    );

    let data_layout = LLVMGetModuleDataLayout(llvm_internal_context.module);
    let size = LLVMABISizeOfType(data_layout, llvm_type);
    let size = LLVMConstInt(LLVMInt64Type(), size, false as i32);
    let mut alignment = LLVMABIAlignmentOfType(data_layout, llvm_var_ty);
    if alignment < 4 {
        alignment = 4; // Minimal alignment in MMTk
    }
    let alignment =
        LLVMConstInt(LLVMInt64Type(), alignment as u64, false as i32);

    let mut mmtk_alloc_args = vec![
        total_size,
        alignment,
        LLVMConstInt(LLVMInt64Type(), 0, false as i32),
        LLVMConstInt(LLVMInt32Type(), mu_type.hdr.id() as u64, 0),
    ];

    let mmtk_alloc_call = LLVMBuildCall(
        llvm_internal_context.builder,
        muentry_alloc_mmtk_fn,
        mmtk_alloc_args.as_mut_ptr(),
        mmtk_alloc_args.len() as u32,
        "hyb_i8\0".as_ptr() as *const c_char
    );

    let mmtk_alloc_bitcast = LLVMBuildBitCast(
        llvm_internal_context.builder,
        mmtk_alloc_call,
        LLVMPointerType(llvm_type, 1),
        name.as_str().as_ptr() as *const c_char
    );

    store.insert(id, mmtk_alloc_bitcast);
}

pub unsafe fn gen_instr_alloca(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    llvm_type: LLVMTypeRef
) {
    let ins_value = ins.value.clone().unwrap();
    if ins_value.len() > 1 {
        panic!("Unsupported operation.");
    };
    let id = ins_value.get(0).unwrap().hdr.id();
    let name = format!("{}\0", ins_value.get(0).unwrap().hdr.name().clone());
    let alloc_a = LLVMBuildAlloca(
        llvm_internal_context.builder,
        llvm_type,
        name.as_str().as_ptr() as *const c_char
    );

    store.insert(id, alloc_a);
}

pub unsafe fn gen_instr_new(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    llvm_type: LLVMTypeRef,
    mu_type: &P<MuType>
) {
    let ins_value = ins.value.clone().unwrap();
    if ins_value.len() > 1 {
        panic!("Unsupported operation.");
    };
    let id = ins_value.get(0).unwrap().hdr.id();
    let type_id = mu_type.hdr.id();
    let name_ptr =
        format!("{}_ptr\0", ins_value.get(0).unwrap().hdr.name().clone());
    let name = format!("{}\0", ins_value.get(0).unwrap().hdr.name().clone());

    let data_layout = LLVMGetModuleDataLayout(llvm_internal_context.module);
    let size = LLVMABISizeOfType(data_layout, llvm_type);
    let allocate_small = size < 64 && !mu_type.is_aggregate();
    let muentry_alloc_mmtk_fn = if allocate_small {
        let allocate_fn = LLVMGetNamedFunction(
            llvm_internal_context.module,
            "allocate_mem_small\0".as_ptr() as *const c_char
        );
        if allocate_fn.is_null() {
            panic!("Function has not been defined.");
        };
        allocate_fn
    } else {
        let allocate_fn = LLVMGetNamedFunction(
            llvm_internal_context.module,
            "allocate_mem\0".as_ptr() as *const c_char
        );
        if allocate_fn.is_null() {
            panic!("Function has not been defined.");
        };
        allocate_fn
    };

    let size = LLVMConstInt(LLVMInt64Type(), size, false as i32);
    let mut alignment = LLVMABIAlignmentOfType(data_layout, llvm_type);
    if alignment < 4 {
        alignment = 4; // Minimal alignment in MMTk
    }
    let alignment =
        LLVMConstInt(LLVMInt64Type(), alignment as u64, false as i32);

    let mut args = if allocate_small {
        vec![
            size,
            alignment,
            LLVMConstInt(LLVMInt64Type(), 0, false as i32),
            LLVMConstInt(LLVMInt1Type(), mu_type.is_heap_reference() as u64, 0),
        ]
    } else {
        vec![
            size,
            alignment,
            LLVMConstInt(LLVMInt64Type(), 0, false as i32),
            LLVMConstInt(LLVMInt32Type(), type_id as u64, 0),
        ]
    };

    let ptr = LLVMBuildCall(
        llvm_internal_context.builder,
        muentry_alloc_mmtk_fn,
        args.as_mut_ptr(),
        args.len() as u32,
        name_ptr.as_str().as_ptr() as *const c_char
    );

    let var_ptr = LLVMBuildBitCast(
        llvm_internal_context.builder,
        ptr,
        LLVMPointerType(llvm_type, 1),
        name.as_str().as_ptr() as *const c_char
    );

    store.insert(id, var_ptr);
}

pub unsafe fn gen_instr_get_iref(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    op: &usize
) {
    let op = ins.ops.get(*op).unwrap();
    let ins_value = ins.value.clone().unwrap();
    if ins_value.len() > 1 {
        panic!("Unsupported operation.");
    };
    let id = ins_value.get(0).unwrap().hdr.id();
    let name = format!("{}\0", ins_value.get(0).unwrap().hdr.name());

    let (op_type, ptr) = match &op.v {
        TreeNode_::Value(value) => {
            let t = &value.ty.v;
            match store.get(&value.hdr.id()) {
                Some(s) => (t, s),
                None => {
                    match llvm_internal_context
                        .global_store
                        .get(&value.hdr.id())
                    {
                        Some(s) => (t, s),
                        None => panic!("Could not find variable.")
                    }
                }
            }
        }
        _ => unimplemented!()
    };

    let is_array_internal;
    match op_type {
        MuType_::IRef(t) => {
            is_array_internal = match t.v {
                MuType_::Array(_, _) => true,
                _ => false
            }
        }
        MuType_::Ref(t) => {
            is_array_internal = match t.v {
                MuType_::Array(_, _) => true,
                _ => false
            }
        }
        _ => unimplemented!()
    }

    let get_elem_ptr;
    // returns a pointer to the first element of the array
    if is_array_internal {
        // FIXME Types should be compatible with pointer?
        let pos = [
            LLVMConstInt(LLVMInt32Type(), 0, false as i32),
            LLVMConstInt(LLVMInt32Type(), 0, false as i32)
        ];

        get_elem_ptr = LLVMBuildInBoundsGEP(
            llvm_internal_context.builder,
            *ptr,
            pos.as_ptr() as *mut LLVMValueRef,
            2,
            name.as_str().as_ptr() as *const c_char
        );
    } else {
        // returns a pointer to the memory location of ptr
        let pos = [LLVMConstInt(LLVMInt32Type(), 0, false as i32)];

        get_elem_ptr = *ptr;
    }

    store.insert(id, get_elem_ptr);
}

pub unsafe fn gen_instr_get_varpart_iref(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    base: &usize,
    _is_ptr: &bool
) {
    let base = ins.ops.get(*base).unwrap();
    let base_llvm = gen_llvm_value(llvm_internal_context, base, store);
    let base_ty = base.ty();
    let hybrid_type_name = match &base_ty.v {
        MuType_::UPtr(h) => h,
        MuType_::Ref(h) => h,
        MuType_::IRef(h) => h,
        _ => {
            println!("{:?}", base_ty.v);
            panic!("Expecting pointer type.")
        }
    };

    let hybrid_map = HYBRID_TAG_MAP.read().unwrap();
    let hybrid_ty: &HybridType_ = hybrid_map
        .get(&hybrid_type_name.get_struct_hybrid_tag().unwrap())
        .unwrap();

    let var_type_pos = hybrid_ty.get_fix_tys().len();
    let instr_res;

    if var_type_pos == 0 {
        let pos = [
            LLVMConstInt(LLVMInt32Type(), 0, false as i32),
            LLVMConstInt(LLVMInt32Type(), 0, false as i32)
        ];

        let get_elem_ptr = LLVMBuildInBoundsGEP(
            llvm_internal_context.builder,
            base_llvm,
            pos.as_ptr() as *mut LLVMValueRef,
            2,
            "tmp\0".as_ptr() as *const c_char
        );
        instr_res = get_elem_ptr;
    } else {
        let pos = [
            LLVMConstInt(LLVMInt32Type(), 0, false as i32),
            LLVMConstInt(LLVMInt32Type(), var_type_pos as u64, false as i32),
            LLVMConstInt(LLVMInt32Type(), 0, false as i32)
        ];

        let get_elem_ptr = LLVMBuildInBoundsGEP(
            llvm_internal_context.builder,
            base_llvm,
            pos.as_ptr() as *mut LLVMValueRef,
            3,
            "tmp\0".as_ptr() as *const c_char
        );

        instr_res = get_elem_ptr;
    }

    let ins_value = ins.value.clone().unwrap();
    let mut name = Arc::new(String::from(""));
    let mut id = 0;
    if ins_value.len() > 1 {
        panic!("Unsupported operation.");
    } else if ins_value.len() == 1 {
        id = ins_value.get(0).unwrap().hdr.id();
        name = ins_value.get(0).unwrap().hdr.name().clone();
    }

    if name.as_str() != "" {
        store.insert(id, instr_res);
    };
}

pub unsafe fn gen_instr_get_field_iref(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    base: &usize,
    index: &usize,
    _is_ptr: bool
) {
    let base = ins.ops.get(*base).unwrap();
    let ins_value = ins.value.clone().unwrap();
    if ins_value.len() != 1 {
        panic!("Unsupported operation.");
    };
    let id = ins_value.get(0).unwrap().hdr.id();
    let name = format!("{}\0", ins_value.get(0).unwrap().hdr.name());

    let ptr = match &base.v {
        TreeNode_::Value(value) => store.get(&value.hdr.id()).unwrap(),
        _ => unimplemented!()
    };

    // FIXME Types should be compatible with pointer?
    let pos = [
        LLVMConstInt(LLVMInt32Type(), 0, false as i32),
        LLVMConstInt(LLVMInt32Type(), *index as u64, false as i32)
    ];

    let get_elem_ptr = LLVMBuildInBoundsGEP(
        llvm_internal_context.builder,
        *ptr,
        pos.as_ptr() as *mut LLVMValueRef,
        2,
        name.as_str().as_ptr() as *const c_char
    );

    // when getting a field of a global variable that is a pointer, do an
    // addrspacecast
    //    let is_global = LLVMIsAGlobalObject(*ptr);
    //    let is_global = LLVMPrintValueToString(is_global);
    //    let is_global = CString::from_raw(is_global);
    //    let is_global = String::from(is_global.to_str().unwrap());
    //
    //    let get_elem_ptr_type = LLVMTypeOf(get_elem_ptr);
    //    let internal_type = LLVMGetElementType(get_elem_ptr_type);
    //    let addrspace = LLVMGetPointerAddressSpace(internal_type);
    //
    //    if addrspace == 1 && is_global.contains("external global") {
    //        let internal_type_elem = LLVMGetElementType(internal_type);
    //        let new_type_internal = LLVMPointerType(internal_type_elem, 0);
    //        let new_type_get_elem_pointer = LLVMPointerType(new_type_internal,
    // 0);        let name = format!("{}_a0\0",
    // ins_value.get(0).unwrap().hdr.name());        get_elem_ptr =
    // LLVMBuildBitCast(            llvm_internal_context.builder,
    //            get_elem_ptr,
    //            new_type_get_elem_pointer,
    //            name.as_str().as_ptr() as *const c_char
    //        );
    //    }

    store.insert(id, get_elem_ptr);
}

pub unsafe fn gen_instr_get_element_iref(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    base: &usize,
    index: &usize,
    _is_ptr: bool
) {
    let base = ins.ops.get(*base).unwrap();
    let index = ins.ops.get(*index).unwrap();
    let ins_value = ins.value.clone().unwrap();
    if ins_value.len() != 1 {
        panic!("Unsupported operation.");
    };
    let id = ins_value.get(0).unwrap().hdr.id();
    let name = format!("{}\0", ins_value.get(0).unwrap().hdr.name());
    let llvm_idx = gen_llvm_value(llvm_internal_context, index, store);

    let llvm_base_zero_address_type =
        gen_llvm_type(llvm_internal_context, &index.as_value().ty);

    let ptr = match &base.v {
        TreeNode_::Value(value) => store.get(&value.hdr.id()).unwrap(),
        _ => unimplemented!()
    };

    // FIXME Types should be compatible with pointer?
    let pos = [
        LLVMConstInt(llvm_base_zero_address_type, 0, false as i32),
        llvm_idx
    ];

    let mut get_elem_ptr = LLVMBuildInBoundsGEP(
        llvm_internal_context.builder,
        *ptr,
        pos.as_ptr() as *mut LLVMValueRef,
        2,
        name.as_str().as_ptr() as *const c_char
    );

    // when getting an element of a global variable that is a pointer, do an
    // addrspacecast
    let is_global = LLVMIsAGlobalObject(*ptr);
    let is_global = LLVMPrintValueToString(is_global);
    let is_global = CString::from_raw(is_global);
    let is_global = String::from(is_global.to_str().unwrap());

    let get_elem_ptr_type = LLVMTypeOf(get_elem_ptr);

    let internal_type = LLVMGetElementType(get_elem_ptr_type);
    let addrspace = LLVMGetPointerAddressSpace(internal_type);
    if addrspace == 1 && is_global.contains("external global") {
        let internal_type_elem = LLVMGetElementType(internal_type);
        let new_type_internal = LLVMPointerType(internal_type_elem, 0);
        let new_type_get_elem_pointer = LLVMPointerType(new_type_internal, 0);
        let name = format!("{}_a0\0", ins_value.get(0).unwrap().hdr.name());
        get_elem_ptr = LLVMBuildBitCast(
            llvm_internal_context.builder,
            get_elem_ptr,
            new_type_get_elem_pointer,
            name.as_str().as_ptr() as *const c_char
        );
    }

    store.insert(id, get_elem_ptr);
}

pub unsafe fn gen_instr_shift_iref(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    base: &usize,
    index: &usize,
    _is_ptr: bool
) {
    let base = ins.ops.get(*base).unwrap();
    let index = ins.ops.get(*index).unwrap();
    let ins_value = ins.value.clone().unwrap();
    if ins_value.len() != 1 {
        panic!("Unsupported operation.");
    };
    let id = ins_value.get(0).unwrap().hdr.id();
    let name = format!("{}\0", ins_value.get(0).unwrap().hdr.name());
    let llvm_idx = gen_llvm_value(llvm_internal_context, index, store);

    let ptr = match &base.v {
        TreeNode_::Value(value) => store.get(&value.hdr.id()).unwrap(),
        _ => unimplemented!()
    };

    // FIXME Types should be compatible with pointer?
    let pos = [llvm_idx];

    let get_elem_ptr = LLVMBuildInBoundsGEP(
        llvm_internal_context.builder,
        *ptr,
        pos.as_ptr() as *mut LLVMValueRef,
        1,
        name.as_ptr() as *const c_char
    );

    store.insert(id, get_elem_ptr);
}

pub unsafe fn gen_instr_store(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    mem_loc: &usize,
    value: &usize,
    is_ptr: bool,
    order: MemoryOrder
) {
    let mem_loc = ins.ops.get(*mem_loc).unwrap();
    let value = ins.ops.get(*value).unwrap();
    let value_type = value.ty();

    // FIXME this should be the case, but apparently it is not
    //    assert_eq!(
    //        value_type.is_heap_reference() ||
    // value_type.is_opaque_reference(),        is_ptr
    //    );

    let llvm_value = gen_llvm_value(llvm_internal_context, value, store);

    let llvm_mem_loc = match &mem_loc.v {
        TreeNode_::Value(value) => {
            let key = &value.hdr.id();
            if llvm_internal_context.global_store.contains_key(key) {
                llvm_internal_context.global_store.get(key).unwrap()
            } else {
                store.get(key).unwrap()
            }
        }
        _ => unimplemented!()
    };

    #[cfg(feature = "gencopy")]
    {
        if value_type.is_heap_reference() && mem_loc.ty().is_heap_reference() {
            let target_type = LLVMTypeOf(*llvm_mem_loc);
            let addrspace_memloc = LLVMGetPointerAddressSpace(target_type);

            if addrspace_memloc == 1 {
                let value_bitcast_name = format!("{}_casted\0", value.name());
                let loc_bitcast_name = format!("{}_casted\0", mem_loc.name());
                let value_new_type =
                    LLVMPointerType(LLVMInt8Type(), 1 as c_uint);
                let target_new_type =
                    LLVMPointerType(value_new_type, 1 as c_uint);
                let value_bitcast = LLVMBuildBitCast(
                    llvm_internal_context.builder,
                    llvm_value,
                    value_new_type,
                    value_bitcast_name.as_str().as_ptr() as *const c_char
                );

                let mem_loc_bitcast = LLVMBuildBitCast(
                    llvm_internal_context.builder,
                    *llvm_mem_loc,
                    target_new_type,
                    loc_bitcast_name.as_str().as_ptr() as *const c_char
                );

                let write_barrier_fn = LLVMGetNamedFunction(
                    llvm_internal_context.module,
                    "llvm_write_barrier\0".as_ptr() as *const c_char
                );

                if write_barrier_fn.is_null() {
                    panic!("Function 'llvm_write_barrier' does not exist.");
                }

                // FIXME add order as parameter to write barrier
                let mut args = vec![value_bitcast, mem_loc_bitcast];

                let call_write_barrier = LLVMBuildCall(
                    llvm_internal_context.builder,
                    write_barrier_fn,
                    args.as_mut_ptr(),
                    args.len() as u32,
                    "\0".as_ptr() as *const c_char
                );

                return;
            }
        }
    }

    let store = LLVMBuildStore(
        llvm_internal_context.builder,
        llvm_value,
        *llvm_mem_loc
    );
    set_ordering(store, order);
}

pub unsafe fn gen_instr_load(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    mem_loc: &usize,
    _is_ptr: bool,
    order: MemoryOrder
) {
    let mem_loc = ins.ops.get(*mem_loc).unwrap();
    let ins_value = ins.value.clone().unwrap();
    if ins_value.len() != 1 {
        panic!("Unsupported operation.");
    };
    let id = ins_value.get(0).unwrap().hdr.id();
    let name = ins_value.get(0).unwrap().hdr.name().clone();
    let name = format!("{}\0", name);

    let llvm_mem_loc = gen_llvm_value(llvm_internal_context, mem_loc, store);

    let load = LLVMBuildLoad(
        llvm_internal_context.builder,
        llvm_mem_loc,
        name.as_str().as_ptr() as *const c_char
    );

    store.insert(id, load);
    set_ordering(load, order);
}

unsafe fn set_ordering(instr: LLVMValueRef, order: MemoryOrder) {
    if order == MemoryOrder::Relaxed {
        return;
    };

    // to address error in LLVM: atomic store must have explicit non-zero
    // alignment
    LLVMSetAlignment(instr, 1);
    match order {
        MemoryOrder::SeqCst => LLVMSetOrdering(
            instr,
            LLVMAtomicOrdering::LLVMAtomicOrderingSequentiallyConsistent
        ),
        MemoryOrder::Relaxed => {}
        MemoryOrder::Acquire => LLVMSetOrdering(
            instr,
            LLVMAtomicOrdering::LLVMAtomicOrderingAcquire
        ),
        MemoryOrder::AcqRel => LLVMSetOrdering(
            instr,
            LLVMAtomicOrdering::LLVMAtomicOrderingAcquireRelease
        ),
        MemoryOrder::Consume => LLVMSetOrdering(
            instr,
            LLVMAtomicOrdering::LLVMAtomicOrderingMonotonic
        ),
        MemoryOrder::NotAtomic => LLVMSetOrdering(
            instr,
            LLVMAtomicOrdering::LLVMAtomicOrderingNotAtomic
        ),
        MemoryOrder::Release => LLVMSetOrdering(
            instr,
            LLVMAtomicOrdering::LLVMAtomicOrderingRelease
        )
    }
}

pub unsafe fn gen_instr_select(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    cond: &usize,
    true_val: &usize,
    false_val: &usize
) {
    let cond = ins.ops.get(*cond).unwrap();
    let true_val = ins.ops.get(*true_val).unwrap();
    let false_val = ins.ops.get(*false_val).unwrap();
    let ins_value = ins.value.clone().unwrap();
    if ins_value.len() != 1 {
        panic!("Unsupported operation.");
    };
    let id = ins_value.get(0).unwrap().hdr.id();
    let name = ins_value.get(0).unwrap().hdr.name().clone();

    let llvm_cond = gen_llvm_value(llvm_internal_context, cond, store);
    let llvm_true_value =
        gen_llvm_value(llvm_internal_context, true_val, store);
    let llvm_false_value =
        gen_llvm_value(llvm_internal_context, false_val, store);

    let select = LLVMBuildSelect(
        llvm_internal_context.builder,
        llvm_cond,
        llvm_true_value,
        llvm_false_value,
        name.as_str().as_ptr() as *const c_char
    );
    store.insert(id, select);
}

pub unsafe fn gen_instr_set_ret_val(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    ret_val: &usize
) {
    let ret_val = ins.ops.get(*ret_val).unwrap();
    let function_name = CString::new("muentry_set_retval").unwrap();

    let llvm_function_name = LLVMGetNamedFunction(
        llvm_internal_context.module,
        function_name.as_ptr()
    );

    if llvm_function_name.is_null() {
        panic!("Could not find function.");
    }

    let set_ret_val_args = vec![ret_val];
    let mut llvm_typed_args =
        process_inst_args(llvm_internal_context, &set_ret_val_args, store);
    let mut llvm_args = vec![];

    for (_, llvm_arg) in llvm_typed_args.drain(..) {
        llvm_args.push(llvm_arg);
    }

    LLVMBuildCall(
        llvm_internal_context.builder,
        llvm_function_name,
        llvm_args.as_mut_ptr(),
        llvm_args.len() as c_uint,
        "\0".as_ptr() as *const c_char
    );
}

pub unsafe fn gen_instr_thread_exit(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    _store: &mut HashMap<MuID, LLVMValueRef>,
    _ins: &Instruction
) {
    let function_name = CString::new("muentry_get_thread_local").unwrap();

    let llvm_function_name = LLVMGetNamedFunction(
        llvm_internal_context.module,
        function_name.as_ptr()
    );

    if llvm_function_name.is_null() {
        panic!("Could not find function.");
    }

    let mut get_thread_local_args = vec![];

    let thread_local = LLVMBuildCall(
        llvm_internal_context.builder,
        llvm_function_name,
        get_thread_local_args.as_mut_ptr(),
        get_thread_local_args.len() as c_uint,
        "tl\0".as_ptr() as *const c_char
    );

    let function_name = CString::new("muentry_thread_exit").unwrap();

    let llvm_function_name = LLVMGetNamedFunction(
        llvm_internal_context.module,
        function_name.as_ptr()
    );

    if llvm_function_name.is_null() {
        panic!("Could not find function.");
    }

    let thread_local_pointer = LLVMBuildBitCast(
        llvm_internal_context.builder,
        thread_local,
        LLVMPointerType(LLVMPointerType(LLVMInt8Type(), 1), 1),
        "tl_ref\0".as_ptr() as *const c_char
    );

    // according to the offset native_sp_loc (thread::NATIVE_SP_LOC_OFFSET)
    let pos = [LLVMConstInt(LLVMInt8Type(), 29, false as i32)];

    let tl_address_ref = LLVMBuildGEP(
        llvm_internal_context.builder,
        thread_local_pointer,
        pos.as_ptr() as *mut LLVMValueRef,
        1,
        "tl_add_ref\0".as_ptr() as *const c_char
    );

    let tl_address = LLVMBuildLoad(
        llvm_internal_context.builder,
        tl_address_ref,
        "tl_address\0".as_ptr() as *const c_char
    );

    let tl_address_void = LLVMBuildAddrSpaceCast(
        llvm_internal_context.builder,
        tl_address,
        LLVMPointerType(LLVMInt8Type(), 0),
        "tl_add_void\0".as_ptr() as *const c_char
    );

    let mut llvm_args = vec![tl_address_void];

    LLVMBuildCall(
        llvm_internal_context.builder,
        llvm_function_name,
        llvm_args.as_mut_ptr(),
        llvm_args.len() as c_uint,
        "\0".as_ptr() as *const c_char
    );

    LLVMBuildUnreachable(llvm_internal_context.builder);
}

pub unsafe fn gen_instr_get_thread_local(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction
) {
    let function_name = CString::new("muentry_get_thread_local").unwrap();

    let llvm_function_name = LLVMGetNamedFunction(
        llvm_internal_context.module,
        function_name.as_ptr()
    );

    if llvm_function_name.is_null() {
        panic!("Could not find function.");
    }

    let mut get_thread_local_args = vec![];

    let thread_local = LLVMBuildCall(
        llvm_internal_context.builder,
        llvm_function_name,
        get_thread_local_args.as_mut_ptr(),
        get_thread_local_args.len() as c_uint,
        "tl\0".as_ptr() as *const c_char
    );

    let ins_value = ins.value.clone().unwrap();
    let mut id = 0;
    if ins_value.len() > 1 {
        panic!("Unsupported operation.");
    } else if ins_value.len() == 1 {
        id = ins_value.get(0).unwrap().hdr.id();
    }

    store.insert(id, thread_local);
}

pub unsafe fn gen_instr_new_thread(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    stack: &usize,
    thread_local: &Option<usize>,
    is_exception: &bool,
    args: &Vec<usize>
) {
    let setup_stack_args_fn = LLVMGetNamedFunction(
        llvm_internal_context.module,
        "muentry_stack_setup_args\0".as_ptr() as *const c_char
    );

    if setup_stack_args_fn.is_null() {
        panic!("Function has not been defined.");
    }

    let module_data_layout =
        LLVMGetModuleDataLayout(llvm_internal_context.module);
    let new_thread_alloc_fn = LLVMGetNamedFunction(
        llvm_internal_context.module,
        "calloc\0".as_ptr() as *const c_char
    );
    if new_thread_alloc_fn.is_null() {
        panic!("Function has not been defined.");
    }

    let stack = ins.ops.get(*stack).unwrap();
    let llvm_stack = gen_llvm_value(llvm_internal_context, stack, store);

    let mut llvm_stack_elm_type =
        vec![LLVMPointerType(LLVMInt8Type(), 1), LLVMInt32Type()];
    let llvm_stack_elm_type = LLVMStructType(
        llvm_stack_elm_type.as_mut_ptr(),
        llvm_stack_elm_type.len() as c_uint,
        false as i32
    );
    let llvm_stack_array_type =
        LLVMArrayType(llvm_stack_elm_type, args.len() as c_uint);

    let size = LLVMABISizeOfType(module_data_layout, llvm_stack_array_type);
    let new_thread_alloc_arg =
        LLVMConstInt(LLVMInt64Type(), size, false as i32);
    let mut new_thread_alloc_args = vec![
        LLVMConstInt(LLVMInt64Type(), 1, false as i32),
        new_thread_alloc_arg,
    ];

    let array_ptr = LLVMBuildCall(
        llvm_internal_context.builder,
        new_thread_alloc_fn,
        new_thread_alloc_args.as_mut_ptr(),
        new_thread_alloc_args.len() as u32,
        "a_ptr\0".as_ptr() as *const c_char
    );

    let array_ptr_cast = LLVMBuildBitCast(
        llvm_internal_context.builder,
        array_ptr,
        LLVMPointerType(llvm_stack_array_type, 1),
        "a_cast\0".as_ptr() as *const c_char
    );

    // FIXME passing other argument types to the stack
    for i in 0..args.len() {
        let arg = args.get(i).unwrap();
        let arg = ins.ops.get(*arg).unwrap();

        match &arg.as_value().v {
            Value_::Constant(c) => match c {
                Constant::Int(v) => {
                    let new_thread_alloc_arg =
                        LLVMConstInt(LLVMInt64Type(), size, false as i32);

                    let mut new_thread_alloc_args = vec![
                        LLVMConstInt(LLVMInt64Type(), 1, false as i32),
                        new_thread_alloc_arg,
                    ];

                    let new_thread_alloc_call = LLVMBuildCall(
                        llvm_internal_context.builder,
                        new_thread_alloc_fn,
                        new_thread_alloc_args.as_mut_ptr(),
                        new_thread_alloc_args.len() as u32,
                        "a_p\0".as_ptr() as *const c_char
                    );

                    let pointer_type = LLVMPointerType(LLVMInt64Type(), 1);
                    let new_thread_alloc_bitcast = LLVMBuildBitCast(
                        llvm_internal_context.builder,
                        new_thread_alloc_call,
                        pointer_type,
                        "arg\0".as_ptr() as *const c_char
                    );

                    LLVMBuildStore(
                        llvm_internal_context.builder,
                        LLVMConstInt(LLVMInt64Type(), *v, false as i32),
                        new_thread_alloc_bitcast
                    );

                    let type_code =
                        LLVMConstInt(LLVMInt32Type(), 1, false as i32);

                    let mut pos = vec![
                        LLVMConstInt(LLVMInt32Type(), 0, false as i32),
                        LLVMConstInt(LLVMInt32Type(), i as u64, false as i32),
                        LLVMConstInt(LLVMInt32Type(), 0, false as i32),
                    ];
                    let name = format!("arg_{}_v\0", i);

                    let gep = LLVMBuildInBoundsGEP(
                        llvm_internal_context.builder,
                        array_ptr_cast,
                        pos.as_mut_ptr(),
                        pos.len() as u32,
                        name.as_str().as_ptr() as *const c_char
                    );

                    LLVMBuildStore(
                        llvm_internal_context.builder,
                        new_thread_alloc_call,
                        gep
                    );

                    let mut pos = vec![
                        LLVMConstInt(LLVMInt32Type(), 0, false as i32),
                        LLVMConstInt(LLVMInt32Type(), i as u64, false as i32),
                        LLVMConstInt(LLVMInt32Type(), 1, false as i32),
                    ];
                    let name = format!("arg_{}_t\0", i);

                    let gep = LLVMBuildInBoundsGEP(
                        llvm_internal_context.builder,
                        array_ptr_cast,
                        pos.as_mut_ptr(),
                        pos.len() as u32,
                        name.as_str().as_ptr() as *const c_char
                    );

                    LLVMBuildStore(
                        llvm_internal_context.builder,
                        type_code,
                        gep
                    );
                }
                Constant::Float(_) => unimplemented!(),
                Constant::Double(_) => unimplemented!(),
                _ => unimplemented!()
            },
            _ => unimplemented!()
        };
    }

    let mut args = vec![
        llvm_stack,
        LLVMConstInt(LLVMInt32Type(), args.len() as u64, true as i32),
        array_ptr,
    ];

    LLVMBuildCall(
        llvm_internal_context.builder,
        setup_stack_args_fn,
        args.as_mut_ptr(),
        args.len() as u32,
        "\0".as_ptr() as *const c_char
    );

    let new_thread_fn = match is_exception {
        false => LLVMGetNamedFunction(
            llvm_internal_context.module,
            "muentry_new_thread_normal\0".as_ptr() as *const c_char
        ),
        true => {
            // FIXME call exceptional thread creation
            unimplemented!()
        }
    };

    if new_thread_fn.is_null() {
        panic!("Function has not been defined.");
    }

    let llvm_thread_local = match thread_local {
        None => LLVMConstNull(LLVMPointerType(LLVMInt8Type(), 1)),
        Some(v) => {
            let thread_local = ins.ops.get(*v).unwrap();
            gen_llvm_value(llvm_internal_context, thread_local, store)
        }
    };

    let mut args = vec![llvm_stack, llvm_thread_local];

    let ins_value = ins.value.clone().unwrap();
    let mut name = String::from("");
    let mut id = 0;
    if ins_value.len() > 1 {
        panic!("Unsupported operation.");
    } else if ins_value.len() == 1 {
        id = ins_value.get(0).unwrap().hdr.id();
        name = format!("{}\0", ins_value.get(0).unwrap().hdr.name());
    }

    let new_thread_call = LLVMBuildCall(
        llvm_internal_context.builder,
        new_thread_fn,
        args.as_mut_ptr(),
        args.len() as u32,
        name.as_str().as_ptr() as *const c_char
    );

    store.insert(id, new_thread_call);
}

pub unsafe fn gen_instr_binop_with_status(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    bop: &BinOp,
    bop_status: &BinOpStatus,
    op1: &usize,
    op2: &usize
) {
    let ret_value = ins.value.as_ref().unwrap().get(0).unwrap();
    let flag_n_pos = 1;
    let mut flag_z_pos = 1;
    let mut flag_c_pos = 1;
    let mut flag_v_pos = 1;
    if bop_status.flag_n {
        flag_z_pos += 1;
        flag_c_pos += 1;
        flag_v_pos += 1;
    }
    if bop_status.flag_z {
        flag_c_pos += 1;
        flag_v_pos += 1;
    }
    if bop_status.flag_c {
        flag_v_pos += 1;
    }

    let bin_op_ret_value = vec![ret_value.clone()];
    let mu_op1 = ins.ops.get(*op1).unwrap();
    let mu_op2 = ins.ops.get(*op2).unwrap();
    let llvm_op1 = gen_llvm_value(llvm_internal_context, mu_op1, store);
    let llvm_op2 = gen_llvm_value(llvm_internal_context, mu_op2, store);
    let op_type = mu_op1.as_value().ty.clone();
    let op_type_llvm = gen_llvm_type(llvm_internal_context, &op_type);

    let op_type_name = match op_type.v {
        MuType_::Int(n) => format!("i{}", n),
        _ => panic!("Operand should be integer.")
    };

    // overflow unsigned
    if bop_status.flag_c {
        let op_name = match bop {
            mu::ast::op::BinOp::Add => String::from("add"),
            mu::ast::op::BinOp::Sub => String::from("sub"),
            mu::ast::op::BinOp::Mul => String::from("mul"),
            _ => panic!("Unsupported operation.")
        };
        let function_name = MuName::new(format!(
            "llvm.u{}.with.overflow.{}",
            op_name, op_type_name
        ));
        let function_name =
            CString::new((*function_name).clone().as_str()).unwrap();

        let llvm_function = LLVMGetNamedFunction(
            llvm_internal_context.module,
            function_name.as_ptr()
        );

        if llvm_function.is_null() {
            panic!("Could not find function.");
        }

        let mut args = vec![llvm_op1, llvm_op2];
        let ret_value_name = format!("{}\0", ret_value.hdr.name());

        let call = LLVMBuildCall(
            llvm_internal_context.builder,
            llvm_function,
            args.as_mut_ptr(),
            2,
            "temp\0".as_ptr() as *const c_char
        );

        let sum = LLVMBuildExtractValue(
            llvm_internal_context.builder,
            call,
            0,
            ret_value_name.as_str().as_ptr() as *const c_char
        );

        let flag = LLVMBuildExtractValue(
            llvm_internal_context.builder,
            call,
            1,
            "c_flag\0".as_ptr() as *const c_char
        );

        let flag_mu = ins.value.as_ref().unwrap().get(flag_c_pos).unwrap();

        store.insert(ret_value.hdr.id(), sum);
        store.insert(flag_mu.hdr.id(), flag);
    }

    // overflow signed
    if bop_status.flag_v {
        let op_name = match bop {
            mu::ast::op::BinOp::Add => String::from("add"),
            mu::ast::op::BinOp::Sub => String::from("sub"),
            mu::ast::op::BinOp::Mul => String::from("mul"),
            _ => panic!("Unsupported operation.")
        };
        let function_name = MuName::new(format!(
            "llvm.s{}.with.overflow.{}",
            op_name, op_type_name
        ));
        let function_name =
            CString::new((*function_name).clone().as_str()).unwrap();

        let llvm_function = LLVMGetNamedFunction(
            llvm_internal_context.module,
            function_name.as_ptr()
        );

        if llvm_function.is_null() {
            panic!("Could not find function.");
        }

        let mut args = vec![llvm_op1, llvm_op2];
        let ret_value_name = format!("{}\0", ret_value.hdr.name());

        let call = LLVMBuildCall(
            llvm_internal_context.builder,
            llvm_function,
            args.as_mut_ptr(),
            2,
            "temp\0".as_ptr() as *const c_char
        );

        let sum = LLVMBuildExtractValue(
            llvm_internal_context.builder,
            call,
            0,
            ret_value_name.as_str().as_ptr() as *const c_char
        );

        let flag = LLVMBuildExtractValue(
            llvm_internal_context.builder,
            call,
            1,
            "v_flag\0".as_ptr() as *const c_char
        );

        let flag_mu = ins.value.as_ref().unwrap().get(flag_v_pos).unwrap();

        store.insert(ret_value.hdr.id(), sum);
        store.insert(flag_mu.hdr.id(), flag);
    }

    // if no overflow flags are requested do regular BOP operation to check N
    // and Z
    if !bop_status.flag_c && !bop_status.flag_v {
        let op1_mu = ins.ops.get(*op1).unwrap().clone();
        let op2_mu = ins.ops.get(*op2).unwrap().clone();
        let mut ops = vec![];
        ops.insert(*op1, op1_mu);
        ops.insert(*op2, op2_mu);

        // generate and process binop instruction
        let bin_op_ins = Instruction {
            hdr: MuEntityHeader::unnamed(0),
            value: Some(bin_op_ret_value),
            ops,
            v: Instruction_::BinOp(bop.clone(), *op1, *op2)
        };

        gen_instr_binop(
            llvm_internal_context,
            store,
            &bin_op_ins,
            bop,
            op1,
            op2
        );
    }

    // negative flag
    if bop_status.flag_n {
        let sum = store.get(&ret_value.hdr.id()).unwrap();

        let zero_const = LLVMConstInt(op_type_llvm, 0, false as i32);

        let flag = LLVMBuildICmp(
            llvm_internal_context.builder,
            LLVMIntPredicate::LLVMIntSLT,
            *sum,
            zero_const,
            "n_flag\0".as_ptr() as *const c_char
        );

        let flag_mu = ins.value.as_ref().unwrap().get(flag_n_pos).unwrap();

        store.insert(flag_mu.hdr.id(), flag);
    }

    // zero flag
    if bop_status.flag_z {
        let sum = store.get(&ret_value.hdr.id()).unwrap();

        let zero_const = LLVMConstInt(op_type_llvm, 0, false as i32);

        let flag = LLVMBuildICmp(
            llvm_internal_context.builder,
            LLVMIntPredicate::LLVMIntEQ,
            *sum,
            zero_const,
            "z_flag\0".as_ptr() as *const c_char
        );

        let flag_mu = ins.value.as_ref().unwrap().get(flag_z_pos).unwrap();

        store.insert(flag_mu.hdr.id(), flag);
    }
}

pub unsafe fn gen_instr_convop(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    operation: &ConvOp,
    _from_ty: &P<MuType>,
    to_ty: &P<MuType>,
    operand: &usize
) {
    let operand_mu = ins.ops.get(*operand).unwrap().clone();
    info!("operand mu: {:?}", operand_mu);
    let operand_llvm =
        gen_llvm_value(llvm_internal_context, &operand_mu, store);
    let dest_type_llvm = gen_llvm_type(llvm_internal_context, to_ty);

    let llvm_op = match operation {
        ConvOp::ZEXT => LLVMOpcode::LLVMZExt,
        ConvOp::FPEXT => LLVMOpcode::LLVMFPExt,
        ConvOp::BITCAST => LLVMOpcode::LLVMBitCast,
        ConvOp::TRUNC => LLVMOpcode::LLVMTrunc,
        ConvOp::FPTRUNC => LLVMOpcode::LLVMFPTrunc,
        ConvOp::UITOFP => LLVMOpcode::LLVMUIToFP,
        ConvOp::SITOFP => LLVMOpcode::LLVMSIToFP,
        ConvOp::FPTOUI => LLVMOpcode::LLVMFPToUI,
        ConvOp::FPTOSI => LLVMOpcode::LLVMFPToSI,
        ConvOp::REFCAST => LLVMOpcode::LLVMBitCast,
        ConvOp::SEXT => LLVMOpcode::LLVMSExt,
        ConvOp::PTRCAST => match to_ty.v {
            MuType_::Int(_) => LLVMOpcode::LLVMPtrToInt,
            MuType_::UPtr(_) | MuType_::IRef(_) | MuType_::Ref(_) => {
                let llvm_type_val = LLVMTypeOf(operand_llvm);
                let type_llvm: *mut ::libc::c_char =
                    LLVMPrintTypeToString(llvm_type_val);
                let c_str: &CStr = CStr::from_ptr(type_llvm);
                let str_slice: &str = c_str.to_str().unwrap();
                let str_buf: String = str_slice.to_owned();
                info!("LLVM operand Type: {}", str_buf);

                match _from_ty.v {
                    MuType_::Int(_) => LLVMOpcode::LLVMIntToPtr,
                    MuType_::UPtr(_) | MuType_::IRef(_) | MuType_::Ref(_) => {
                        LLVMOpcode::LLVMBitCast
                    }
                    _ => panic!("Type should be reference type or integer.")
                }
            }
            _ => panic!("Type should be reference type or integer.")
        }
    };

    let ins_value = ins.value.clone().unwrap();
    let mut name = Arc::new(String::from(""));
    let mut id = 0;
    if ins_value.len() > 1 {
        panic!("Unsupported operation.");
    } else if ins_value.len() == 1 {
        id = ins_value.get(0).unwrap().hdr.id();
        name = Arc::new(format!(
            "{}\0",
            ins_value.get(0).unwrap().hdr.name().clone()
        ));
    }

    info!("Operator code: {:?}", llvm_op);

    let cast = LLVMBuildCast(
        llvm_internal_context.builder,
        llvm_op,
        operand_llvm,
        dest_type_llvm,
        name.as_str().as_ptr() as *const c_char
    );

    if name.as_str() != "" {
        store.insert(id, cast);
    };
}

pub unsafe fn gen_instr_move(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    op: &usize
) {
    let src = gen_llvm_value(
        llvm_internal_context,
        &ins.ops.get(*op).unwrap().clone(),
        store
    );

    let ins_value = ins.value.clone().unwrap();
    let mut id = 0;
    if ins_value.len() > 1 {
        panic!("Unsupported operation.");
    } else if ins_value.len() == 1 {
        id = ins_value.get(0).unwrap().hdr.id();
    }

    store.insert(id, src);
}

pub unsafe fn gen_instr_printhex(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    op: &usize
) {
    let function_name = CString::new("muentry_print_hex").unwrap();

    let llvm_function_name = LLVMGetNamedFunction(
        llvm_internal_context.module,
        function_name.as_ptr()
    );

    if llvm_function_name.is_null() {
        panic!("Could not find function.");
    }

    let llvm_op = gen_llvm_value(
        llvm_internal_context,
        &ins.ops.get(*op).unwrap().clone(),
        store
    );

    let mut print_hex_args = vec![llvm_op];

    LLVMBuildCall(
        llvm_internal_context.builder,
        llvm_function_name,
        print_hex_args.as_mut_ptr(),
        print_hex_args.len() as c_uint,
        "\0".as_ptr() as *const c_char
    );
}

pub unsafe fn gen_instr_throw(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    op: &usize
) {
    let allocate_exception_fn = LLVMGetNamedFunction(
        llvm_internal_context.module,
        "__cxa_allocate_exception\0".as_ptr() as *const c_char
    );
    if allocate_exception_fn.is_null() {
        panic!("function does not exist");
    }

    let exception_mu = ins.ops.get(*op).unwrap().clone();
    let exception = gen_llvm_value(llvm_internal_context, &exception_mu, store);

    let exception_type = exception_mu.ty();
    let exception_llvm_type = LLVMPointerType(
        gen_llvm_type(llvm_internal_context, &exception_type),
        1
    );
    let target_data = LLVMGetModuleDataLayout(llvm_internal_context.module);
    let size = LLVMABISizeOfType(target_data, exception_llvm_type);
    let mut args = vec![LLVMConstInt(LLVMInt64Type(), size, false as i32)];

    let exception_loc = LLVMBuildCall(
        llvm_internal_context.builder,
        allocate_exception_fn,
        args.as_mut_ptr(),
        1,
        "exc\0".as_ptr() as *const c_char
    );

    let exception_loc_cast = LLVMBuildBitCast(
        llvm_internal_context.builder,
        exception_loc,
        exception_llvm_type,
        "exc_cast\0".as_ptr() as *const c_char
    );

    LLVMBuildStore(
        llvm_internal_context.builder,
        exception,
        exception_loc_cast
    );

    let throw_fn = LLVMGetNamedFunction(
        llvm_internal_context.module,
        "__cxa_throw\0".as_ptr() as *const c_char
    );

    if throw_fn.is_null() {
        panic!("function does not exist");
    }

    let null_const = LLVMConstNull(LLVMPointerType(LLVMInt8Type(), 1));

    let mut args = vec![exception_loc, null_const.clone(), null_const];

    LLVMBuildCall(
        llvm_internal_context.builder,
        throw_fn,
        args.as_mut_ptr(),
        3,
        "\0".as_ptr() as *const c_char
    );

    LLVMBuildUnreachable(llvm_internal_context.builder);
}

pub unsafe fn gen_instr_new_stack(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    store: &mut HashMap<MuID, LLVMValueRef>,
    ins: &Instruction,
    op: &usize
) {
    let func_ref = ins.ops.get(*op).unwrap().clone();
    let func_ref_llvm = gen_llvm_value(llvm_internal_context, &func_ref, store);

    let sig = match &func_ref.as_value().ty.v {
        MuType_::FuncRef(ref sig) => sig.clone(),
        _ => panic!("expected funcref")
    };

    let tmp_stack_arg_size = {
        use mu::compiler::backend::x86_64::callconv::swapstack;
        let (size, _) = swapstack::compute_stack_args(
            &sig.arg_tys,
            llvm_internal_context.vm
        );
        size
    };

    let func_ref_bitcast = LLVMBuildAddrSpaceCast(
        llvm_internal_context.builder,
        func_ref_llvm,
        LLVMPointerType(LLVMInt8Type(), 1),
        "fn_arg\0".as_ptr() as *const c_char
    );

    let stack_size =
        LLVMConstInt(LLVMInt64Type(), tmp_stack_arg_size as u64, false as i32);

    let mu_new_stack = LLVMGetNamedFunction(
        llvm_internal_context.module,
        "muentry_new_stack\0".as_ptr() as *const c_char
    );

    if mu_new_stack.is_null() {
        panic!("Function has not been defined.");
    }

    let mut args = vec![func_ref_bitcast, stack_size];

    let ins_value = ins.value.clone().unwrap();
    let mut name = String::from("\0");
    let mut id = 0;
    if ins_value.len() > 1 {
        panic!("Unsupported operation.");
    } else if ins_value.len() == 1 {
        id = ins_value.get(0).unwrap().hdr.id();
        name = format!("{}\0", ins_value.get(0).unwrap().hdr.name());
    }

    let new_stack = LLVMBuildCall(
        llvm_internal_context.builder,
        mu_new_stack,
        args.as_mut_ptr(),
        args.len() as u32,
        name.as_str().as_ptr() as *const c_char
    );

    store.insert(id, new_stack);
}

pub unsafe fn process_inst_args(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    ops: &Vec<&P<TreeNode>>,
    store: &mut HashMap<MuID, LLVMValueRef>
) -> Vec<(LLVMTypeRef, LLVMValueRef)> {
    let mut llvm_args = vec![];

    for op in ops {
        if op.is_value() {
            let value = op.as_value();
            let value_type = gen_llvm_type(llvm_internal_context, &value.ty);

            let value_llvm = gen_llvm_value(llvm_internal_context, *op, store);

            llvm_args.push((value_type, value_llvm));
        } else {
            // TODO handle non-const arguments
            unimplemented!()
        }
    }

    llvm_args
}

pub unsafe fn process_inst_exc_args(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    ops: &Vec<&P<TreeNode>>,
    store: &mut HashMap<MuID, LLVMValueRef>
) -> Vec<(LLVMTypeRef, LLVMValueRef)> {
    let mut llvm_args = vec![];

    for op in ops {
        if op.is_value() {
            let value = op.as_value();
            let value_type = gen_llvm_type(llvm_internal_context, &value.ty);
            let value_llvm = gen_llvm_value(llvm_internal_context, *op, store);

            llvm_args.push((value_type, value_llvm));
        } else {
            // TODO handle non-const arguments
            unimplemented!()
        }
    }

    llvm_args
}

pub unsafe fn process_inst_args_with_select(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    cond: LLVMValueRef,
    true_args: &Vec<&P<TreeNode>>,
    false_args: &Vec<&P<TreeNode>>,
    store: &mut HashMap<MuID, LLVMValueRef>
) -> Vec<(LLVMTypeRef, LLVMValueRef)> {
    let mut llvm_args = vec![];

    for (op_true, op_false) in true_args.iter().zip(false_args) {
        if op_true.is_value() && op_false.is_value() {
            let true_value = op_true.as_value();
            let true_value_type =
                gen_llvm_type(llvm_internal_context, &true_value.ty);

            let true_value_llvm =
                gen_llvm_value(llvm_internal_context, *op_true, store);

            let false_value = op_false.as_value();
            let false_value_type =
                gen_llvm_type(llvm_internal_context, &false_value.ty);

            assert_eq!(true_value_type, false_value_type);

            let false_value_llvm =
                gen_llvm_value(llvm_internal_context, *op_false, store);

            let result = LLVMBuildSelect(
                llvm_internal_context.builder,
                cond,
                true_value_llvm,
                false_value_llvm,
                "temp_arg\0".as_ptr() as *const c_char
            );

            llvm_args.push((true_value_type, result));
        } else {
            // TODO handle non-const arguments
            unimplemented!()
        }
    }

    llvm_args
}
