// Copyright 2017 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

pub use crate::llvm_backend;
use mu::compiler::CompilerPolicy;
pub use mu::compiler::*;

use std::collections::HashMap;

use mu::ast::ir::*;
use mu::compiler::backend::x86_64::callconv::CallConvResult::STACK;
use mu::utils::*;
use mu::vm::handle::*;
use mu::vm::*;
use std::path::Path;

pub fn llvm(wl_fns: Vec<MuID>) -> CompilerPolicy {
    let mut passes: Vec<Box<dyn CompilerPass>> = vec![];
    passes.push(Box::new(passes::UIRGen::new("")));
    passes.push(Box::new(passes::ControlFlowAnalysis::new()));

    // ir level passes
    //        passes.push(Box::new(passes::RetSink::new()));
    //        passes.push(Box::new(passes::Inlining::new()));
    //        passes.push(Box::new(passes::InjectRuntime::new()));
    //        passes.push(Box::new(passes::DefUse::new()));
    //        passes.push(Box::new(passes::TreeGen::new()));
    //        passes.push(Box::new(passes::GenMovPhi::new()));
    //
    //        passes.push(Box::new(passes::TraceGen::new()));
    //        passes.push(Box::new(passes::DotGen::new(".transformed")));

    // llvm compilation
    passes.push(Box::new(llvm_backend::LLVMCodeEmission::new(wl_fns)));

    CompilerPolicy { passes: passes }
}

/// makes a boot image using LLVM generated code
/// We are basically following the spec for this API calls.
/// However, there are a few differences:
/// 1. we are not doing 'automagic' relocation for unsafe pointers,
/// relocation of    unsafe pointers needs to be done via explicit
/// sym_fields/strings, reloc_fields/strings 2. if the output name for
/// the boot image has extension name for dynamic libraries    (.so or
/// .dylib), we generate a dynamic library as boot image. Otherwise, we
/// generate    an executable.
/// 3. we do not support primordial stack (as Kunshan pointed out, making
/// boot image with a    primordial stack may get deprecated)
///
/// args:
/// whitelist               : functions to be put into the boot image
/// primordial_func         : starting function for the boot image
/// primordial_stack        : starting stack for the boot image
///                           (client should name either primordial_func or
/// stack,                            currently Zebu only supports func)
/// primordial_threadlocal  : thread local for the starting thread
/// sym_fields/strings      : declare an address with symbol
/// reloc_fields/strings    : declare an field pointing to a symbol
/// output_file             : path for the boot image
pub fn make_boot_image(
    vm: &VM,
    whitelist: Vec<MuID>,
    primordial_func: Option<&APIHandle>,
    primordial_stack: Option<&APIHandle>,
    primordial_threadlocal: Option<&APIHandle>,
    sym_fields: Vec<&APIHandle>,
    sym_strings: Vec<MuName>,
    reloc_fields: Vec<&APIHandle>,
    reloc_strings: Vec<MuName>,
    output_file: String
) {
    make_boot_image_internal(
        vm,
        whitelist,
        primordial_func,
        primordial_stack,
        primordial_threadlocal,
        sym_fields,
        sym_strings,
        reloc_fields,
        reloc_strings,
        vec![],
        output_file,
        false
    )
}

/// makes a boot image using LLVM generated code
/// We are basically following the spec for this API calls.
/// However, there are a few differences:
/// 1. we are not doing 'automagic' relocation for unsafe pointers,
/// relocation of    unsafe pointers needs to be done via explicit
/// sym_fields/strings, reloc_fields/strings 2. if the output name for
/// the boot image has extension name for dynamic libraries    (.so or
/// .dylib), we generate a dynamic library as boot image. Otherwise, we
/// generate    an executable.
/// 3. we do not support primordial stack (as Kunshan pointed out, making
/// boot image with a    primordial stack may get deprecated)
///
/// args:
/// whitelist               : functions to be put into the boot image
/// primordial_func         : starting function for the boot image
/// primordial_stack        : starting stack for the boot image
///                           (client should name either primordial_func or
/// stack,                            currently Zebu only supports func)
/// primordial_threadlocal  : thread local for the starting thread
/// sym_fields/strings      : declare an address with symbol
/// reloc_fields/strings    : declare an field pointing to a symbol
/// output_file             : path for the boot image
pub fn make_boot_image_for_testing(
    vm: &VM,
    whitelist: Vec<MuID>,
    primordial_func: Option<&APIHandle>,
    primordial_stack: Option<&APIHandle>,
    primordial_threadlocal: Option<&APIHandle>,
    sym_fields: Vec<&APIHandle>,
    sym_strings: Vec<MuName>,
    reloc_fields: Vec<&APIHandle>,
    reloc_strings: Vec<MuName>,
    output_file: String
) {
    make_boot_image_internal(
        vm,
        whitelist,
        primordial_func,
        primordial_stack,
        primordial_threadlocal,
        sym_fields,
        sym_strings,
        reloc_fields,
        reloc_strings,
        vec![],
        output_file,
        true
    )
}

/// the actual function to make boot image
/// One difference from the public one is that we allow linking extra source
/// code during generating the boot image.
#[allow(unused_variables)]
pub fn make_boot_image_internal(
    vm: &VM,
    whitelist: Vec<MuID>,
    primordial_func: Option<&APIHandle>,
    primordial_stack: Option<&APIHandle>,
    primordial_threadlocal: Option<&APIHandle>,
    sym_fields: Vec<&APIHandle>,
    sym_strings: Vec<MuName>,
    reloc_fields: Vec<&APIHandle>,
    reloc_strings: Vec<MuName>,
    extra_sources_to_link: Vec<String>,
    output_file: String,
    is_test: bool
) {
    info!("Making boot image...");

    mu::vm::uir_output::emit_uir("", vm);
    // Only store name info for whitelisted entities
    {
        let mut new_id_name_map =
            HashMap::<MuID, MuName>::with_capacity(whitelist.len());
        let mut new_name_id_map =
            HashMap::<MuName, MuID>::with_capacity(whitelist.len());

        let mut id_name_map = vm.id_name_map.write().unwrap();
        let mut name_id_map = vm.name_id_map.write().unwrap();
        for &id in whitelist.iter() {
            match id_name_map.get(&id) {
                Some(name) => {
                    new_id_name_map.insert(id, name.clone());
                    new_name_id_map.insert(name.clone(), id);
                }
                None => {}
            }
        }
        *id_name_map = new_id_name_map;
        *name_id_map = new_name_id_map;
    }

    let mut whitelist_funcs_ids = vec![];
    let funcs = vm.funcs().read().unwrap();
    for &id in whitelist.iter() {
        if let Some(f) = funcs.get(&id) {
            whitelist_funcs_ids.push(id);
        }
    }

    // compile the whitelist functions
    let whitelist_funcs = {
        let compiler = Compiler::new(llvm(whitelist_funcs_ids.clone()), &vm);
        let funcs = vm.funcs().read().unwrap();
        let func_vers = vm.func_vers().read().unwrap();

        // make sure all functions in whitelist are compiled
        let mut whitelist_funcs: Vec<MuID> = vec![];
        for &id in whitelist.iter() {
            if let Some(f) = funcs.get(&id) {
                whitelist_funcs.push(id);

                let f: &MuFunction = &f.read().unwrap();
                match f.cur_ver {
                    Some(fv_id) => {
                        let mut func_ver =
                            func_vers.get(&fv_id).unwrap().write().unwrap();
                        if !func_ver.is_compiled() {
                            compiler.compile(&mut func_ver);
                        }
                    }
                    None => error!(
                        "whitelist function {} has no version defined",
                        f
                    )
                }
            }
        }

        whitelist_funcs
    };

    let has_primordial_func = primordial_func.is_some();
    let has_primordial_stack = primordial_stack.is_some();

    // we assume client will start with a function (instead of a stack)
    if has_primordial_stack {
        panic!(
            "Zebu doesnt support creating primordial thread from a stack, \
             name a entry function instead"
        )
    } else {
        if has_primordial_func {
            // extract func id
            let func_id = primordial_func.unwrap().v.as_funcref();

            // make primordial thread in vm
            // no const args are passed, we will use argc/argv
            vm.set_primordial_thread(func_id, false, vec![]);
        } else {
            warn!("no entry function is passed");
        }

        // deal with relocation symbols, zip the two vectors into a hashmap
        assert_eq!(sym_fields.len(), sym_strings.len());
        let symbols: HashMap<Address, MuName> = sym_fields
            .into_iter()
            .map(|handle| handle.v.as_address())
            .zip(sym_strings.into_iter())
            .collect();

        // deal with relocation fields
        // zip the two vectors into a hashmap, and add fields for pending
        // funcref stores
        assert_eq!(reloc_fields.len(), reloc_strings.len());
        let fields = {
            // init reloc fields with client-supplied field/symbol pair
            let mut reloc_fields: HashMap<Address, MuName> = reloc_fields
                .into_iter()
                .map(|handle| handle.v.as_address())
                .zip(reloc_strings.into_iter())
                .collect();

            // pending funcrefs - we want to replace them as symbol
            {
                let mut pending_funcref =
                    vm.aot_pending_funcref_store.write().unwrap();
                for (addr, vl) in pending_funcref.drain() {
                    reloc_fields.insert(addr, vl.to_relocatable());
                }
            }

            reloc_fields
        };

        // emit context (persist vm, etc)
        backend::emit_context_with_reloc(
            vm,
            symbols,
            fields,
            primordial_threadlocal.map(|x| x.v.as_ref().1)
        );

        let out_file = output_file.clone();

        // link
        if is_test {
            link_boot_image_for_testing(
                vm,
                whitelist_funcs,
                extra_sources_to_link,
                output_file
            );
        } else {
            vm.link_boot_image(
                whitelist_funcs,
                extra_sources_to_link,
                output_file,
                true
            );
        }
    }
}

fn link_boot_image_for_testing(
    vm: &VM,
    funcs: Vec<MuID>,
    extra_srcs: Vec<String>,
    output_file: String
) {
    info!("Linking boot image...");

    let func_names = {
        let funcs_guard = vm.funcs().read().unwrap();
        funcs
            .iter()
            .map(|x| funcs_guard.get(x).unwrap().read().unwrap().name())
            .collect()
    };

    trace!("functions: {:?}", func_names);
    trace!("extern sources: {:?}", extra_srcs);
    trace!("output   : {}", output_file);

    if output_file.ends_with("dylib") || output_file.ends_with("so") {
        // compile as dynamic library
        mu::linkutils::aot::link_dylib_with_extra_srcs(
            func_names,
            extra_srcs,
            &output_file,
            vm,
            true
        );
    } else {
        if extra_srcs.len() != 0 {
            panic!("trying to create an executable with linking extern sources, unimplemented");
        }
        // compile as executable
        mu::linkutils::aot::link_test_primordial(
            func_names,
            &output_file,
            vm,
            true
        );
    }

    trace!("Done!");
}
