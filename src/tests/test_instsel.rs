// Copyright 2017 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate mu;

use self::mu::vm::VM;
use crate::aot_mu;
use crate::tests::test_ir::test_ir::factorial;
use std::sync::Arc;

#[test]
fn test_instsel_fac() {
    VM::start_logging_trace();

    let vm = Arc::new(factorial());

    let mut whitelist = vec![];
    let func_id = vm.id_of("fac");
    whitelist.push(func_id);
    let primordial_func = vm.handle_from_func(func_id);

    vm.set_primordial_thread(func_id, true, vec![]);
    let image_name = String::from("fac");

    aot_mu::make_boot_image_for_testing(
        &vm,
        whitelist,
        Some(&*primordial_func),
        None,
        None,
        Vec::new(),
        Vec::new(),
        Vec::new(),
        Vec::new(),
        image_name.clone()
    );
}
