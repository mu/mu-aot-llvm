// Copyright 2017 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate libloading;
extern crate log;
extern crate mu;

use self::mu::ast::inst::*;
use self::mu::ast::ir::*;
use self::mu::ast::op::*;
use self::mu::ast::types::*;
use self::mu::runtime::thread::MuThread;
use self::mu::utils::Address;
use self::mu::utils::LinkedHashMap;
use self::mu::vm::*;
use crate::aot_mu;
use crate::tests::test_call::gen_ccall_exit;
use mu::linkutils;
use std::path::PathBuf;

use crate::tests::create_dyn_lib;
use std::sync::Arc;

#[test]
fn test_gc_single_call_single_obj() {
    build_and_run_llvm_test!(
        gc_single_call_single_obj,
        gc_single_call_single_obj_test1
    );
}

fn gc_single_call_single_obj() -> VM {
    let opts = String::from("init_mu --generate-llvm");
    let vm = VM::new_with_opts(opts.as_str());

    typedef!    ((vm) int1         = mu_int(1));
    typedef!    ((vm) int64        = mu_int(64));
    typedef!    ((vm) ref_int64    = mu_ref(int64));
    typedef!    ((vm) struct_t     = mu_struct(int64, int64, ref_int64));
    typedef!    ((vm) ref_struct_t = mu_ref(struct_t));

    funcsig!    ((vm) sig = () -> ());
    funcdecl!   ((vm) <sig> gc_single_call_single_obj);
    funcdef!    ((vm) <sig> gc_single_call_single_obj VERSION gc_single_call_single_obj_v1);

    block!      ((vm, gc_single_call_single_obj_v1) blk_entry);

    // a = NEW <struct_t>
    ssa!        ((vm, gc_single_call_single_obj_v1) <ref_struct_t> a);
    inst!       ((vm, gc_single_call_single_obj_v1) blk_entry_new1:
        a = NEW <struct_t>
    );

    ssa! ((vm, gc_single_call_single_obj_v1) <int64> i);
    inst!       ((vm, gc_single_call_single_obj_v1) blk_entry_convop:
        i = CONVOP (ConvOp::PTRCAST) <ref_struct_t int64> a
    );

    inst!       ((vm, gc_single_call_single_obj_v1) blk_entry_print1:
        PRINTHEX i
    );

    ssa!        ((vm, gc_single_call_single_obj_v1) <ref_struct_t> b);
    inst!       ((vm, gc_single_call_single_obj_v1) blk_entry_new2:
        b = NEW <struct_t>
    );

    ssa! ((vm, gc_single_call_single_obj_v1) <int64> i2);
    inst!       ((vm, gc_single_call_single_obj_v1) blk_entry_convop2:
        i2 = CONVOP (ConvOp::PTRCAST) <ref_struct_t int64> b
    );

    inst!       ((vm, gc_single_call_single_obj_v1) blk_entry_print2:
        PRINTHEX i2
    );

    inst!       ((vm, gc_single_call_single_obj_v1) blk_entry_threadexit:
        THREADEXIT
    );

    define_block!   ((vm, gc_single_call_single_obj_v1) blk_entry() {
        blk_entry_new1, blk_entry_convop,
        blk_entry_new2,
        blk_entry_print1, blk_entry_convop2, blk_entry_print2,
        blk_entry_threadexit
    });

    define_func_ver!((vm) gc_single_call_single_obj_v1 (entry: blk_entry) {blk_entry});

    emit_test! ((vm)
        gc_single_call_single_obj, gc_single_call_single_obj_test1, gc_single_call_single_obj_test1_v1,
        sig,
    );

    vm
}

#[test]
fn test_gc_single_call_mult_obj() {
    build_and_run_llvm_test!(
        gc_single_call_mult_obj,
        gc_single_call_mult_obj_test1
    );
}

fn gc_single_call_mult_obj() -> VM {
    let opts = String::from("init_mu --generate-llvm");
    let vm = VM::new_with_opts(opts.as_str());

    typedef!    ((vm) int1         = mu_int(1));
    typedef!    ((vm) int64        = mu_int(64));
    typedef!    ((vm) ref_int64    = mu_ref(int64));
    typedef!    ((vm) struct_t     = mu_struct(int64, int64, ref_int64));
    typedef!    ((vm) ref_struct_t = mu_ref(struct_t));

    funcsig!    ((vm) sig = () -> ());
    funcdecl!   ((vm) <sig> gc_single_call_mult_obj);
    funcdef!    ((vm) <sig> gc_single_call_mult_obj VERSION gc_single_call_mult_obj_v1);

    block!      ((vm, gc_single_call_mult_obj_v1) blk_entry);

    // a = NEW <struct_t>
    ssa!        ((vm, gc_single_call_mult_obj_v1) <ref_struct_t> a);
    inst!       ((vm, gc_single_call_mult_obj_v1) blk_entry_new1:
        a = NEW <struct_t>
    );

    ssa! ((vm, gc_single_call_mult_obj_v1) <int64> i);
    inst!       ((vm, gc_single_call_mult_obj_v1) blk_entry_convop:
        i = CONVOP (ConvOp::PTRCAST) <ref_struct_t int64> a
    );

    ssa!        ((vm, gc_single_call_mult_obj_v1) <ref_struct_t> b);
    inst!       ((vm, gc_single_call_mult_obj_v1) blk_entry_new2:
        b = NEW <struct_t>
    );

    inst!       ((vm, gc_single_call_mult_obj_v1) blk_entry_print1:
        PRINTHEX i
    );

    // c = LOAD a
    ssa!        ((vm, gc_single_call_mult_obj_v1) <struct_t> c);
    inst!       ((vm, gc_single_call_mult_obj_v1) blk_entry_load_a:
        c = LOAD a (is_ptr: true, order: MemoryOrder::Relaxed)
    );

    ssa! ((vm, gc_single_call_mult_obj_v1) <int64> i2);
    inst!       ((vm, gc_single_call_mult_obj_v1) blk_entry_convop2:
        i2 = CONVOP (ConvOp::PTRCAST) <ref_struct_t int64> b
    );

    inst!       ((vm, gc_single_call_mult_obj_v1) blk_entry_print2:
        PRINTHEX i2
    );

    inst!       ((vm, gc_single_call_mult_obj_v1) blk_entry_threadexit:
        THREADEXIT
    );

    define_block!   ((vm, gc_single_call_mult_obj_v1) blk_entry() {
        blk_entry_new1, blk_entry_convop,
        blk_entry_new2,
        blk_entry_print1, blk_entry_load_a, blk_entry_convop2, blk_entry_print2,
        blk_entry_threadexit
    });

    define_func_ver!((vm) gc_single_call_mult_obj_v1 (entry: blk_entry) {blk_entry});

    emit_test! ((vm)
        gc_single_call_mult_obj, gc_single_call_mult_obj_test1, gc_single_call_mult_obj_test1_v1,
        sig,
    );

    vm
}

#[test]
fn test_gc_mult_call_single_obj_no_gc() {
    build_and_run_llvm_test!(
        gc_mult_call_single_obj AND foo,
        gc_mult_call_single_obj_test1
    );
}

fn gc_mult_call_single_obj() -> VM {
    let opts = String::from("init_mu --generate-llvm");
    let vm = VM::new_with_opts(opts.as_str());

    typedef!    ((vm) int64        = mu_int(64));
    typedef!    ((vm) ref_int64    = mu_ref(int64));
    funcsig!    ((vm) foo_sig = () -> ());
    funcdecl!   ((vm) <foo_sig> foo);

    {
        // add
        funcdef!    ((vm) <foo_sig> foo VERSION foo_v1);

        block!      ((vm, foo_v1) blk_entry);
        ssa!        ((vm, foo_v1) <ref_int64> x);
        inst!       ((vm, foo_v1) blk_entry_new:
            x = NEW <int64>
        );

        ssa! ((vm, foo_v1) <int64> i);
        inst!       ((vm, foo_v1) blk_entry_convop:
            i = CONVOP (ConvOp::PTRCAST) <ref_int64 int64> x
        );

        inst!       ((vm, foo_v1) blk_entry_print1:
            PRINTHEX i
        );

        ssa!        ((vm, foo_v1) <int64> y);
        inst!       ((vm, foo_v1) blk_entry_load_x:
            y = LOAD x (is_ptr: true, order: MemoryOrder::Relaxed)
        );

        inst!       ((vm, foo_v1) blk_entry_ret:
            RET
        );

        define_block!   ((vm, foo_v1) blk_entry() {
            blk_entry_new, blk_entry_convop, blk_entry_print1,
            blk_entry_load_x, blk_entry_ret
        });

        define_func_ver!((vm) foo_v1 (entry: blk_entry) {blk_entry});
    }

    typedef!    ((vm) int1         = mu_int(1));
    typedef!    ((vm) struct_t     = mu_struct(int64, int64, ref_int64));
    typedef!    ((vm) ref_struct_t = mu_ref(struct_t));

    funcsig!    ((vm) sig = () -> ());
    funcdecl!   ((vm) <sig> gc_mult_call_single_obj);
    funcdef!    ((vm) <sig> gc_mult_call_single_obj VERSION gc_mult_call_single_obj_v1);

    block!      ((vm, gc_mult_call_single_obj_v1) blk_entry);

    // a = NEW <struct_t>
    ssa!        ((vm, gc_mult_call_single_obj_v1) <ref_struct_t> a);
    inst!       ((vm, gc_mult_call_single_obj_v1) blk_entry_new1:
        a = NEW <struct_t>
    );

    ssa! ((vm, gc_mult_call_single_obj_v1) <int64> i);
    inst!       ((vm, gc_mult_call_single_obj_v1) blk_entry_convop:
        i = CONVOP (ConvOp::PTRCAST) <ref_struct_t int64> a
    );

    ssa!        ((vm, gc_mult_call_single_obj_v1) <ref_struct_t> b);
    inst!       ((vm, gc_mult_call_single_obj_v1) blk_entry_new2:
        b = NEW <struct_t>
    );

    inst!       ((vm, gc_mult_call_single_obj_v1) blk_entry_print1:
        PRINTHEX i
    );

    typedef!    ((vm) funcref_to_sig = mu_funcref(foo_sig));
    constdef!   ((vm) <funcref_to_sig> funcref_foo = Constant::FuncRef(foo));

    consta!     ((vm, gc_mult_call_single_obj_v1) funcref_foo_local = funcref_foo);
    inst!       ((vm, gc_mult_call_single_obj_v1) blk_entry_call:
            EXPRCALL (CallConvention::Mu, is_abort: false) funcref_foo_local ()
    );

    ssa! ((vm, gc_mult_call_single_obj_v1) <int64> i2);
    inst!       ((vm, gc_mult_call_single_obj_v1) blk_entry_convop2:
        i2 = CONVOP (ConvOp::PTRCAST) <ref_struct_t int64> b
    );

    inst!       ((vm, gc_mult_call_single_obj_v1) blk_entry_print2:
        PRINTHEX i2
    );

    inst!       ((vm, gc_mult_call_single_obj_v1) blk_entry_threadexit:
        THREADEXIT
    );

    define_block!   ((vm, gc_mult_call_single_obj_v1) blk_entry() {
        blk_entry_new1, blk_entry_convop,
        blk_entry_new2,
        blk_entry_print1, blk_entry_call, blk_entry_convop2, blk_entry_print2,
        blk_entry_threadexit
    });

    define_func_ver!((vm) gc_mult_call_single_obj_v1 (entry: blk_entry) {blk_entry});

    emit_test! ((vm)
        gc_mult_call_single_obj, gc_mult_call_single_obj_test1, gc_mult_call_single_obj_test1_v1,
        sig,
    );

    vm
}

#[test]
fn test_gc_mult_call_single_obj_with_gc() {
    build_and_run_llvm_test!(
        gc_mult_call_single_obj_with_gc AND foo,
        gc_mult_call_single_obj_with_gc_test1
    );
}

fn gc_mult_call_single_obj_with_gc() -> VM {
    let opts = String::from("init_mu --generate-llvm");
    let vm = VM::new_with_opts(opts.as_str());

    typedef!    ((vm) int64        = mu_int(64));
    typedef!    ((vm) ref_int64    = mu_ref(int64));
    typedef!    ((vm) iref_int64    = mu_iref(int64));
    typedef!    ((vm) iref_ref_int64    = mu_iref(ref_int64));
    funcsig!    ((vm) foo_sig = () -> ());
    funcdecl!   ((vm) <foo_sig> foo);

    {
        // add
        funcdef!    ((vm) <foo_sig> foo VERSION foo_v1);

        block!      ((vm, foo_v1) blk_entry);
        ssa!        ((vm, foo_v1) <ref_int64> x);
        inst!       ((vm, foo_v1) blk_entry_new:
            x = NEW <int64>
        );

        ssa! ((vm, foo_v1) <int64> i);
        inst!       ((vm, foo_v1) blk_entry_convop:
            i = CONVOP (ConvOp::PTRCAST) <ref_int64 int64> x
        );

        inst!       ((vm, foo_v1) blk_entry_print1:
            PRINTHEX i
        );

        ssa!        ((vm, foo_v1) <int64> y);
        inst!       ((vm, foo_v1) blk_entry_load_x:
            y = LOAD x (is_ptr: true, order: MemoryOrder::Relaxed)
        );

        inst!       ((vm, foo_v1) blk_entry_ret:
            RET
        );

        define_block!   ((vm, foo_v1) blk_entry() {
            blk_entry_new, blk_entry_convop, blk_entry_print1,
            blk_entry_load_x, blk_entry_ret
        });

        define_func_ver!((vm) foo_v1 (entry: blk_entry) {blk_entry});
    }

    typedef!    ((vm) int1         = mu_int(1));
    typedef!    ((vm) struct_t     = mu_struct(int64, int64, ref_int64));
    typedef!    ((vm) ref_struct_t = mu_ref(struct_t));
    typedef!    ((vm) iref_struct_t  = mu_iref(struct_t));
    constdef!   ((vm) <int64> i64_2 = Constant::Int(2));
    constdef!   ((vm) <int64> i64_7 = Constant::Int(9));

    funcsig!    ((vm) sig = () -> ());
    funcdecl!   ((vm) <sig> gc_mult_call_single_obj_with_gc);
    funcdef!    ((vm) <sig> gc_mult_call_single_obj_with_gc VERSION gc_mult_call_single_obj_with_gc_v1);

    block!      ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry);

    // a = NEW <struct_t>
    ssa!        ((vm, gc_mult_call_single_obj_with_gc_v1) <ref_struct_t> a);
    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_new1:
        a = NEW <struct_t>
    );

    ssa! ((vm, gc_mult_call_single_obj_with_gc_v1) <int64> i);
    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_convop:
        i = CONVOP (ConvOp::PTRCAST) <ref_struct_t int64> a
    );

    ssa!        ((vm, gc_mult_call_single_obj_with_gc_v1) <ref_struct_t> b);
    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_new2:
        b = NEW <struct_t>
    );

    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_print1:
        PRINTHEX i
    );

    ssa!        ((vm, gc_mult_call_single_obj_with_gc_v1) <iref_struct_t> irefa);
    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_getiref:
        irefa = GETIREF a
    );

    ssa!        ((vm, gc_mult_call_single_obj_with_gc_v1) <iref_int64> iref_field1);
    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_getfieldiref:
        iref_field1 = GETFIELDIREF irefa (is_ptr: false, index: 1)
    );

    // a.0 = 2
    consta!     ((vm, gc_mult_call_single_obj_with_gc_v1) i64_2_local = i64_2);
    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_write:
        STORE iref_field1 i64_2_local (is_ptr: false, order: MemoryOrder::Relaxed)
    );

    // var = a.0
    ssa!        ((vm, gc_mult_call_single_obj_with_gc_v1) <int64> var);
    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_load:
        var = LOAD iref_field1 (is_ptr: false, order: MemoryOrder::Relaxed)
    );

    // printex var
    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_print_a0:
        PRINTHEX var
    );

    // a.3 = new i64
    ssa!        ((vm, gc_mult_call_single_obj_with_gc_v1) <ref_int64> a_3);
    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_new_struct_a3:
        a_3 = NEW <int64>
    );

    // store 7 a_3
    consta!     ((vm, gc_mult_call_single_obj_with_gc_v1) i64_7_local = i64_7);
    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_write_a3_7:
        STORE a_3 i64_7_local (is_ptr: false, order: MemoryOrder::Relaxed)
    );

    // store a_3 a[3]
    ssa!        ((vm, gc_mult_call_single_obj_with_gc_v1) <iref_ref_int64> iref_field3);
    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_getfieldiref_a3:
        iref_field3 = GETFIELDIREF irefa (is_ptr: true, index: 2)
    );

    consta!     ((vm, gc_mult_call_single_obj_with_gc_v1) i64_7_local = i64_7);
    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_write_a3:
        STORE iref_field3 a_3 (is_ptr: true, order: MemoryOrder::Relaxed)
    );

    // va2 = load a[3]
    ssa!        ((vm, gc_mult_call_single_obj_with_gc_v1) <ref_int64> var2);
    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_load_a3_ptr:
        var2 = LOAD iref_field3 (is_ptr: false, order: MemoryOrder::Relaxed)
    );

    // va2 = load a[3]
    ssa!        ((vm, gc_mult_call_single_obj_with_gc_v1) <int64> var3);
    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_load_a3:
        var3 = LOAD var2 (is_ptr: false, order: MemoryOrder::Relaxed)
    );

    // printhex var3
    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_print_a3:
        PRINTHEX var3
    );

    // call trigger gc
    funcsig!    ((vm) trigger_gc_sig = () -> ());
    typedef!((vm) ufp_trigger_gc = mu_ufuncptr(trigger_gc_sig));
    constdef!((vm) <ufp_trigger_gc> const_trigger_gc = Constant::ExternSym(C ("trigger_gc_void")));
    consta!((vm,  gc_mult_call_single_obj_with_gc_v1) const_trigger_gc_local = const_trigger_gc);

    inst!((vm,  gc_mult_call_single_obj_with_gc_v1) blk_entry_ccall:
        EXPRCCALL (CallConvention::Foreign(ForeignFFI::C), is_abort: false) const_trigger_gc_local ()
    );

    typedef!    ((vm) funcref_to_sig = mu_funcref(foo_sig));
    constdef!   ((vm) <funcref_to_sig> funcref_foo = Constant::FuncRef(foo));

    consta!     ((vm, gc_mult_call_single_obj_with_gc_v1) funcref_foo_local = funcref_foo);
    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_call:
            EXPRCALL (CallConvention::Mu, is_abort: false) funcref_foo_local ()
    );

    ssa! ((vm, gc_mult_call_single_obj_with_gc_v1) <int64> i2);
    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_convop2:
        i2 = CONVOP (ConvOp::PTRCAST) <ref_struct_t int64> b
    );

    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_print2:
        PRINTHEX i2
    );

    // va2 = load a[3]
    ssa!        ((vm, gc_mult_call_single_obj_with_gc_v1) <ref_int64> var4);
    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_load_a3_ptr2:
        var4 = LOAD iref_field3 (is_ptr: false, order: MemoryOrder::Relaxed)
    );

    ssa! ((vm, gc_mult_call_single_obj_with_gc_v1) <int64> i3);
    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_convop3:
        i3 = CONVOP (ConvOp::PTRCAST) <ref_int64 int64> var4
    );

    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_print3:
        PRINTHEX i3
    );

    inst!       ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry_threadexit:
        THREADEXIT
    );

    define_block!   ((vm, gc_mult_call_single_obj_with_gc_v1) blk_entry() {
        blk_entry_new1,
        blk_entry_new2,
        blk_entry_getiref, blk_entry_getfieldiref, blk_entry_write,
        blk_entry_new_struct_a3,
        blk_entry_write_a3_7,
        blk_entry_getfieldiref_a3,
        blk_entry_write_a3,
        blk_entry_ccall,
        blk_entry_call,
        blk_entry_load,
        blk_entry_load_a3_ptr,
        blk_entry_load_a3,
        blk_entry_print_a0,
        blk_entry_print_a3,
        blk_entry_convop, blk_entry_print1,
        blk_entry_convop2, blk_entry_print2,
        blk_entry_load_a3_ptr2,
        blk_entry_convop3, blk_entry_print3,
        blk_entry_threadexit
    });

    define_func_ver!((vm) gc_mult_call_single_obj_with_gc_v1 (entry: blk_entry) {blk_entry});

    emit_test! ((vm)
        gc_mult_call_single_obj_with_gc, gc_mult_call_single_obj_with_gc_test1, gc_mult_call_single_obj_test1_v1,
        sig,
    );

    vm
}

#[test]
fn test_gc_global_var() {
    VM::start_logging_trace();

    let vm = Arc::new(VM::new_with_opts("init_mu --generate-llvm"));
    //    unsafe {
    //        MuThread::current_thread_as_mu_thread(Address::zero(),
    // vm.clone());    }
    global_var_gc(&vm);

    let mut whitelist = vec![];
    let func_id = vm.id_of("foo");
    whitelist.push(func_id);
    let func_id = vm.id_of("global_var_gc");
    whitelist.push(func_id);
    let primordial_func = vm.handle_from_func(func_id);

    // set global by api here
    {
        let global_id = vm.id_of("my_global");
        let global_handle = vm.handle_from_global(global_id);

        let uint64_10_handle = vm.handle_from_uint64(10, 64);

        debug!(
            "write {:?} to location {:?}",
            uint64_10_handle, global_handle
        );

        vm.handle_store(
            MemoryOrder::Relaxed,
            &global_handle,
            &uint64_10_handle
        );
    }

    // then emit context (global will be put into context.s
    vm.set_primordial_thread(func_id, true, vec![]);
    let image_name = String::from("global_var_gc");

    aot_mu::make_boot_image_for_testing(
        &vm,
        whitelist,
        Some(&*primordial_func),
        None,
        None,
        Vec::new(),
        Vec::new(),
        Vec::new(),
        Vec::new(),
        image_name.clone()
    );

    // link
    let image_location = format!("emit/{}", image_name);
    let executable = PathBuf::from(image_location);
    let output = linkutils::exec_path_nocheck(executable);

    assert!(output.status.code().is_some());

    let ret_code = output.status.code().unwrap();
    info!("return code: {} (i.e. the value set before)", ret_code);
    assert!(ret_code == 10);
}

fn global_var_gc(vm: &VM) {
    typedef!    ((vm) int64      = mu_int(64));
    typedef!    ((vm) ref_int64 = mu_ref(int64));

    globaldef!  ((vm) <int64> my_global);

    funcsig!    ((vm) foo_sig = () -> ());
    funcdecl!   ((vm) <foo_sig> foo);

    {
        // add
        funcdef!    ((vm) <foo_sig> foo VERSION foo_v1);

        block!      ((vm, foo_v1) blk_entry);

        global!     ((vm, foo_v1) blk_entry_my_global = my_global);

        ssa! ((vm, foo_v1) <int64> i);
        inst!       ((vm, foo_v1) blk_entry_convop:
            i = CONVOP (ConvOp::PTRCAST) <ref_int64 int64> blk_entry_my_global
        );

        inst!       ((vm, foo_v1) blk_entry_print1:
            PRINTHEX i
        );

        inst!       ((vm, foo_v1) blk_entry_ret:
            RET
        );

        define_block!   ((vm, foo_v1) blk_entry() {
             blk_entry_convop, blk_entry_print1, blk_entry_ret
        });

        define_func_ver!((vm) foo_v1 (entry: blk_entry) {blk_entry});
    }

    funcsig!    ((vm) sig = () -> (int64));
    funcdecl!   ((vm) <sig> global_var_gc);
    funcdef!    ((vm) <sig> global_var_gc VERSION global_var_gc_v1);

    // blk entry
    block!      ((vm, global_var_gc_v1) blk_entry);
    global!     ((vm, global_var_gc_v1) blk_entry_my_global = my_global);

    // call trigger gc
    funcsig!    ((vm) trigger_gc_sig = () -> ());
    typedef!((vm) ufp_trigger_gc = mu_ufuncptr(trigger_gc_sig));
    constdef!((vm) <ufp_trigger_gc> const_trigger_gc = Constant::ExternSym(C ("trigger_gc_void")));
    consta!((vm,  global_var_gc_v1) const_trigger_gc_local = const_trigger_gc);

    inst!((vm,  global_var_gc_v1) blk_entry_ccall:
        EXPRCCALL (CallConvention::Foreign(ForeignFFI::C), is_abort: false) const_trigger_gc_local ()
    );

    typedef!    ((vm) funcref_to_sig = mu_funcref(foo_sig));
    constdef!   ((vm) <funcref_to_sig> funcref_foo = Constant::FuncRef(foo));

    consta!     ((vm, global_var_gc_v1) funcref_foo_local = funcref_foo);
    inst!       ((vm, global_var_gc_v1) blk_entry_call:
            EXPRCALL (CallConvention::Mu, is_abort: false) funcref_foo_local ()
    );

    ssa!        ((vm, global_var_gc_v1) <int64> val);
    inst!       ((vm, global_var_gc_v1) blk_entry_load:
        val = LOAD blk_entry_my_global (is_ptr: false, order: MemoryOrder::SeqCst)
    );

    ssa!        ((vm, global_var_gc_v1) <ref_int64> b);
    inst!       ((vm, global_var_gc_v1) blk_entry_new_b:
        b = NEW <int64>
    );

    let blk_entry_exit =
        gen_ccall_exit(val.clone(), &mut global_var_gc_v1, &vm);

    // won't execute this inst
    inst!       ((vm, global_var_gc_v1) blk_entry_ret:
        RET (val)
    );

    define_block!   ((vm, global_var_gc_v1) blk_entry() {
        blk_entry_ccall,
        blk_entry_call,
        blk_entry_load,
        blk_entry_new_b,
        blk_entry_exit, blk_entry_ret
    });

    define_func_ver!((vm) global_var_gc_v1 (entry: blk_entry) {
        blk_entry
    });
}

#[test]
fn test_gc_global_pointer() {
    VM::start_logging_trace();

    let vm = Arc::new(VM::new_with_opts("init_mu --generate-llvm"));
    //    unsafe {
    //        MuThread::current_thread_as_mu_thread(Address::zero(),
    // vm.clone());    }
    global_pointer_gc(&vm);

    let mut whitelist = vec![];
    let func_id = vm.id_of("foo");
    whitelist.push(func_id);
    let func_id = vm.id_of("global_pointer_gc");
    whitelist.push(func_id);
    let primordial_func = vm.handle_from_func(func_id);

    // set global by api here
    //    {
    let global_id = vm.id_of("my_global");
    let global_handle = vm.handle_from_global(global_id);

    //        let uint64_10_handle = vm.handle_from_uint64(10, 64);
    //
    //
    //
    //        debug!(
    //            "write {:?} to location {:?}",
    //            uint64_10_handle, global_handle
    //        );
    //
    //        vm.handle_store(
    //            MemoryOrder::Relaxed,
    //            &global_handle,
    //            &uint64_10_handle
    //        );
    //    }

    // then emit context (global will be put into context.s
    vm.set_primordial_thread(func_id, true, vec![]);
    let image_name = String::from("global_access");

    aot_mu::make_boot_image_for_testing(
        &vm,
        whitelist,
        Some(&*primordial_func),
        None,
        None,
        Vec::new(),
        Vec::new(),
        Vec::new(),
        Vec::new(),
        image_name.clone()
    );

    // link
    let image_location = format!("emit/{}", image_name);
    let executable = PathBuf::from(image_location);
    let output = linkutils::exec_path_nocheck(executable);

    assert!(output.status.code().is_some());

    let ret_code = output.status.code().unwrap();
    info!("return code: {} (i.e. the value set before)", ret_code);
    info!("{:?}", global_handle.v.as_address());
    assert!(ret_code == 0);
}

fn global_pointer_gc(vm: &VM) {
    typedef!    ((vm) int64      = mu_int(64));
    typedef!    ((vm) ref_int64 = mu_ref(int64));
    typedef!    ((vm) ref_ref_int64 = mu_ref(ref_int64));

    globaldef!  ((vm) <ref_int64> my_global);

    funcsig!    ((vm) foo_sig = () -> ());
    funcdecl!   ((vm) <foo_sig> foo);

    {
        // add
        funcdef!    ((vm) <foo_sig> foo VERSION foo_v1);

        block!      ((vm, foo_v1) blk_entry);

        global!     ((vm, foo_v1) blk_entry_my_global = my_global);

        //        ssa!        ((vm, foo_v1) <ref_int64> x);
        //        inst!       ((vm, foo_v1) blk_entry_load:
        //            x = LOAD blk_entry_my_global (is_ptr: false, order:
        // MemoryOrder::SeqCst)        );

        ssa! ((vm, foo_v1) <int64> i);
        inst!       ((vm, foo_v1) blk_entry_convop:
            i = CONVOP (ConvOp::PTRCAST) <ref_ref_int64 int64> blk_entry_my_global
        );

        inst!       ((vm, foo_v1) blk_entry_print1:
            PRINTHEX i
        );

        inst!       ((vm, foo_v1) blk_entry_ret:
            RET
        );

        define_block!   ((vm, foo_v1) blk_entry() {
        //            blk_entry_load,
                     blk_entry_convop, blk_entry_print1, blk_entry_ret
                });

        define_func_ver!((vm) foo_v1 (entry: blk_entry) {blk_entry});
    }

    funcsig!    ((vm) sig = () -> (int64));
    funcdecl!   ((vm) <sig> global_pointer_gc);
    funcdef!    ((vm) <sig> global_pointer_gc VERSION global_pointer_gc_v1);

    // blk entry
    block!      ((vm, global_pointer_gc_v1) blk_entry);
    global!     ((vm, global_pointer_gc_v1) blk_entry_my_global = my_global);

    ssa!        ((vm, global_pointer_gc_v1) <ref_int64> a);
    inst!       ((vm, global_pointer_gc_v1) blk_entry_new:
        a = NEW <int64>
    );

    inst!       ((vm, global_pointer_gc_v1) blk_entry_write:
        STORE blk_entry_my_global a (is_ptr: true, order: MemoryOrder::Relaxed)
    );

    // call trigger gc
    funcsig!    ((vm) trigger_gc_sig = () -> ());
    typedef!((vm) ufp_trigger_gc = mu_ufuncptr(trigger_gc_sig));
    constdef!((vm) <ufp_trigger_gc> const_trigger_gc = Constant::ExternSym(C ("trigger_gc_void")));
    consta!((vm,  global_pointer_gc_v1) const_trigger_gc_local = const_trigger_gc);

    inst!((vm,  global_pointer_gc_v1) blk_entry_ccall:
        EXPRCCALL (CallConvention::Foreign(ForeignFFI::C), is_abort: false) const_trigger_gc_local ()
    );

    typedef!    ((vm) funcref_to_sig = mu_funcref(foo_sig));
    constdef!   ((vm) <funcref_to_sig> funcref_foo = Constant::FuncRef(foo));

    consta!     ((vm, global_pointer_gc_v1) funcref_foo_local = funcref_foo);
    inst!       ((vm, global_pointer_gc_v1) blk_entry_call:
            EXPRCALL (CallConvention::Mu, is_abort: false) funcref_foo_local ()
    );

    ssa!        ((vm, global_pointer_gc_v1) <ref_int64> val_ptr);
    inst!       ((vm, global_pointer_gc_v1) blk_entry_load:
        val_ptr = LOAD blk_entry_my_global (is_ptr: false, order: MemoryOrder::SeqCst)
    );

    ssa!        ((vm, global_pointer_gc_v1) <int64> val);
    inst!       ((vm, global_pointer_gc_v1) blk_entry_load_2:
        val = LOAD val_ptr (is_ptr: false, order: MemoryOrder::SeqCst)
    );

    ssa!        ((vm, global_pointer_gc_v1) <ref_int64> b);
    inst!       ((vm, global_pointer_gc_v1) blk_entry_new_b:
        b = NEW <int64>
    );

    let blk_entry_exit =
        gen_ccall_exit(val.clone(), &mut global_pointer_gc_v1, &vm);

    // won't execute this inst
    inst!       ((vm, global_pointer_gc_v1) blk_entry_ret:
        RET (val)
    );

    define_block!   ((vm, global_pointer_gc_v1) blk_entry() {
        blk_entry_new,
        blk_entry_write,
        blk_entry_ccall,
        blk_entry_call,
        blk_entry_load,
        blk_entry_load_2,
        blk_entry_new_b,
        blk_entry_exit, blk_entry_ret
    });

    define_func_ver!((vm) global_pointer_gc_v1 (entry: blk_entry) {
        blk_entry
    });
}

#[test]
fn test_gc_global_internal_pointer() {
    VM::start_logging_trace();

    let vm = Arc::new(VM::new_with_opts("init_mu --generate-llvm"));
    //    unsafe {
    //        MuThread::current_thread_as_mu_thread(Address::zero(),
    // vm.clone());    }
    global_internal_pointer_gc(&vm);

    let mut whitelist = vec![];
    let func_id = vm.id_of("foo");
    whitelist.push(func_id);
    let func_id = vm.id_of("global_internal_pointer_gc");
    whitelist.push(func_id);
    let primordial_func = vm.handle_from_func(func_id);

    // set global by api here
    {
        let global_id = vm.id_of("my_global");
        let global_handle = vm.handle_from_global(global_id);

        let element_handle = vm.handle_get_field_iref(&global_handle, 0);
        let uint64_10_handle = vm.handle_from_uint64(10, 64);

        debug!(
            "write {:?} to location {:?}",
            uint64_10_handle, global_handle
        );

        vm.handle_store(
            MemoryOrder::Relaxed,
            &element_handle,
            &uint64_10_handle
        );
    }

    // then emit context (global will be put into context.s
    vm.set_primordial_thread(func_id, true, vec![]);
    let image_name = String::from("global_internal_pointer");

    aot_mu::make_boot_image_for_testing(
        &vm,
        whitelist,
        Some(&*primordial_func),
        None,
        None,
        Vec::new(),
        Vec::new(),
        Vec::new(),
        Vec::new(),
        image_name.clone()
    );

    // link
    let image_location = format!("emit/{}", image_name);
    let executable = PathBuf::from(image_location);
    let output = linkutils::exec_path_nocheck(executable);

    assert!(output.status.code().is_some());

    let ret_code = output.status.code().unwrap();
    info!("return code: {} (i.e. the value set before)", ret_code);
    assert!(ret_code == 10);
}

fn global_internal_pointer_gc(vm: &VM) {
    typedef!    ((vm) int64         = mu_int(64));
    typedef!    ((vm) ref_int64     = mu_ref(int64));
    typedef!    ((vm) iref_int64    = mu_iref(int64));
    typedef!    ((vm) struct_t      = mu_struct(int64, int64, ref_int64));
    typedef!    ((vm) iref_struct_t = mu_iref(struct_t));
    typedef!    ((vm) ref_struct_t  = mu_ref(struct_t));

    globaldef!  ((vm) <struct_t> my_global);

    funcsig!    ((vm) foo_sig = () -> ());
    funcdecl!   ((vm) <foo_sig> foo);

    {
        // add
        funcdef!    ((vm) <foo_sig> foo VERSION foo_v1);

        block!      ((vm, foo_v1) blk_entry);

        global!     ((vm, foo_v1) blk_entry_my_global = my_global);

        ssa! ((vm, foo_v1) <int64> i);
        inst!       ((vm, foo_v1) blk_entry_convop:
            i = CONVOP (ConvOp::PTRCAST) <ref_struct_t int64> blk_entry_my_global
        );

        inst!       ((vm, foo_v1) blk_entry_print1:
            PRINTHEX i
        );

        inst!       ((vm, foo_v1) blk_entry_ret:
            RET
        );

        define_block!   ((vm, foo_v1) blk_entry() {
        //            blk_entry_load,
                     blk_entry_convop, blk_entry_print1, blk_entry_ret
                });

        define_func_ver!((vm) foo_v1 (entry: blk_entry) {blk_entry});
    }

    funcsig!    ((vm) sig = () -> (int64));
    funcdecl!   ((vm) <sig> global_internal_pointer_gc);
    funcdef!    ((vm) <sig> global_internal_pointer_gc VERSION global_internal_pointer_gc_v1);

    // blk entry
    block!      ((vm, global_internal_pointer_gc_v1) blk_entry);
    global!     ((vm, global_internal_pointer_gc_v1) blk_entry_my_global = my_global);

    //    ssa!        ((vm, global_internal_pointer_gc_v1) <ref_int64> a);
    //    inst!       ((vm, global_internal_pointer_gc_v1) blk_entry_new:
    //        a = NEW <int64>
    //    );
    //
    //    inst!       ((vm, global_internal_pointer_gc_v1) blk_entry_write:
    //        STORE blk_entry_my_global a (is_ptr: true, order:
    // MemoryOrder::Relaxed)    );

    // call trigger gc
    funcsig!    ((vm) trigger_gc_sig = () -> ());
    typedef!((vm) ufp_trigger_gc = mu_ufuncptr(trigger_gc_sig));
    constdef!((vm) <ufp_trigger_gc> const_trigger_gc = Constant::ExternSym(C ("trigger_gc_void")));
    consta!((vm,  global_internal_pointer_gc_v1) const_trigger_gc_local = const_trigger_gc);

    inst!((vm,  global_internal_pointer_gc_v1) blk_entry_ccall:
        EXPRCCALL (CallConvention::Foreign(ForeignFFI::C), is_abort: false) const_trigger_gc_local ()
    );

    typedef!    ((vm) funcref_to_sig = mu_funcref(foo_sig));
    constdef!   ((vm) <funcref_to_sig> funcref_foo = Constant::FuncRef(foo));

    consta!     ((vm, global_internal_pointer_gc_v1) funcref_foo_local = funcref_foo);
    inst!       ((vm, global_internal_pointer_gc_v1) blk_entry_call:
            EXPRCALL (CallConvention::Mu, is_abort: false) funcref_foo_local ()
    );

    ssa!        ((vm, global_internal_pointer_gc_v1) <iref_struct_t> irefa);
    inst!       ((vm, global_internal_pointer_gc_v1) blk_entry_getiref:
        irefa = GETIREF blk_entry_my_global
    );

    ssa!        ((vm, global_internal_pointer_gc_v1) <iref_int64> iref_field0);
    inst!       ((vm, global_internal_pointer_gc_v1) blk_entry_getfieldiref:
        iref_field0 = GETFIELDIREF irefa (is_ptr: false, index: 0)
    );

    ssa!        ((vm, global_internal_pointer_gc_v1) <iref_int64> iref_field2);
    inst!       ((vm, global_internal_pointer_gc_v1) blk_entry_getfieldiref2:
        iref_field2 = GETFIELDIREF irefa (is_ptr: false, index: 2)
    );

    ssa!        ((vm, global_internal_pointer_gc_v1) <ref_int64> a);
    inst!       ((vm, global_internal_pointer_gc_v1) blk_entry_new:
        a = NEW <int64>
    );

    inst!       ((vm, global_internal_pointer_gc_v1) blk_entry_write:
            STORE iref_field2 a (is_ptr: true, order:
     MemoryOrder::Relaxed)    );

    // val = my_global.0
    ssa!        ((vm, global_internal_pointer_gc_v1) <int64> val);
    inst!       ((vm, global_internal_pointer_gc_v1) blk_entry_load:
        val = LOAD iref_field0 (is_ptr: false, order: MemoryOrder::Relaxed)
    );

    ssa!        ((vm, global_internal_pointer_gc_v1) <ref_int64> b);
    inst!       ((vm, global_internal_pointer_gc_v1) blk_entry_new_b:
        b = NEW <int64>
    );

    let blk_entry_exit =
        gen_ccall_exit(val.clone(), &mut global_internal_pointer_gc_v1, &vm);

    // won't execute this inst
    inst!       ((vm, global_internal_pointer_gc_v1) blk_entry_ret:
        RET (val)
    );

    define_block!   ((vm, global_internal_pointer_gc_v1) blk_entry() {
        blk_entry_getiref,
        blk_entry_getfieldiref,
        blk_entry_getfieldiref2,
        blk_entry_new,
        blk_entry_write,
        blk_entry_ccall,
        blk_entry_call,
        blk_entry_load,
        blk_entry_new_b,
        blk_entry_exit, blk_entry_ret
    });

    define_func_ver!((vm) global_internal_pointer_gc_v1 (entry: blk_entry) {
        blk_entry
    });
}

#[test]
fn test_gc_global_point_persisted_obj() {
    VM::start_logging_trace();

    let vm = Arc::new(VM::new_with_opts("init_mu --generate-llvm"));
    //    unsafe {
    //        MuThread::current_thread_as_mu_thread(Address::zero(),
    // vm.clone());    }
    global_point_persisted_obj_gc(&vm);

    let mut whitelist = vec![];
    let func_id = vm.id_of("foo");
    whitelist.push(func_id);
    let func_id = vm.id_of("global_point_persisted_obj_gc");
    whitelist.push(func_id);
    let primordial_func = vm.handle_from_func(func_id);

    // set global by api here
    {
        let global_id = vm.id_of("my_global");
        let global_handle = vm.handle_from_global(global_id);

        // my_global.0 = 10
        let element_0_handle = vm.handle_get_field_iref(&global_handle, 0);
        let uint64_10_handle = vm.handle_from_uint64(10, 64);

        debug!(
            "write {:?} to location {:?}",
            uint64_10_handle, global_handle
        );

        vm.handle_store(
            MemoryOrder::Relaxed,
            &element_0_handle,
            &uint64_10_handle
        );

        // my_global.2 = new struct
        let struct_ty = vm.id_of("struct_t2");
        let new_obj = vm.new_fixed(struct_ty);
        let new_obj_iref = vm.handle_get_iref(&new_obj);
        let element_2_handle = vm.handle_get_field_iref(&global_handle, 2);

        let element_0_handle = vm.handle_get_field_iref(&new_obj_iref, 0);
        let uint64_7_handle = vm.handle_from_uint64(7, 64);

        vm.handle_store(MemoryOrder::Relaxed, &element_2_handle, &new_obj);

        vm.handle_store(
            MemoryOrder::Relaxed,
            &element_0_handle,
            &uint64_7_handle
        );
    }

    // then emit context (global will be put into context.s
    vm.set_primordial_thread(func_id, true, vec![]);
    let image_name = String::from("global_point_persisted_obj_gc");

    aot_mu::make_boot_image_for_testing(
        &vm,
        whitelist,
        Some(&*primordial_func),
        None,
        None,
        Vec::new(),
        Vec::new(),
        Vec::new(),
        Vec::new(),
        image_name.clone()
    );

    // link
    let image_location = format!("emit/{}", image_name);
    let executable = PathBuf::from(image_location);
    let output = linkutils::exec_path_nocheck(executable);

    assert!(output.status.code().is_some());

    let ret_code = output.status.code().unwrap();
    info!("return code: {} (i.e. the value set before)", ret_code);
    assert!(ret_code == 2);
}

fn global_point_persisted_obj_gc(vm: &VM) {
    typedef!    ((vm) int64         = mu_int(64));
    typedef!    ((vm) ref_int64     = mu_ref(int64));
    typedef!    ((vm) iref_int64    = mu_iref(int64));
    typedef!    ((vm) iref_ref_int64 = mu_iref(ref_int64));

    typedef!    ((vm) struct_t2     = mu_struct(int64, int64, ref_int64));
    typedef!    ((vm) ref_struct_t2 = mu_ref(struct_t2));
    typedef!    ((vm) iref_ref_struct_t2 = mu_iref(ref_struct_t2));

    typedef!    ((vm) struct_t      = mu_struct(int64, int64, ref_struct_t2));
    typedef!    ((vm) iref_struct_t = mu_iref(struct_t));
    typedef!    ((vm) ref_struct_t  = mu_ref(struct_t));

    constdef!   ((vm) <int64> i64_2 = Constant::Int(2));

    globaldef!  ((vm) <struct_t> my_global);

    funcsig!    ((vm) foo_sig = () -> ());
    funcdecl!   ((vm) <foo_sig> foo);

    {
        // add
        funcdef!    ((vm) <foo_sig> foo VERSION foo_v1);

        block!      ((vm, foo_v1) blk_entry);

        global!     ((vm, foo_v1) blk_entry_my_global = my_global);

        ssa! ((vm, foo_v1) <int64> i);
        inst!       ((vm, foo_v1) blk_entry_convop:
            i = CONVOP (ConvOp::PTRCAST) <ref_struct_t int64> blk_entry_my_global
        );

        inst!       ((vm, foo_v1) blk_entry_print1:
            PRINTHEX i
        );

        inst!       ((vm, foo_v1) blk_entry_ret:
            RET
        );

        define_block!   ((vm, foo_v1) blk_entry() {
        //            blk_entry_load,
                     blk_entry_convop, blk_entry_print1, blk_entry_ret
                });

        define_func_ver!((vm) foo_v1 (entry: blk_entry) {blk_entry});
    }

    funcsig!    ((vm) sig = () -> (int64));
    funcdecl!   ((vm) <sig> global_point_persisted_obj_gc);
    funcdef!    ((vm) <sig> global_point_persisted_obj_gc VERSION global_point_persisted_obj_gc_v1);

    // blk entry
    block!      ((vm, global_point_persisted_obj_gc_v1) blk_entry);
    global!     ((vm, global_point_persisted_obj_gc_v1) blk_entry_my_global = my_global);

    ssa!        ((vm, global_point_persisted_obj_gc_v1) <ref_int64> a);
    inst!       ((vm, global_point_persisted_obj_gc_v1) blk_entry_new:
        a = NEW <int64>
    );

    ssa!        ((vm, global_point_persisted_obj_gc_v1) <iref_struct_t> irefa);
    inst!       ((vm, global_point_persisted_obj_gc_v1) blk_entry_getiref:
        irefa = GETIREF blk_entry_my_global
    );

    ssa!        ((vm, global_point_persisted_obj_gc_v1) <iref_int64> iref_field0);
    inst!       ((vm, global_point_persisted_obj_gc_v1) blk_entry_getfieldiref:
        iref_field0 = GETFIELDIREF irefa (is_ptr: false, index: 0)
    );

    ssa!        ((vm, global_point_persisted_obj_gc_v1) <iref_ref_struct_t2> iref_field2);
    inst!       ((vm, global_point_persisted_obj_gc_v1) blk_entry_getfieldiref2:
        iref_field2 = GETFIELDIREF irefa (is_ptr: false, index: 2)
    );

    ssa!        ((vm, global_point_persisted_obj_gc_v1) <ref_struct_t2> ref_struct);
    inst!       ((vm, global_point_persisted_obj_gc_v1) blk_entry_load_t2:
        ref_struct = LOAD iref_field2 (is_ptr: false, order: MemoryOrder::Relaxed)
    );

    ssa!        ((vm, global_point_persisted_obj_gc_v1) <iref_struct_t> irefa_t2);
    inst!       ((vm, global_point_persisted_obj_gc_v1) blk_entry_getiref_t2:
        irefa_t2 = GETIREF ref_struct
    );

    ssa!        ((vm, global_point_persisted_obj_gc_v1) <iref_ref_int64> iref_field2_t2);
    inst!       ((vm, global_point_persisted_obj_gc_v1) blk_entry_getfieldiref2_t2:
        iref_field2_t2 = GETFIELDIREF irefa_t2 (is_ptr: false, index: 2)
    );

    consta!     ((vm, global_point_persisted_obj_gc_v1) i64_2_local = i64_2);
    inst!       ((vm, global_point_persisted_obj_gc_v1) blk_entry_write_const:
            STORE a i64_2_local (is_ptr: false, order:
     MemoryOrder::Relaxed)    );

    inst!       ((vm, global_point_persisted_obj_gc_v1) blk_entry_write:
            STORE iref_field2_t2 a (is_ptr: true, order:
     MemoryOrder::Relaxed)    );

    // call trigger gc
    funcsig!    ((vm) trigger_gc_sig = () -> ());
    typedef!((vm) ufp_trigger_gc = mu_ufuncptr(trigger_gc_sig));
    constdef!((vm) <ufp_trigger_gc> const_trigger_gc = Constant::ExternSym(C ("trigger_gc_void")));
    consta!((vm,  global_point_persisted_obj_gc_v1) const_trigger_gc_local = const_trigger_gc);

    inst!((vm,  global_point_persisted_obj_gc_v1) blk_entry_ccall:
        EXPRCCALL (CallConvention::Foreign(ForeignFFI::C), is_abort: false) const_trigger_gc_local ()
    );

    typedef!    ((vm) funcref_to_sig = mu_funcref(foo_sig));
    constdef!   ((vm) <funcref_to_sig> funcref_foo = Constant::FuncRef(foo));

    consta!     ((vm, global_point_persisted_obj_gc_v1) funcref_foo_local = funcref_foo);
    inst!       ((vm, global_point_persisted_obj_gc_v1) blk_entry_call:
            EXPRCALL (CallConvention::Mu, is_abort: false) funcref_foo_local ()
    );

    //    ssa!        ((vm, global_point_persisted_obj_gc_v1) <ref_int64> a);
    //    inst!       ((vm, global_point_persisted_obj_gc_v1) blk_entry_new:
    //        a = NEW <int64>
    //    );
    //
    //    inst!       ((vm, global_point_persisted_obj_gc_v1) blk_entry_write:
    //            STORE iref_field2 a (is_ptr: true, order:
    //     MemoryOrder::Relaxed)    );

    // val = my_global.0
    ssa!        ((vm, global_point_persisted_obj_gc_v1) <ref_int64> val2);
    inst!       ((vm, global_point_persisted_obj_gc_v1) blk_entry_load:
        val2 = LOAD iref_field2_t2 (is_ptr: false, order: MemoryOrder::Relaxed)
    );

    ssa!        ((vm, global_point_persisted_obj_gc_v1) <int64> val);
    inst!       ((vm, global_point_persisted_obj_gc_v1) blk_entry_load_val2:
        val = LOAD val2 (is_ptr: false, order: MemoryOrder::Relaxed)
    );

    ssa!        ((vm, global_point_persisted_obj_gc_v1) <ref_int64> b);
    inst!       ((vm, global_point_persisted_obj_gc_v1) blk_entry_new_b:
        b = NEW <int64>
    );

    let blk_entry_exit =
        gen_ccall_exit(val.clone(), &mut global_point_persisted_obj_gc_v1, &vm);

    // won't execute this inst
    inst!       ((vm, global_point_persisted_obj_gc_v1) blk_entry_ret:
        RET (val)
    );

    define_block!   ((vm, global_point_persisted_obj_gc_v1) blk_entry() {
            blk_entry_getiref,
    //        blk_entry_getfieldiref,
            blk_entry_getfieldiref2,
            blk_entry_load_t2,
            blk_entry_getiref_t2,
            blk_entry_getfieldiref2_t2,
            blk_entry_new,
            blk_entry_write_const,
            blk_entry_write,
            blk_entry_ccall,
            blk_entry_call,
            blk_entry_load,
            blk_entry_load_val2,
            blk_entry_new_b,
            blk_entry_exit, blk_entry_ret
        });

    define_func_ver!((vm) global_point_persisted_obj_gc_v1 (entry: blk_entry) {
        blk_entry
    });
}

#[test]
fn test_gc_mult_call_mult_obj() {
    build_and_run_llvm_test!(
        gc_mult_call_mult_obj AND foo,
        gc_mult_call_mult_obj_test1
    );
}

fn gc_mult_call_mult_obj() -> VM {
    let opts = String::from("init_mu --generate-llvm");
    let vm = VM::new_with_opts(opts.as_str());

    typedef!    ((vm) int64        = mu_int(64));
    typedef!    ((vm) ref_int64    = mu_ref(int64));
    funcsig!    ((vm) foo_sig = () -> ());
    funcdecl!   ((vm) <foo_sig> foo);

    {
        // add
        funcdef!    ((vm) <foo_sig> foo VERSION foo_v1);

        block!      ((vm, foo_v1) blk_entry);
        ssa!        ((vm, foo_v1) <ref_int64> x);
        inst!       ((vm, foo_v1) blk_entry_new:
            x = NEW <int64>
        );

        ssa!        ((vm, foo_v1) <ref_int64> x2);
        inst!       ((vm, foo_v1) blk_entry_new_x2:
            x2 = NEW <int64>
        );

        ssa! ((vm, foo_v1) <int64> i);
        inst!       ((vm, foo_v1) blk_entry_convop:
            i = CONVOP (ConvOp::PTRCAST) <ref_int64 int64> x
        );

        inst!       ((vm, foo_v1) blk_entry_print1:
            PRINTHEX i
        );

        ssa!        ((vm, foo_v1) <int64> y);
        inst!       ((vm, foo_v1) blk_entry_load_x:
            y = LOAD x (is_ptr: true, order: MemoryOrder::Relaxed)
        );

        ssa!        ((vm, foo_v1) <int64> y2);
        inst!       ((vm, foo_v1) blk_entry_load_x2:
            y2 = LOAD x2 (is_ptr: true, order: MemoryOrder::Relaxed)
        );

        inst!       ((vm, foo_v1) blk_entry_ret:
            RET
        );

        define_block!   ((vm, foo_v1) blk_entry() {
            blk_entry_new, blk_entry_new_x2, blk_entry_convop, blk_entry_print1,
            blk_entry_load_x, blk_entry_load_x2, blk_entry_ret
        });

        define_func_ver!((vm) foo_v1 (entry: blk_entry) {blk_entry});
    }

    typedef!    ((vm) int1         = mu_int(1));
    typedef!    ((vm) struct_t     = mu_struct(int64, int64, ref_int64));
    typedef!    ((vm) ref_struct_t = mu_ref(struct_t));

    funcsig!    ((vm) sig = () -> ());
    funcdecl!   ((vm) <sig> gc_mult_call_mult_obj);
    funcdef!    ((vm) <sig> gc_mult_call_mult_obj VERSION gc_mult_call_mult_obj_v1);

    block!      ((vm, gc_mult_call_mult_obj_v1) blk_entry);

    // a = NEW <struct_t>
    ssa!        ((vm, gc_mult_call_mult_obj_v1) <ref_struct_t> a);
    inst!       ((vm, gc_mult_call_mult_obj_v1) blk_entry_new1:
        a = NEW <struct_t>
    );

    ssa! ((vm, gc_mult_call_mult_obj_v1) <int64> i);
    inst!       ((vm, gc_mult_call_mult_obj_v1) blk_entry_convop:
        i = CONVOP (ConvOp::PTRCAST) <ref_struct_t int64> a
    );

    ssa!        ((vm, gc_mult_call_mult_obj_v1) <ref_struct_t> b);
    inst!       ((vm, gc_mult_call_mult_obj_v1) blk_entry_new2:
        b = NEW <struct_t>
    );

    inst!       ((vm, gc_mult_call_mult_obj_v1) blk_entry_print1:
        PRINTHEX i
    );

    typedef!    ((vm) funcref_to_sig = mu_funcref(foo_sig));
    constdef!   ((vm) <funcref_to_sig> funcref_foo = Constant::FuncRef(foo));

    consta!     ((vm, gc_mult_call_mult_obj_v1) funcref_foo_local = funcref_foo);
    inst!       ((vm, gc_mult_call_mult_obj_v1) blk_entry_call:
            EXPRCALL (CallConvention::Mu, is_abort: false) funcref_foo_local ()
    );

    // c = LOAD a
    ssa!        ((vm, gc_mult_call_mult_obj_v1) <struct_t> c);
    inst!       ((vm, gc_mult_call_mult_obj_v1) blk_entry_load_a:
        c = LOAD a (is_ptr: true, order: MemoryOrder::Relaxed)
    );

    ssa! ((vm, gc_mult_call_mult_obj_v1) <int64> i2);
    inst!       ((vm, gc_mult_call_mult_obj_v1) blk_entry_convop2:
        i2 = CONVOP (ConvOp::PTRCAST) <ref_struct_t int64> b
    );

    inst!       ((vm, gc_mult_call_mult_obj_v1) blk_entry_print2:
        PRINTHEX i2
    );

    inst!       ((vm, gc_mult_call_mult_obj_v1) blk_entry_threadexit:
        THREADEXIT
    );

    define_block!   ((vm, gc_mult_call_mult_obj_v1) blk_entry() {
        blk_entry_new1, blk_entry_convop,
        blk_entry_new2,
        blk_entry_print1, blk_entry_call, blk_entry_load_a, blk_entry_convop2, blk_entry_print2,
        blk_entry_threadexit
    });

    define_func_ver!((vm) gc_mult_call_mult_obj_v1 (entry: blk_entry) {blk_entry});

    emit_test! ((vm)
        gc_mult_call_mult_obj, gc_mult_call_mult_obj_test1, gc_mult_call_mult_obj_test1_v1,
        sig,
    );

    vm
}

#[test]
fn test_exception_throw_catch_twice_with_gc() {
    build_and_run_llvm_test!(catch_twice AND throw_exception,
 catch_twice_test2, throw_catch_twice_with_gc);
}

fn throw_catch_twice_with_gc() -> VM {
    let opts = String::from("init_mu --generate-llvm");
    let vm = VM::new_with_opts(opts.as_str());

    crate::tests::test_exception::declare_commons(&vm);

    let throw_exc =
        crate::tests::test_exception::create_throw_exception_func(&vm);
    create_catch_twice_with_gc(&vm, throw_exc);

    vm
}

fn create_catch_twice_with_gc(vm: &VM, throw_exception: MuEntityHeader) {
    let throw_exception_sig = vm.get_func_sig(vm.id_of("throw_exception_sig"));
    let ref_int64 = vm.get_type(vm.id_of("ref_int64"));
    let iref_int64 = vm.get_type(vm.id_of("iref_int64"));
    let int64 = vm.get_type(vm.id_of("int64"));

    typedef!    ((vm) type_funcref_throw_exception = mu_funcref(throw_exception_sig));
    constdef!   ((vm) <type_funcref_throw_exception> const_funcref_throw_exception
        = Constant::FuncRef(throw_exception));

    funcsig!    ((vm) catch_exception_sig = () -> (int64));
    funcdecl!   ((vm) <catch_exception_sig> catch_twice);
    funcdef!    ((vm) <catch_exception_sig> catch_twice VERSION catch_twice_v1);

    // blk_entry
    block!      ((vm, catch_twice_v1) blk_entry);
    block!      ((vm, catch_twice_v1) blk_normal);
    block!      ((vm, catch_twice_v1) blk_exception1);
    consta!     ((vm, catch_twice_v1) funcref_throw_local = const_funcref_throw_exception);
    inst!       ((vm, catch_twice_v1) blk_entry_call:
        CALL (funcref_throw_local) FUNC(0) (vec![]) CallConvention::Mu,
            normal: blk_normal (vec![]),
            exc   : blk_exception1 (vec![])
    );

    define_block!((vm, catch_twice_v1) blk_entry() {
        blk_entry_call
    });

    // blk_normal
    inst!       ((vm, catch_twice_v1) blk_normal_threadexit:
        THREADEXIT
    );
    define_block!((vm, catch_twice_v1) blk_normal() {
        blk_normal_threadexit
    });

    // blk_exception1
    block!      ((vm, catch_twice_v1) blk_exception2);
    ssa!        ((vm, catch_twice_v1) <ref_int64> exc_arg1);

    // call trigger gc
    funcsig!    ((vm) trigger_gc_sig = () -> ());
    typedef!((vm) ufp_trigger_gc = mu_ufuncptr(trigger_gc_sig));
    constdef!((vm) <ufp_trigger_gc> const_trigger_gc = Constant::ExternSym(C ("trigger_gc_void")));
    consta!((vm,  catch_twice_v1) const_trigger_gc_local = const_trigger_gc);

    inst!((vm,  catch_twice_v1) blk_entry_ccall:
        EXPRCCALL (CallConvention::Foreign(ForeignFFI::C), is_abort: false) const_trigger_gc_local ()
    );

    inst!       ((vm, catch_twice_v1) blk_exception1_call:
        CALL (funcref_throw_local, exc_arg1) FUNC(0) (vec![]) CallConvention::Mu,
            normal: blk_normal (vec![]),
            exc   : blk_exception2 (vec![DestArg::Normal(1)])
    );

    define_block!((vm, catch_twice_v1) blk_exception1() [exc_arg1] {
        blk_entry_ccall,
        blk_exception1_call
    });

    // blk_exception2
    ssa!        ((vm, catch_twice_v1) <ref_int64> blk_exception2_exc_arg1);
    ssa!        ((vm, catch_twice_v1) <ref_int64> exc_arg2);

    ssa!        ((vm, catch_twice_v1) <iref_int64> blk_exception2_iref_exc1);
    inst!       ((vm, catch_twice_v1) blk_exception2_getiref1:
        blk_exception2_iref_exc1 = GETIREF blk_exception2_exc_arg1
    );

    ssa! ((vm, catch_twice_v1) <int64> i_arg);
    inst!       ((vm, catch_twice_v1) blk_entry_convop_arg:
        i_arg = CONVOP (ConvOp::PTRCAST) <ref_int64 int64> blk_exception2_exc_arg1
    );

    inst!       ((vm, catch_twice_v1) blk_entry_print_arg:
        PRINTHEX i_arg
    );

    ssa!        ((vm, catch_twice_v1) <int64> exc_arg1_val);
    inst!       ((vm, catch_twice_v1) blk_exception2_load1:
        exc_arg1_val = LOAD blk_exception2_exc_arg1 (is_ptr: false, order: MemoryOrder::SeqCst)
    );

    ssa!        ((vm, catch_twice_v1) <iref_int64> blk_exception2_iref_exc2);
    inst!       ((vm, catch_twice_v1) blk_exception2_getiref2:
        blk_exception2_iref_exc2 = GETIREF exc_arg2
    );
    ssa!        ((vm, catch_twice_v1) <int64> exc_arg2_val);
    inst!       ((vm, catch_twice_v1) blk_exception2_load2:
        exc_arg2_val = LOAD exc_arg2 (is_ptr: false, order: MemoryOrder::SeqCst)
    );

    ssa!        ((vm, catch_twice_v1) <int64> res);
    inst!       ((vm, catch_twice_v1) blk_exception2_add:
        res = BINOP (BinOp::Add) exc_arg1_val exc_arg2_val
    );

    inst!       ((vm, catch_twice_v1) blk_exception2_ret:
        RET (res)
    );

    define_block!   ((vm, catch_twice_v1) blk_exception2(blk_exception2_exc_arg1) [exc_arg2] {
    //        blk_exception2_getiref1,
            blk_entry_convop_arg,
            blk_entry_print_arg,

            blk_exception2_load1,
    //        blk_exception2_getiref2,
            blk_exception2_load2,

            blk_exception2_add,
            blk_exception2_ret
        });

    define_func_ver!((vm) catch_twice_v1 (entry: blk_entry) {
        blk_entry,
        blk_exception1,
        blk_normal,
        blk_exception2
    });

    emit_test! ((vm)
        catch_twice, catch_twice_test2, catch_twice_test2_v1,
        RET Int,
        EQ,
        catch_exception_sig,
        RET int64(2u64),
    );
}
