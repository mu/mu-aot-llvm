#[macro_use]
mod ir_macros;

extern crate libloading as ll;
use crate::aot_mu;
use mu::runtime::thread::MuThread;
use mu::utils::Address;
use mu::vm::*;
use std::path::PathBuf;
use std::sync::Arc;

pub fn create_dyn_lib<'a>(
    build_fn: &'a dyn Fn() -> VM,
    fns: Vec<String>
) -> ll::Library {
    VM::start_logging_trace();

    let vm = Arc::new(build_fn());
    let mut whitelist = vec![];

    for fn_name in fns.iter() {
        let func_id = vm.id_of(fn_name.as_str());
        whitelist.push(func_id);
    }

    let fn_name = fns.get(0).unwrap();
    let func_id = vm.id_of(fn_name.as_str());

    vm.set_primordial_thread(func_id, true, vec![]);

    let image_name = format!("{}.so", fn_name);

    let primordial_func = vm.handle_from_func(func_id);
    vm.set_primordial_thread(func_id, true, vec![]);

    aot_mu::make_boot_image_for_testing(
        &vm,
        whitelist,
        Some(&*primordial_func),
        None,
        None,
        Vec::new(),
        Vec::new(),
        Vec::new(),
        Vec::new(),
        image_name.clone()
    );

    let image_location = format!("emit/{}", image_name);
    let dylib = PathBuf::from(image_location);

    let lib: ll::Library = unsafe {
        std::mem::transmute(
            ll::os::unix::Library::open(
                Some(dylib.as_os_str()),
                libc::RTLD_NOW | libc::RTLD_GLOBAL
            )
            .unwrap()
        )
    };

    unsafe {
        let tl_address = Address::zero();

        MuThread::current_thread_as_mu_thread(tl_address, vm.clone());
    }

    lib
}

mod test_ir;

// FIXME need to support allocation in the heap (and GC)
#[cfg(test)]
pub mod test_alloc;

#[cfg(test)]
pub mod test_binop;

#[cfg(test)]
pub mod test_call;

#[cfg(test)]
pub mod test_compiler;

#[cfg(test)]
pub mod test_controlflow;

#[cfg(test)]
pub mod test_convop;

// FIXME how to support exceptions in LLVM?
#[cfg(test)]
pub mod test_exception;

#[cfg(test)]
pub mod test_floatingpoint;

#[cfg(test)]
pub mod test_gc;

#[cfg(test)]
pub mod test_global;

#[cfg(test)]
pub mod test_inline;

#[cfg(test)]
pub mod test_instsel;

#[cfg(test)]
pub mod test_int;

#[cfg(test)]
pub mod test_int128;

#[cfg(test)]
pub mod test_mem_inst;

#[cfg(test)]
pub mod test_misc;

#[cfg(test)]
pub mod test_opt;

// Not applicable for the LLVM AOT Compiler
//#[cfg(test)]
//pub mod test_pre_instsel;

#[cfg(test)]
pub mod test_regalloc;

#[cfg(test)]
pub mod test_thread;
