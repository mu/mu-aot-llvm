// Copyright 2017 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

#![allow(unused_imports)]

extern crate mu;

use self::mu::ast::inst::*;
use self::mu::ast::ir::*;
use self::mu::ast::op::*;
use self::mu::ast::types::*;
use self::mu::compiler::*;
use self::mu::linkutils;
use self::mu::linkutils::aot;
use self::mu::utils::LinkedHashMap;
use self::mu::vm::*;

use std::sync::Arc;
use std::sync::RwLock;

use crate::aot_mu;
use std::path::PathBuf;

#[test]
fn test_thread_create() {
    VM::start_logging_trace();

    let vm = Arc::new(primordial_main());

    let func_id = vm.id_of("primordial_main");

    let mut whitelist = vec![];
    whitelist.push(func_id);
    let primordial_func = vm.handle_from_func(func_id);
    vm.set_primordial_thread(func_id, true, vec![]);
    let image_name = String::from("primordial_main_test");

    aot_mu::make_boot_image_for_testing(
        &vm,
        whitelist,
        Some(&*primordial_func),
        None,
        None,
        Vec::new(),
        Vec::new(),
        Vec::new(),
        Vec::new(),
        image_name.clone()
    );

    let image_location = format!("emit/{}", image_name);

    let executable = PathBuf::from(image_location);
    linkutils::exec_path(executable);
}

fn primordial_main() -> VM {
    let vm = VM::new_with_opts("init_mu --generate-llvm");

    funcsig!    ((vm) sig = () -> ());
    funcdecl!   ((vm) <sig> primordial_main);
    funcdef!    ((vm) <sig> primordial_main VERSION primordial_main_v1);

    block!      ((vm, primordial_main_v1) blk_entry);
    inst!       ((vm, primordial_main_v1) blk_entry_threadexit:
        THREADEXIT
    );

    define_block!((vm, primordial_main_v1) blk_entry() {
        blk_entry_threadexit
    });

    define_func_ver!((vm) primordial_main_v1(entry: blk_entry) {
        blk_entry
    });

    vm
}

#[test]
fn test_main_with_retval() {
    VM::start_logging_trace();
    let vm = Arc::new(main_with_retval());

    let func_id = vm.id_of("main_with_retval");
    let mut whitelist = vec![];
    whitelist.push(func_id);
    let func_handle = vm.handle_from_func(func_id);
    let image_name = String::from("main_with_retval");

    aot_mu::make_boot_image(
        &vm,
        whitelist,
        Some(&func_handle),
        None,
        None,
        Vec::new(),
        Vec::new(),
        Vec::new(),
        Vec::new(),
        image_name.clone()
    );

    // run
    let image_location = format!("emit/{}", image_name);

    let executable = PathBuf::from(image_location);
    let output = linkutils::exec_path_nocheck(executable);

    assert!(output.status.code().is_some());

    let ret_code = output.status.code().unwrap();
    println!("return code: {}", ret_code);
    assert!(ret_code == 42);
}

fn main_with_retval() -> VM {
    let vm = VM::new_with_opts("init_mu --generate-llvm");

    typedef!    ((vm) int32 = mu_int(32));
    constdef!   ((vm) <int32> int32_42 = Constant::Int(42));

    funcsig!    ((vm) sig = () -> ());
    funcdecl!   ((vm) <sig> main_with_retval);
    funcdef!    ((vm) <sig> main_with_retval VERSION main_with_retval_v1);

    block!      ((vm, main_with_retval_v1) blk_entry);

    consta!     ((vm, main_with_retval_v1) int32_42_local = int32_42);
    inst!       ((vm, main_with_retval_v1) blk_entry_set_retval:
        SET_RETVAL int32_42_local
    );

    inst!       ((vm, main_with_retval_v1) blk_entry_threadexit:
        THREADEXIT
    );

    define_block!((vm, main_with_retval_v1) blk_entry() {
        blk_entry_set_retval,
        blk_entry_threadexit
    });

    define_func_ver!((vm) main_with_retval_v1(entry: blk_entry) {
        blk_entry
    });

    vm
}

#[test]
fn test_new_thread() {
    VM::start_logging_trace();

    let vm = Arc::new(create_thread());

    let func_id = vm.id_of("the_func");
    let mut whitelist = vec![];
    whitelist.push(func_id);
    let func_id = vm.id_of("create_thread_func");
    whitelist.push(func_id);
    let primordial_func = vm.handle_from_func(func_id);
    vm.set_primordial_thread(func_id, true, vec![]);
    let image_name = String::from("primordial_main_test");

    aot_mu::make_boot_image(
        &vm,
        whitelist,
        Some(&*primordial_func),
        None,
        None,
        Vec::new(),
        Vec::new(),
        Vec::new(),
        Vec::new(),
        image_name.clone()
    );

    let image_location = format!("emit/{}", image_name);

    let executable = PathBuf::from(image_location);

    linkutils::exec_path(executable);
}

//fn empty_function() -> VM {
//    let vm = VM::new_with_opts("--aot-link-static");
//
//    typedef!    ((vm) int64  = mu_int(64));
//
//    constdef!   ((vm) <int64> int64_0 = Constant::Int(0));
//
//    funcsig!    ((vm) sig = () -> (int64));
//    funcdecl!   ((vm) <sig> the_func);
//    funcdef!    ((vm) <sig> the_func VERSION the_func_v1);
//
//    block!      ((vm, the_func_v1) blk_entry);
//
//    inst!((vm, the_func_v1) blk_entry_ret:
//        RET (int64_0)
//    );
//
//    define_block!   ((vm, the_func_v1) blk_entry() {
//        blk_entry_ret
//    });
//
//    define_func_ver!((vm) the_func_v1 (entry: blk_entry) {
//        blk_entry
//    });
//
//    vm
//}

//fn create_thread() -> VM {
//    let vm = VM::new_with_opts("init_mu --generate-llvm --aot-link-static");
//
//    typedef!    ((vm) int64  = mu_int(64));
//    typedef!    ((vm) int32  = mu_int(32));
//    typedef!    ((vm) int8  = mu_int(8));
//    typedef!    ((vm) iref_int8  = mu_iref(int8));
//
//    constdef!   ((vm) <int64> int64_0 = Constant::Int(0));
//    constdef!   ((vm) <int64> int64_1 = Constant::Int(10));
//
//    funcsig!    ((vm) sig = () -> ());
//    funcdecl!   ((vm) <sig> the_func);
//    funcdef!    ((vm) <sig> the_func VERSION the_func_v1);
//
//    consta!     ((vm, the_func_v1) int64_0_local = int64_0);
//
//    block!      ((vm, the_func_v1) blk_entry);
//
//    inst!((vm, the_func_v1) blk_entry_print:
//        PRINTHEX int64_0_local
//    );
//
//    inst!((vm, the_func_v1) blk_entry_ret:
//        THREADEXIT
//    );
//
//    define_block!   ((vm, the_func_v1) blk_entry() {
//        blk_entry_print,
//        blk_entry_ret
//    });
//
//    define_func_ver!((vm) the_func_v1 (entry: blk_entry) {
//        blk_entry
//    });
//
//    typedef!    ((vm) func_ref = mu_funcref(sig));
//    typedef!    ((vm) stack_ref = mu_stackref);
//    typedef!    ((vm) thread_ref = mu_threadref);
//
//    constdef!   ((vm) <func_ref> const_funcref = Constant::FuncRef(the_func));
//
//    funcsig!    ((vm) create_thread_sig = () -> ());
//    funcdecl!   ((vm) <create_thread_sig> create_thread_func);
//    funcdef!    ((vm) <create_thread_sig> create_thread_func VERSION
// create_thread_func_v1);
//
//    ssa!        ((vm, create_thread_func_v1) <stack_ref> the_stack_ref);
//    ssa!        ((vm, create_thread_func_v1) <thread_ref> the_thread_ref);
//    consta!     ((vm, create_thread_func_v1) const_funcref_local =
// const_funcref);
//
//    // blk_entry
//    block!      ((vm, create_thread_func_v1) blk_entry);
//
//    inst!((vm, create_thread_func_v1) blk_entry_create_stack:
//        the_stack_ref = NEWSTACK const_funcref_local
//    );
//
//    ssa! ((vm, create_thread_func_v1) <int64> arg_u32);
//    inst!((vm, create_thread_func_v1) blk_entry_conv:
//            arg_u32 = CONVOP (ConvOp::PTRCAST) <iref_int8 int64> the_stack_ref
//        );
//
//    inst!((vm, create_thread_func_v1) blk_entry_create_stack_print:
//        PRINTHEX arg_u32
//    );
//
//    inst!((vm, create_thread_func_v1) blk_entry_create_thread:
//        the_thread_ref = NEWTHREAD STACK: the_stack_ref, ARGS: ()
//    );
//
//    ssa! ((vm, create_thread_func_v1) <int64> nt_u32);
//    inst!((vm, create_thread_func_v1) blk_entry_conv2:
//            nt_u32 = CONVOP (ConvOp::PTRCAST) <iref_int8 int64> the_thread_ref
//        );
//
//    inst!((vm, create_thread_func_v1) blk_entry_create_thread_print:
//        PRINTHEX nt_u32
//    );
//
//    inst!((vm, create_thread_func_v1) blk_entry_ret:
//        THREADEXIT
//    );
//
//    define_block!((vm, create_thread_func_v1) blk_entry() {
//            blk_entry_create_stack,
//            blk_entry_conv,
//            blk_entry_create_stack_print,
//            blk_entry_create_thread,
//            blk_entry_conv2,
//            blk_entry_create_thread_print,
//    //        blk_entry_start_thread,
//            blk_entry_ret
//        });
//
//    define_func_ver!((vm) create_thread_func_v1 (entry: blk_entry) {
//        blk_entry
//    });
//
//    vm
//}

fn create_thread() -> VM {
    let vm = VM::new_with_opts("init_mu --generate-llvm --aot-link-static");

    typedef!    ((vm) int64  = mu_int(64));
    typedef!    ((vm) int32  = mu_int(32));
    typedef!    ((vm) int8  = mu_int(8));
    typedef!    ((vm) iref_int8  = mu_iref(int8));

    constdef!   ((vm) <int64> int64_0 = Constant::Int(0));
    constdef!   ((vm) <int64> int64_1 = Constant::Int(10));

    funcsig!    ((vm) sig = (int64) -> ());
    funcdecl!   ((vm) <sig> the_func);
    funcdef!    ((vm) <sig> the_func VERSION the_func_v1);
    ssa!        ((vm, the_func_v1) <int64> a);

    block!      ((vm, the_func_v1) blk_entry);

    inst!((vm, the_func_v1) blk_entry_print:
        PRINTHEX a
    );

    inst!((vm, the_func_v1) blk_entry_ret:
        THREADEXIT
    );

    define_block!   ((vm, the_func_v1) blk_entry(a) {
        blk_entry_print,
        blk_entry_ret
    });

    define_func_ver!((vm) the_func_v1 (entry: blk_entry) {
        blk_entry
    });

    typedef!    ((vm) func_ref = mu_funcref(sig));
    typedef!    ((vm) stack_ref = mu_stackref);
    typedef!    ((vm) thread_ref = mu_threadref);

    constdef!   ((vm) <func_ref> const_funcref = Constant::FuncRef(the_func));

    funcsig!    ((vm) create_thread_sig = () -> ());
    funcdecl!   ((vm) <create_thread_sig> create_thread_func);
    funcdef!    ((vm) <create_thread_sig> create_thread_func VERSION create_thread_func_v1);

    ssa!        ((vm, create_thread_func_v1) <stack_ref> the_stack_ref);
    ssa!        ((vm, create_thread_func_v1) <thread_ref> the_thread_ref);
    consta!     ((vm, create_thread_func_v1) const_funcref_local = const_funcref);

    // blk_entry
    block!      ((vm, create_thread_func_v1) blk_entry);

    inst!((vm, create_thread_func_v1) blk_entry_create_stack:
        the_stack_ref = NEWSTACK const_funcref_local
    );

    ssa! ((vm, create_thread_func_v1) <int64> arg_u32);
    inst!((vm, create_thread_func_v1) blk_entry_conv:
        arg_u32 = CONVOP (ConvOp::PTRCAST) <iref_int8 int64> the_stack_ref
    );

    inst!((vm, create_thread_func_v1) blk_entry_create_stack_print:
        PRINTHEX arg_u32
    );

    consta!     ((vm, create_thread_func_v1) int64_0_local = int64_1);

    inst!((vm, create_thread_func_v1) blk_entry_create_thread:
        the_thread_ref = NEWTHREAD STACK: the_stack_ref, ARGS: (int64_0_local)
    );

    ssa! ((vm, create_thread_func_v1) <int64> nt_u32);
    inst!((vm, create_thread_func_v1) blk_entry_conv2:
        nt_u32 = CONVOP (ConvOp::PTRCAST) <iref_int8 int64> the_thread_ref
    );

    inst!((vm, create_thread_func_v1) blk_entry_create_thread_print:
        PRINTHEX nt_u32
    );

    inst!((vm, create_thread_func_v1) blk_entry_ret:
        THREADEXIT
    );

    define_block!((vm, create_thread_func_v1) blk_entry() {
            blk_entry_create_stack,
            blk_entry_conv,
            blk_entry_create_stack_print,
            blk_entry_create_thread,
            blk_entry_conv2,
            blk_entry_create_thread_print,
    //        blk_entry_start_thread,
            blk_entry_ret
        });

    define_func_ver!((vm) create_thread_func_v1 (entry: blk_entry) {
        blk_entry
    });

    vm
}
