// Copyright 2017 The Australian National University
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

extern crate llvm_sys;

use mu::ast::ir::*;
use mu::ast::ptr::*;
use mu::ast::types::*;
use mu::compiler::CompilerPass;
use mu::vm::VM;

use llvm_sys::analysis::LLVMVerifierFailureAction::*;
use llvm_sys::analysis::*;
use llvm_sys::core::*;
use llvm_sys::execution_engine::*;
use llvm_sys::prelude::*;
use llvm_sys::target::*;
use llvm_sys::target_machine::*;

use crate::instr::*;

use llvm_sys::initialization::{
    LLVMInitializeCodeGen, LLVMInitializeCore, LLVMInitializeScalarOpts
};
use llvm_sys::transforms::scalar::{
    LLVMAddCFGSimplificationPass, LLVMAddGVNPass, LLVMAddReassociatePass
};
use mu::utils::LinkedHashMap;
use std::any::Any;
use std::borrow::Borrow;
use std::collections::HashMap;
use std::ffi::CString;
use std::os::raw::{c_char, c_double, c_uint, c_ulong};
use std::process::Command;
use std::{path, ptr};

pub struct LLVMCodeEmission {
    name: &'static str,
    whitelisted_fns: Vec<MuID>
}

impl LLVMCodeEmission {
    pub fn new(wl_fns: Vec<MuID>) -> LLVMCodeEmission {
        LLVMCodeEmission {
            name: "LLVM Code Emission",
            whitelisted_fns: wl_fns
        }
    }
}

impl CompilerPass for LLVMCodeEmission {
    fn name(&self) -> &'static str {
        self.name
    }

    fn as_any(&self) -> &dyn Any {
        self
    }

    fn visit_function(&mut self, vm: &VM, func: &mut MuFunctionVersion) {
        let function_name = vm.name_of(func.func_id);
        let function_name = Arc::new(mu::ast::ir::mangle_name(function_name));
        let mut llvm_internal_context = AOTLLVMInternalContext::new(
            function_name.as_str(),
            &mut self.whitelisted_fns,
            vm
        );

        // all extern calls need to be declared at the top level of the module
        // thus, it is necessary to visit blocks and instructions before code
        // emission
        for (_, ref mut block) in
            func.content.as_mut().unwrap().blocks.iter_mut()
        {
            if block.is_receiving_exception_arg() {
                unsafe {
                    let mut params = vec![];
                    let function_name =
                        "__gxx_personality_v0\0".as_ptr() as *const c_char;
                    let function = LLVMGetNamedFunction(
                        llvm_internal_context.module,
                        function_name
                    );
                    if function.is_null() {
                        LLVMAddFunction(
                            llvm_internal_context.module,
                            function_name,
                            LLVMFunctionType(
                                LLVMPointerType(LLVMInt8Type(), 0),
                                params.as_mut_ptr(),
                                0,
                                false as i32
                            )
                        );
                    }

                    let function_name =
                        "__cxa_begin_catch\0".as_ptr() as *const c_char;
                    let mut params = vec![LLVMPointerType(LLVMInt8Type(), 0)];
                    let function = LLVMGetNamedFunction(
                        llvm_internal_context.module,
                        function_name
                    );

                    if function.is_null() {
                        LLVMAddFunction(
                            llvm_internal_context.module,
                            function_name,
                            LLVMFunctionType(
                                LLVMPointerType(LLVMInt8Type(), 0),
                                params.as_mut_ptr(),
                                1,
                                false as i32
                            )
                        );
                    }
                }

                llvm_internal_context.exception_blocks.push(block.hdr.id());
            }

            for inst in block.content.as_mut().unwrap().body.iter_mut() {
                visit_inst_custom(
                    vm,
                    &mut llvm_internal_context,
                    &mut func.context,
                    inst
                );
            }
        }

        // emit the code based on llvm
        unsafe {
            emit_llvm_asm_code(func, vm, &mut llvm_internal_context);
        }
    }
}

// declare all extern functions
fn visit_inst_custom(
    _vm: &VM,
    llvm_internal_context: &mut AOTLLVMInternalContext,
    _func_context: &mut FunctionContext,
    node: &P<TreeNode>
) {
    match &node.v {
        // visited to declare external function prior the LLVM generation
        TreeNode_::Instruction(ins) => match &ins.v {
            mu::ast::inst::Instruction_::ExprCCall { data, is_abort: _ } => unsafe {
                let function_name = ins.ops.get(data.func).unwrap();
                match &function_name.v {
                    TreeNode_::Value(v) => {
                        let function_sig = match &v.ty.v {
                            MuType_::UFuncPtr(func_sig) => func_sig,
                            _ => return
                        };

                        let function_name = match &v.v {
                            Value_::Constant(c) => match c {
                                Constant::ExternSym(name) => name,
                                _ => return
                            },
                            _ => return
                        };

                        let function_name =
                            CString::new((*function_name).clone().as_str())
                                .unwrap();
                        let function_sig = create_llvm_function_type(
                            llvm_internal_context,
                            function_sig
                        );

                        let named_fn = LLVMGetNamedFunction(
                            llvm_internal_context.module,
                            function_name.as_ptr()
                        );
                        if named_fn.is_null() {
                            LLVMAddFunction(
                                llvm_internal_context.module,
                                function_name.as_ptr(),
                                function_sig
                            );
                        }
                    }
                    _ => return
                };
            },
            // visited to declare function for write barrier
            mu::ast::inst::Instruction_::Store { .. } => unsafe {
                #[cfg(feature = "gencopy")]
                {
                    let function_name =
                        MuName::new(String::from("llvm_write_barrier"));
                    let i8_type = Arc::new(MuType::new(0, MuType_::Int(8)));
                    let llvm_i8_ptr_type =
                        LLVMPointerType(LLVMInt8Type(), 1 as c_uint);
                    let llvm_arg_types = vec![
                        llvm_i8_ptr_type,
                        LLVMPointerType(llvm_i8_ptr_type, 1 as c_uint),
                    ];
                    let return_type = LLVMVoidType();

                    add_function_to_module(
                        llvm_internal_context,
                        function_name,
                        llvm_arg_types,
                        return_type,
                        false
                    );
                }
            },
            // visited to declare function muentry_new_stack
            mu::ast::inst::Instruction_::NewStack(_) => unsafe {
                let function_name =
                    MuName::new(String::from("muentry_new_stack"));
                let i64_type = Arc::new(MuType::new(0, MuType_::Int(64)));
                let i8_type = Arc::new(MuType::new(0, MuType_::Int(8)));
                let refi8_type =
                    Arc::new(MuType::new(0, MuType_::Ref(i8_type)));

                let llvm_refi8_type =
                    gen_llvm_type(llvm_internal_context, &refi8_type);
                let llvm_i64_type =
                    gen_llvm_type(llvm_internal_context, &i64_type);

                let llvm_arg_types =
                    vec![llvm_refi8_type.clone(), llvm_i64_type];
                let return_type = llvm_refi8_type;

                add_function_to_module(
                    llvm_internal_context,
                    function_name,
                    llvm_arg_types,
                    return_type,
                    false
                );
            },

            // visited to declare external function muentry_set_retval prior the
            // LLVM generation
            mu::ast::inst::Instruction_::SetRetval(_) => unsafe {
                let function_name =
                    MuName::new(String::from("muentry_set_retval"));
                let arg_type = Arc::new(MuType::new(0, MuType_::Int(32)));
                let llvm_arg_type =
                    gen_llvm_type(llvm_internal_context, &arg_type);

                let llvm_arg_types = vec![llvm_arg_type];

                add_function_to_module(
                    llvm_internal_context,
                    function_name,
                    llvm_arg_types,
                    LLVMVoidType(),
                    false
                );
            },
            // visited to declare external function muentry_get_thread_local and
            // muentry_thread_exit prior the LLVM generation
            mu::ast::inst::Instruction_::NewThread { .. } => unsafe {
                let function_name =
                    MuName::new(String::from("muentry_stack_setup_args"));

                let i8_type = Arc::new(MuType::new(0, MuType_::Int(8)));
                let refi8_type =
                    Arc::new(MuType::new(0, MuType_::Ref(i8_type)));
                let llvm_refi8_type =
                    gen_llvm_type(llvm_internal_context, &refi8_type);
                let llvm_i32_type = LLVMInt32Type();

                let llvm_arg_types = vec![
                    llvm_refi8_type.clone(),
                    llvm_i32_type,
                    llvm_refi8_type.clone(),
                ];

                add_function_to_module(
                    llvm_internal_context,
                    function_name,
                    llvm_arg_types,
                    LLVMVoidType(),
                    false
                );

                let function_name =
                    MuName::new(String::from("muentry_new_thread_normal"));

                let llvm_arg_types =
                    vec![llvm_refi8_type.clone(), llvm_refi8_type.clone()];

                let return_type = llvm_refi8_type.clone();

                add_function_to_module(
                    llvm_internal_context,
                    function_name,
                    llvm_arg_types,
                    return_type,
                    false
                );

                let function_name = MuName::new(String::from("calloc"));
                let arg_type = Arc::new(MuType::new(0, MuType_::Int(64)));
                let return_type = llvm_refi8_type.clone();
                let llvm_arg_type =
                    gen_llvm_type(llvm_internal_context, &arg_type);
                let llvm_arg_types =
                    vec![llvm_arg_type.clone(), llvm_arg_type.clone()];

                add_function_to_module(
                    llvm_internal_context,
                    function_name,
                    llvm_arg_types,
                    return_type,
                    false
                );
            },
            // visited to declare function allocation function
            mu::ast::inst::Instruction_::New(_)
            | mu::ast::inst::Instruction_::NewHybrid(_, _) => unsafe {
                let function_name = MuName::new(String::from("allocate_mem"));
                let i8_type = Arc::new(MuType::new(0, MuType_::Int(8)));
                let refi8_type =
                    Arc::new(MuType::new(0, MuType_::Ref(i8_type)));
                let llvm_refi8_type =
                    gen_llvm_type(llvm_internal_context, &refi8_type);
                let return_type = llvm_refi8_type.clone();
                let arg_type = Arc::new(MuType::new(0, MuType_::Int(64)));
                let llvm_arg_type =
                    gen_llvm_type(llvm_internal_context, &arg_type);
                let arg_type_i32 = Arc::new(MuType::new(0, MuType_::Int(32)));
                let llvm_arg_type_i32 =
                    gen_llvm_type(llvm_internal_context, &arg_type_i32);
                let llvm_arg_types = vec![
                    llvm_arg_type.clone(),
                    llvm_arg_type.clone(),
                    llvm_arg_type.clone(),
                    llvm_arg_type_i32,
                ];

                add_function_to_module(
                    llvm_internal_context,
                    function_name,
                    llvm_arg_types,
                    return_type,
                    false
                );

                let function_name =
                    MuName::new(String::from("allocate_mem_small"));
                let i8_type = Arc::new(MuType::new(0, MuType_::Int(8)));
                let refi8_type =
                    Arc::new(MuType::new(0, MuType_::Ref(i8_type)));
                let llvm_refi8_type =
                    gen_llvm_type(llvm_internal_context, &refi8_type);
                let return_type = llvm_refi8_type.clone();
                let arg_type = Arc::new(MuType::new(0, MuType_::Int(64)));
                let llvm_arg_type =
                    gen_llvm_type(llvm_internal_context, &arg_type);
                let arg_type_i32 = Arc::new(MuType::new(0, MuType_::Int(32)));
                let llvm_arg_type_i32 =
                    gen_llvm_type(llvm_internal_context, &arg_type_i32);
                let llvm_arg_types = vec![
                    llvm_arg_type.clone(),
                    llvm_arg_type.clone(),
                    llvm_arg_type.clone(),
                    LLVMInt1Type(),
                ];

                add_function_to_module(
                    llvm_internal_context,
                    function_name,
                    llvm_arg_types,
                    return_type,
                    false
                );

                let function_name =
                    MuName::new(String::from("llvm.experimental.stackmap"));
                let i8_type = Arc::new(MuType::new(0, MuType_::Int(8)));
                let refi8_type =
                    Arc::new(MuType::new(0, MuType_::Ref(i8_type)));
                let llvm_refi8_type =
                    gen_llvm_type(llvm_internal_context, &refi8_type);

                let i32_type = Arc::new(MuType::new(0, MuType_::Int(32)));
                let llvm_i32_type =
                    gen_llvm_type(llvm_internal_context, &i32_type);

                let i64_type = Arc::new(MuType::new(0, MuType_::Int(64)));
                let llvm_i64_type =
                    gen_llvm_type(llvm_internal_context, &i64_type);

                let return_type = LLVMVoidType();

                let llvm_arg_types =
                    vec![llvm_i64_type.clone(), llvm_i32_type.clone()];

                add_function_to_module(
                    llvm_internal_context,
                    function_name,
                    llvm_arg_types,
                    return_type,
                    true
                );
            },
            mu::ast::inst::Instruction_::CommonInst_GetThreadLocal => unsafe {
                let function_name =
                    MuName::new(String::from("muentry_get_thread_local"));

                let llvm_arg_types = vec![];

                let return_type = LLVMPointerType(LLVMInt8Type(), 1);

                add_function_to_module(
                    llvm_internal_context,
                    function_name,
                    llvm_arg_types,
                    return_type,
                    false
                );
            },
            mu::ast::inst::Instruction_::ThreadExit => unsafe {
                let function_name =
                    MuName::new(String::from("muentry_get_thread_local"));

                let llvm_arg_types = vec![];

                let return_type = LLVMPointerType(LLVMInt8Type(), 1);

                add_function_to_module(
                    llvm_internal_context,
                    function_name,
                    llvm_arg_types,
                    return_type,
                    false
                );

                let function_name =
                    MuName::new(String::from("muentry_thread_exit"));

                let arg_type = LLVMPointerType(LLVMInt8Type(), 0);
                let llvm_arg_types = vec![arg_type];

                add_function_to_module(
                    llvm_internal_context,
                    function_name,
                    llvm_arg_types,
                    LLVMVoidType(),
                    false
                );
            },
            // visited to identify exception block
            mu::ast::inst::Instruction_::Call { data: _, resume } => {
                unsafe {
                    let mut params = vec![];
                    let function_name =
                        "__gxx_personality_v0\0".as_ptr() as *const c_char;
                    let function = LLVMGetNamedFunction(
                        llvm_internal_context.module,
                        function_name
                    );
                    if function.is_null() {
                        LLVMAddFunction(
                            llvm_internal_context.module,
                            function_name,
                            LLVMFunctionType(
                                LLVMPointerType(LLVMInt8Type(), 0),
                                params.as_mut_ptr(),
                                0,
                                false as i32
                            )
                        );
                    }

                    let function_name =
                        "__cxa_begin_catch\0".as_ptr() as *const c_char;
                    let mut params = vec![LLVMPointerType(LLVMInt8Type(), 0)];
                    let function = LLVMGetNamedFunction(
                        llvm_internal_context.module,
                        function_name
                    );

                    if function.is_null() {
                        LLVMAddFunction(
                            llvm_internal_context.module,
                            function_name,
                            LLVMFunctionType(
                                LLVMPointerType(LLVMInt8Type(), 0),
                                params.as_mut_ptr(),
                                1,
                                false as i32
                            )
                        );
                    }

                    let function_name = "add_spilled_address_to_roots\0"
                        .as_ptr()
                        as *const c_char;
                    let mut params = vec![LLVMInt64Type()];
                    let function = LLVMGetNamedFunction(
                        llvm_internal_context.module,
                        function_name
                    );

                    if function.is_null() {
                        LLVMAddFunction(
                            llvm_internal_context.module,
                            function_name,
                            LLVMFunctionType(
                                LLVMVoidType(),
                                params.as_mut_ptr(),
                                1,
                                false as i32
                            )
                        );
                    }
                }

                let exception_block = resume.exn_dest.target.id();
                llvm_internal_context.exception_blocks.push(exception_block);

                if resume.normal_dest.target.id() == resume.exn_dest.target.id()
                {
                    llvm_internal_context
                        .dup_blocks
                        .push(resume.normal_dest.target.id());
                }
            }
            // visited to declare intrinsics to calculate overflow
            mu::ast::inst::Instruction_::BinOpWithStatus(
                bop,
                bop_status,
                op1,
                _op2
            ) => unsafe {
                let op_type = &ins.ops.get(*op1).unwrap().as_value().ty;
                let op_type_name = match op_type.v {
                    MuType_::Int(n) => format!("i{}", n),
                    _ => panic!("Operand should be integer.")
                };
                let op_name = match bop {
                    mu::ast::op::BinOp::Add => String::from("add"),
                    mu::ast::op::BinOp::Sub => String::from("sub"),
                    mu::ast::op::BinOp::Mul => String::from("mul"),
                    _ => panic!("Unsupported operation.")
                };

                let llvm_op_type = match op_type.v {
                    MuType_::Int(size) => match size {
                        1 => LLVMInt1Type(),
                        8 => LLVMInt8Type(),
                        16 => LLVMInt16Type(),
                        32 => LLVMInt32Type(),
                        64 => LLVMInt64Type(),
                        128 => LLVMInt128Type(),
                        n => LLVMIntType(n as c_uint)
                    },
                    _ => panic!("operand types should be integer only.")
                };

                let mut llvm_arg_types = vec![llvm_op_type, llvm_op_type];

                let mut llvm_ret_tuple = vec![llvm_op_type, LLVMInt1Type()];

                let return_type = LLVMStructType(
                    llvm_ret_tuple.as_mut_ptr(),
                    2,
                    false as i32
                );

                let function_sig = llvm_sys::core::LLVMFunctionType(
                    return_type,
                    llvm_arg_types.as_mut_ptr(),
                    llvm_arg_types.len() as c_uint,
                    false as LLVMBool
                );

                if bop_status.flag_c {
                    let function_name = MuName::new(format!(
                        "llvm.u{}.with.overflow.{}",
                        op_name, op_type_name
                    ));
                    let function_name =
                        CString::new((*function_name).clone().as_str())
                            .unwrap();
                    let named_fn = LLVMGetNamedFunction(
                        llvm_internal_context.module,
                        function_name.as_ptr()
                    );
                    if named_fn.is_null() {
                        LLVMAddFunction(
                            llvm_internal_context.module,
                            function_name.as_ptr(),
                            function_sig
                        );
                    }
                }
                if bop_status.flag_v {
                    let function_name = MuName::new(format!(
                        "llvm.s{}.with.overflow.{}",
                        op_name, op_type_name
                    ));
                    let function_name =
                        CString::new((*function_name).clone().as_str())
                            .unwrap();

                    let named_fn = LLVMGetNamedFunction(
                        llvm_internal_context.module,
                        function_name.as_ptr()
                    );
                    if named_fn.is_null() {
                        LLVMAddFunction(
                            llvm_internal_context.module,
                            function_name.as_ptr(),
                            function_sig
                        );
                    }
                }
            },
            mu::ast::inst::Instruction_::Throw(_) => unsafe {
                let function_name =
                    MuName::new(String::from("__cxa_allocate_exception"));
                let arg_type = Arc::new(MuType::new(0, MuType_::Int(64)));
                let i8_type = Arc::new(MuType::new(0, MuType_::Int(8)));
                let ret_type = Arc::new(MuType::new(0, MuType_::Ref(i8_type)));

                let function_sig = Arc::new(MuFuncSig {
                    hdr: MuEntityHeader::unnamed(0),
                    ret_tys: vec![ret_type],
                    arg_tys: vec![arg_type]
                });

                let function_name =
                    CString::new((*function_name).clone().as_str()).unwrap();
                let function_sig = create_llvm_function_type(
                    llvm_internal_context,
                    &function_sig
                );

                let named_fn = LLVMGetNamedFunction(
                    llvm_internal_context.module,
                    function_name.as_ptr()
                );
                if named_fn.is_null() {
                    LLVMAddFunction(
                        llvm_internal_context.module,
                        function_name.as_ptr(),
                        function_sig
                    );
                }

                let function_name = MuName::new(String::from("__cxa_throw"));
                let i8_type = Arc::new(MuType::new(0, MuType_::Int(8)));
                let arg_type = Arc::new(MuType::new(0, MuType_::Ref(i8_type)));

                let function_sig = Arc::new(MuFuncSig {
                    hdr: MuEntityHeader::unnamed(0),
                    ret_tys: vec![],
                    arg_tys: vec![
                        arg_type.clone(),
                        arg_type.clone(),
                        arg_type.clone(),
                    ]
                });

                let function_name =
                    CString::new((*function_name).clone().as_str()).unwrap();
                let function_sig = create_llvm_function_type(
                    llvm_internal_context,
                    &function_sig
                );

                let named_fn = LLVMGetNamedFunction(
                    llvm_internal_context.module,
                    function_name.as_ptr()
                );
                if named_fn.is_null() {
                    LLVMAddFunction(
                        llvm_internal_context.module,
                        function_name.as_ptr(),
                        function_sig
                    );
                }
            },
            mu::ast::inst::Instruction_::PrintHex(_) => unsafe {
                let function_name =
                    MuName::new(String::from("muentry_print_hex"));
                let arg_type = Arc::new(MuType::new(0, MuType_::Int(64)));

                let function_sig = Arc::new(MuFuncSig {
                    hdr: MuEntityHeader::unnamed(0),
                    ret_tys: vec![],
                    arg_tys: vec![arg_type]
                });

                let function_name =
                    CString::new((*function_name).clone().as_str()).unwrap();
                let function_sig = create_llvm_function_type(
                    llvm_internal_context,
                    &function_sig
                );

                let named_fn = LLVMGetNamedFunction(
                    llvm_internal_context.module,
                    function_name.as_ptr()
                );

                if named_fn.is_null() {
                    LLVMAddFunction(
                        llvm_internal_context.module,
                        function_name.as_ptr(),
                        function_sig
                    );
                }
            },
            _ => ()
        },
        _ => ()
    }
}

pub unsafe fn add_function_to_module(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    function_name: P<String>,
    mut llvm_arg_types: Vec<LLVMTypeRef>,
    return_type: LLVMTypeRef,
    var_arg: bool
) {
    let function_sig = llvm_sys::core::LLVMFunctionType(
        return_type,
        llvm_arg_types.as_mut_ptr(),
        llvm_arg_types.len() as c_uint,
        var_arg as LLVMBool
    );

    let function_name =
        CString::new((*function_name).clone().as_str()).unwrap();

    let named_fn = LLVMGetNamedFunction(
        llvm_internal_context.module,
        function_name.as_ptr()
    );
    if named_fn.is_null() {
        LLVMAddFunction(
            llvm_internal_context.module,
            function_name.as_ptr(),
            function_sig
        );
    }
}

pub struct AOTLLVMInternalContext<'a> {
    pub context: LLVMContextRef,
    pub builder: LLVMBuilderRef,
    pub module: LLVMModuleRef,
    pub blocks_map: HashMap<MuID, LLVMBasicBlockRef>,
    pub block_args:
        HashMap<MuID, (LLVMBasicBlockRef, Vec<(LLVMTypeRef, LLVMValueRef)>)>,
    pub dup_blocks: Vec<MuID>,
    pub orig_dup_block_map: HashMap<MuID, MuID>,
    pub whitelisted_fns: &'a mut Vec<MuID>,
    pub global_store: HashMap<MuID, LLVMValueRef>,
    pub struct_map: HashMap<StructTag, LLVMTypeRef>,
    pub exception_blocks: Vec<MuID>,
    pub curr_fn: Option<LLVMValueRef>,
    pub vm: &'a VM
}

impl<'a> AOTLLVMInternalContext<'a> {
    pub fn new(
        module_name: &str,
        whitelisted_fns: &'a mut Vec<MuID>,
        vm: &'a VM
    ) -> AOTLLVMInternalContext<'a> {
        let builder;
        let module;
        let context;
        let blocks_map = HashMap::new();
        let block_args = HashMap::new();
        let dup_blocks = vec![];
        let orig_dup_block_map = HashMap::new();
        let module_name = module_name.to_owned() + "\0";
        let global_store = HashMap::new();
        let struct_map = HashMap::new();
        let exception_blocks: Vec<MuID> = vec![];
        unsafe {
            context = llvm_sys::core::LLVMContextCreate();
            builder = llvm_sys::core::LLVMCreateBuilderInContext(context);
            module = llvm_sys::core::LLVMModuleCreateWithName(
                module_name.as_ptr() as *const _
            );
        }

        AOTLLVMInternalContext {
            context,
            builder,
            module,
            blocks_map,
            block_args,
            dup_blocks,
            orig_dup_block_map,
            whitelisted_fns,
            global_store,
            struct_map,
            exception_blocks,
            curr_fn: None,
            vm
        }
    }

    pub fn builder(&self) -> LLVMBuilderRef {
        self.builder
    }
}

unsafe fn emit_llvm_asm_code(
    func: &mut MuFunctionVersion,
    vm: &VM,
    llvm_internal_context: &mut AOTLLVMInternalContext
) {
    let function_name = mu::ast::ir::mangle_name(vm.name_of(func.func_id));
    let function_name = Arc::new(function_name);
    let function_sig = &func.sig;

    declare_global_variables(llvm_internal_context, vm);
    declare_whitelisted_functions(llvm_internal_context, func.func_id);

    let function_type =
        create_llvm_function_type(llvm_internal_context, function_sig);

    let function_name = CString::new(function_name.as_str()).unwrap();

    let function = LLVMAddFunction(
        llvm_internal_context.module,
        function_name.as_ptr() as *const c_char,
        function_type
    );

    // set function as GC-able
    LLVMSetGC(function, "statepoint-example\0".as_ptr() as *const c_char);

    llvm_internal_context.curr_fn = Some(function);

    // functions can be called by their unmangled name
    {
        let function_name = vm.get_name_for_func(func.func_id);
        let function_name = CString::new(function_name.as_str()).unwrap();

        LLVMAddAlias(
            llvm_internal_context.module,
            LLVMTypeOf(function),
            function,
            function_name.as_ptr() as *const c_char
        );
    }

    let func_content = func.content.as_ref().unwrap();
    let entry_block = func_content.get_entry_block();
    let blocks = &func_content.blocks;
    let mut all_blocks = LinkedHashMap::new();
    for (label, block) in blocks {
        all_blocks.insert(*label, block.clone());
    }

    for (label, block) in add_duplicate_blocks(llvm_internal_context, blocks) {
        all_blocks.insert(label, block);
    }

    let mut set_fun_params = false;
    for (label, block) in &all_blocks {
        // do not generate code for Mu dead blocks
        if set_fun_params && block.control_flow.preds.is_empty() {
            continue;
        }

        let llvm_block =
            gen_llvm_block(llvm_internal_context, function, &block);

        // First mu block sets the function parameters
        if !set_fun_params {
            set_entry_block_params(
                llvm_internal_context,
                block,
                llvm_block,
                function
            );
            set_fun_params = true;
        }

        llvm_internal_context.blocks_map.insert(*label, llvm_block);
    }

    let mut instr_args = vec![];

    // entry block does not need phi nodes as params
    let mut create_phi_nodes = false;

    // first process all blocks parameters creating phi nodes when necessary
    // (no need to create phi nodes for first block)
    for (label, block) in &all_blocks {
        info!("Compiling block {} to LLVM IR", block.hdr.name());

        // eliminating Mu dead blocks
        if block.hdr.id() != entry_block.hdr.id()
            && block.control_flow.preds.is_empty()
        {
            continue;
        }

        // process block parameters
        let mut store = process_block_params(
            llvm_internal_context,
            block,
            create_phi_nodes
        );
        create_phi_nodes = true;

        trace!("Processing block contents");

        if block.is_receiving_exception_arg()
            || llvm_internal_context
                .exception_blocks
                .contains(&block.hdr.id())
        {
            instr_args.push(gen_llvm_exception_block_contents(
                llvm_internal_context,
                label,
                block,
                &mut store,
                function
            ));
        } else {
            instr_args.push(gen_llvm_block_contents(
                llvm_internal_context,
                label,
                block,
                &mut store
            ));
        }
    }

    // then process the instruction arguments, adding incoming edges to phi
    // nodes for branching instructions
    for mut intr_arg in instr_args.drain(..) {
        for (key, values) in intr_arg.drain() {
            add_incoming_to_phi_nodes(llvm_internal_context, key, values);
        }
    }

    generate_llvm_ir(&llvm_internal_context, func, vm);
    //    generate_llvm_asm(&llvm_internal_context, func, vm);

    llvm_internal_context.curr_fn = None;
}

fn add_duplicate_blocks(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    blocks: &LinkedHashMap<usize, Block>
) -> LinkedHashMap<usize, Block> {
    let mut dup_blocks = LinkedHashMap::new();
    let mut dup_blocks_map = vec![];

    for block_id in &llvm_internal_context.dup_blocks {
        let orig_block = blocks.get(block_id).unwrap();

        let dup_block = Block {
            hdr: MuEntityHeader::named(
                llvm_internal_context.vm.next_id(),
                Arc::new(format!("{}_normal", orig_block.hdr.name()))
            ),
            content: Some(BlockContent {
                args: orig_block.content.as_ref().unwrap().args.clone(),
                exn_arg: None,
                body: orig_block.content.as_ref().unwrap().body.clone(),
                keepalives: orig_block
                    .content
                    .as_ref()
                    .unwrap()
                    .keepalives
                    .clone()
            }),
            trace_hint: orig_block.trace_hint.clone(),
            control_flow: orig_block.control_flow.clone()
        };
        let dup_block_id = dup_block.hdr.id();
        dup_blocks.insert(dup_block_id, dup_block);
        dup_blocks_map.push((orig_block.hdr.id(), dup_block_id));
    }

    for (orig_id, dup_id) in dup_blocks_map {
        llvm_internal_context
            .orig_dup_block_map
            .insert(orig_id, dup_id);
    }

    dup_blocks
}

unsafe fn set_entry_block_params(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    block: &Block,
    llvm_block: LLVMBasicBlockRef,
    function: LLVMValueRef
) {
    let args = block.content.as_ref().unwrap();
    let mut block_args = vec![];
    for i in 0..args.args.len() {
        let param = LLVMGetParam(function, i as u32);
        // FIXME Could be anything else other than SSA variable?
        let param_name = format!("{}\0", args.args.get(i).unwrap().hdr.name());
        let param_type =
            gen_llvm_type(llvm_internal_context, &args.args.get(i).unwrap().ty);

        llvm_sys::core::LLVMSetValueName(
            param,
            param_name.as_str().as_ptr() as *const c_char
        );

        block_args.push((param_type, param));
    }
    llvm_internal_context
        .block_args
        .insert(block.hdr.id(), (llvm_block, block_args));
}

unsafe fn add_incoming_to_phi_nodes(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    key: MuID,
    values: Vec<(LLVMBasicBlockRef, Vec<(LLVMTypeRef, LLVMValueRef)>)>
) {
    for (source, params) in values {
        let mut phi_nodes = vec![];

        let (_, previous_args) =
            llvm_internal_context.block_args.get_mut(&key).unwrap();

        for ((prev_arg_type, prev_arg), (_, new_arg)) in
            previous_args.iter().zip(params)
        {
            LLVMAddIncoming(
                *prev_arg,
                vec![new_arg].as_mut_slice().as_mut_ptr(),
                vec![source].as_mut_slice().as_mut_ptr(),
                1 as c_uint
            );

            phi_nodes.push((*prev_arg_type, *prev_arg));
        }
    }
}
unsafe fn declare_global_variables(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    vm: &VM
) {
    for (var, value) in vm.globals().read().unwrap().iter() {
        let var_type = match &value.v {
            Value_::Global(mu_type) => {
                gen_llvm_type(llvm_internal_context, mu_type)
            }
            _ => panic!("Variable is not a global variable")
        };

        let var_name = format!("{}\0", value.hdr.name());

        let global = LLVMAddGlobal(
            llvm_internal_context.module,
            var_type,
            var_name.as_str().as_ptr() as *const c_char
        );

        llvm_internal_context.global_store.insert(*var, global);
    }
}
unsafe fn declare_whitelisted_functions(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    curr_fn: MuID
) {
    let whitelist = llvm_internal_context.whitelisted_fns.clone();
    for fn_id in whitelist.iter() {
        if curr_fn == *fn_id {
            continue;
        }

        let fn_name = format!(
            "{}\0",
            mu::ast::ir::mangle_name(
                llvm_internal_context.vm.get_name_for_func(*fn_id)
            )
        );
        let fn_sig = llvm_internal_context.vm.get_sig_for_func(*fn_id);
        let fn_type = create_llvm_function_type(llvm_internal_context, &fn_sig);

        // declare function in current module
        LLVMAddFunction(
            llvm_internal_context.module,
            fn_name.as_str().as_ptr() as *const c_char,
            fn_type
        );
    }
}

unsafe fn create_llvm_function_type(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    function_sig: &P<MuFuncSig>
) -> LLVMTypeRef {
    let mut llvm_arg_types = vec![];
    for arg_ty in &function_sig.arg_tys {
        llvm_arg_types.push(gen_llvm_type(llvm_internal_context, arg_ty));
    }

    let mut llvm_ret_types = vec![];
    for ret_ty in &function_sig.ret_tys {
        llvm_ret_types.push(gen_llvm_type(llvm_internal_context, ret_ty))
    }

    // Mu functions return a tuple of values
    let return_type;
    if llvm_ret_types.len() == 1 {
        return_type = *llvm_ret_types.get(0).unwrap();
    } else if llvm_ret_types.len() == 0 {
        return_type = LLVMVoidType();
    } else {
        // TODO Mu functions return a tuple of values
        println!("{:?}", function_sig);
        unimplemented!()
    }

    llvm_sys::core::LLVMFunctionType(
        return_type,
        llvm_arg_types.as_mut_ptr(),
        llvm_arg_types.len() as c_uint,
        false as LLVMBool
    )
}

unsafe fn process_block_params(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    block: &Block,
    create_phi_nodes: bool
) -> HashMap<MuID, LLVMValueRef> {
    let mut store = HashMap::new();

    if create_phi_nodes {
        let llvm_block = llvm_internal_context
            .blocks_map
            .get(&block.hdr.id())
            .unwrap();
        LLVMPositionBuilderAtEnd(llvm_internal_context.builder, *llvm_block);
        let args = block.content.as_ref().unwrap();
        let mut block_args = vec![];
        for i in 0..args.args.len() {
            let param_name = args.args.get(i).unwrap().hdr.name();
            let param_name = format!("{}\0", param_name);
            let param_id = args.args.get(i).unwrap().hdr.id();
            let param_type = gen_llvm_type(
                llvm_internal_context,
                &args.args.get(i).unwrap().ty
            );
            // each mu basic block parameter becomes a phi node
            let phi = LLVMBuildPhi(
                llvm_internal_context.builder,
                param_type,
                param_name.as_str().as_ptr() as *const c_char
            );
            store.insert(param_id, phi);
            block_args.push((param_type, phi));
        }
        let llvm_block = llvm_internal_context
            .blocks_map
            .get(&block.hdr.id())
            .unwrap();
        llvm_internal_context
            .block_args
            .insert(block.hdr.id(), (*llvm_block, block_args));
    } else {
        let block_params = llvm_internal_context
            .block_args
            .get(block.hdr.id().borrow());

        match block_params {
            Some((_, params)) => {
                let mu_params = &block.content.as_ref().unwrap().args;
                for ((_, llvm_param), mu_param) in params.iter().zip(mu_params)
                {
                    let mu_param_name = format!("{}\0", mu_param.hdr.name());
                    LLVMSetValueName(
                        *llvm_param,
                        mu_param_name.as_str().as_ptr() as *const c_char
                    );

                    store.insert(mu_param.hdr.id(), *llvm_param);
                }
            }
            None => return store
        }
    }

    store
}

pub unsafe fn gen_llvm_type(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    arg_ty: &P<MuType>
) -> LLVMTypeRef {
    match &arg_ty.v {
        MuType_::Double => LLVMDoubleType(),
        MuType_::Float => LLVMFloatType(),
        MuType_::Void => LLVMVoidType(),
        MuType_::Int(size) => match size {
            1 => LLVMInt1Type(),
            8 => LLVMInt8Type(),
            16 => LLVMInt16Type(),
            32 => LLVMInt32Type(),
            64 => LLVMInt64Type(),
            128 => LLVMInt128Type(),
            n => LLVMIntType(*n as c_uint)
        },
        MuType_::Struct(tag) => {
            let struct_map = STRUCT_TAG_MAP.read().unwrap();
            let struct_ty: &StructType_ = struct_map
                .get(&arg_ty.get_struct_hybrid_tag().unwrap())
                .unwrap();

            let struct_name = format!("{}\0", tag);

            if llvm_internal_context.struct_map.contains_key(tag) {
                let struct_type =
                    llvm_internal_context.struct_map.get(tag).unwrap();
                *struct_type
            } else {
                let context =
                    LLVMGetModuleContext(llvm_internal_context.module);
                let struct_type = LLVMStructCreateNamed(
                    context,
                    struct_name.as_str().as_ptr() as *const c_char
                );

                llvm_internal_context
                    .struct_map
                    .insert(tag.clone(), struct_type);

                let mut struct_types = vec![];
                for ty in struct_ty.get_tys() {
                    struct_types.push(gen_llvm_type(llvm_internal_context, ty));
                }

                LLVMStructSetBody(
                    struct_type,
                    struct_types.as_mut_ptr(),
                    struct_types.len() as u32,
                    false as LLVMBool
                );

                struct_type
            }
        }
        MuType_::Hybrid(_) => {
            let hybrid_map = HYBRID_TAG_MAP.read().unwrap();
            let hybrid_ty: &HybridType_ = hybrid_map
                .get(&arg_ty.get_struct_hybrid_tag().unwrap())
                .unwrap();

            if hybrid_ty.get_fix_tys().is_empty() {
                return LLVMArrayType(
                    gen_llvm_type(
                        llvm_internal_context,
                        hybrid_ty.get_var_ty()
                    ),
                    0
                );
            }

            let mut struct_types = vec![];
            for ty in hybrid_ty.get_fix_tys() {
                struct_types.push(gen_llvm_type(llvm_internal_context, ty));
            }

            let var_type = LLVMArrayType(
                gen_llvm_type(llvm_internal_context, hybrid_ty.get_var_ty()),
                0
            );

            struct_types.push(var_type);

            LLVMStructType(
                struct_types.as_mut_ptr(),
                struct_types.len() as u32,
                false as LLVMBool
            )
        }
        MuType_::Array(mu_type, size) => {
            let internal_type = gen_llvm_type(llvm_internal_context, mu_type);
            LLVMArrayType(internal_type, *size as u32)
        }
        MuType_::IRef(mu_type) => {
            if mu_type.is_void() {
                LLVMPointerType(LLVMInt8Type(), 1)
            } else {
                let internal_type =
                    gen_llvm_type(llvm_internal_context, mu_type);
                LLVMPointerType(internal_type, 1)
            }
        }
        MuType_::Ref(mu_type) => {
            if mu_type.is_void() {
                LLVMPointerType(LLVMInt8Type(), 1)
            } else {
                let internal_type =
                    gen_llvm_type(llvm_internal_context, mu_type);
                LLVMPointerType(internal_type, 1)
            }
        }
        MuType_::UPtr(mu_type) => {
            if mu_type.is_void() {
                LLVMPointerType(LLVMInt8Type(), 0)
            } else {
                let internal_type =
                    gen_llvm_type(llvm_internal_context, mu_type);
                LLVMPointerType(internal_type, 0)
            }
        }
        MuType_::FuncRef(sig) => LLVMPointerType(
            create_llvm_function_type(llvm_internal_context, sig),
            0
        ),
        _ => {
            println!("{:?}", arg_ty);
            unimplemented!()
        }
    }
}

unsafe fn gen_llvm_block(
    llvm_internal_context: &AOTLLVMInternalContext,
    function: LLVMValueRef,
    block: &Block
) -> LLVMBasicBlockRef {
    let llvm_block = LLVMAppendBasicBlockInContext(
        llvm_internal_context.context,
        function,
        CString::new(block.hdr.name().clone().as_str())
            .unwrap()
            .as_ptr()
    );

    llvm_block
}

unsafe fn gen_llvm_exception_block_contents(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    label: &usize,
    block: &Block,
    store: &mut HashMap<MuID, LLVMValueRef>,
    function: LLVMValueRef
) -> HashMap<MuID, Vec<(LLVMBasicBlockRef, Vec<(LLVMTypeRef, LLVMValueRef)>)>> {
    let llvm_block = llvm_internal_context.blocks_map.get(label).unwrap();
    LLVMPositionBuilderAtEnd(llvm_internal_context.builder, *llvm_block);

    // TODO how to handle exception arguments?
    let instructions = block.content.as_ref().unwrap();
    let mut block_arguments = HashMap::new();

    // add landing pad LLVM instruction
    let personality_function = LLVMGetNamedFunction(
        llvm_internal_context.module,
        "__gxx_personality_v0\0".as_ptr() as *const c_char
    );

    if personality_function.is_null() {
        unimplemented!()
    }

    LLVMSetPersonalityFn(function, personality_function);

    // Landing Pad default for C++ according to personality function
    let mut cpp_exception_type =
        vec![LLVMPointerType(LLVMInt8Type(), 0), LLVMInt32Type()];
    let cpp_exception_type =
        LLVMStructType(cpp_exception_type.as_mut_ptr(), 2, false as i32);

    let landing_pad = LLVMBuildLandingPad(
        llvm_internal_context.builder,
        cpp_exception_type,
        personality_function,
        1,
        "res\0".as_ptr() as *const c_char
    );

    LLVMAddClause(
        landing_pad,
        LLVMConstNull(LLVMPointerType(LLVMInt8Type(), 0))
    );
    match &block.content.as_ref().unwrap().exn_arg {
        Some(exc) => {
            let exc_llvm = LLVMBuildExtractValue(
                llvm_internal_context.builder,
                landing_pad,
                0,
                "exc\0".as_ptr() as *const c_char
            );
            let exc_type = gen_llvm_type(llvm_internal_context, &exc.ty);
            let exc_name = format!("{}\0", exc.hdr.name());
            let catch_fn = LLVMGetNamedFunction(
                llvm_internal_context.module,
                "__cxa_begin_catch\0".as_ptr() as *const c_char
            );
            let mut args = vec![exc_llvm];
            let exc_caught = LLVMBuildCall(
                llvm_internal_context.builder,
                catch_fn,
                args.as_mut_ptr(),
                1,
                "catch_call\0".as_ptr() as *const c_char
            );
            let exc_caught_ptr = LLVMBuildBitCast(
                llvm_internal_context.builder,
                exc_caught,
                LLVMPointerType(exc_type, 0),
                "exc_ptr_ptr\0".as_ptr() as *const c_char
            );

            let exc_caught_loaded = LLVMBuildLoad(
                llvm_internal_context.builder,
                exc_caught_ptr,
                exc_name.as_str().as_ptr() as *const c_char
            );

            store.insert(exc.hdr.id(), exc_caught_loaded);
        }
        None => ()
    }

    for instruction in &instructions.body {
        let mut args_inst = gen_llvm_instruction(
            llvm_internal_context,
            &instruction,
            store,
            label,
            &mut block_arguments
        );

        let llvm_block = llvm_internal_context.blocks_map.get(label).unwrap();
        for (key, value) in args_inst.drain() {
            if block_arguments.contains_key(&key) {
                let curr_values = block_arguments.get_mut(&key).unwrap();
                curr_values.push((*llvm_block, value));
            } else {
                let values = vec![(*llvm_block, value)];
                block_arguments.insert(key, values);
            }
        }
    }

    block_arguments
}

unsafe fn gen_llvm_block_contents(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    label: &usize,
    block: &Block,
    store: &mut HashMap<MuID, LLVMValueRef>
) -> HashMap<MuID, Vec<(LLVMBasicBlockRef, Vec<(LLVMTypeRef, LLVMValueRef)>)>> {
    let llvm_block = llvm_internal_context.blocks_map.get(label).unwrap();
    LLVMPositionBuilderAtEnd(llvm_internal_context.builder, *llvm_block);

    let instructions = block.content.as_ref().unwrap();
    let mut block_arguments = HashMap::new();

    for instruction in &instructions.body {
        let mut args_inst = gen_llvm_instruction(
            llvm_internal_context,
            &instruction,
            store,
            label,
            &mut block_arguments
        );

        let llvm_block = llvm_internal_context.blocks_map.get(label).unwrap();
        for (key, value) in args_inst.drain() {
            if block_arguments.contains_key(&key) {
                let curr_values = block_arguments.get_mut(&key).unwrap();
                curr_values.push((*llvm_block, value));
            } else {
                let values = vec![(*llvm_block, value)];
                block_arguments.insert(key, values);
            }
        }
    }

    block_arguments
}

unsafe fn gen_llvm_instruction(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    instruction: &P<TreeNode>,
    store: &mut HashMap<MuID, LLVMValueRef>,
    label: &usize,
    mut block_arguments: &mut HashMap<
        MuID,
        Vec<(LLVMBasicBlockRef, Vec<(LLVMTypeRef, LLVMValueRef)>)>
    >
) -> HashMap<MuID, Vec<(LLVMTypeRef, LLVMValueRef)>> {
    let mut ins_args: HashMap<MuID, Vec<(LLVMTypeRef, LLVMValueRef)>> =
        HashMap::new();

    let value = &instruction.v;
    info!("Processing instruction {:?}", value);

    match value {
        TreeNode_::Value(_) => unimplemented!(),
        TreeNode_::Instruction(ins) => match &ins.v {
            mu::ast::inst::Instruction_::Branch1(dest) => {
                gen_instr_branch1(
                    llvm_internal_context,
                    store,
                    &mut ins_args,
                    dest,
                    ins
                );
            }
            mu::ast::inst::Instruction_::Branch2 {
                cond,
                true_dest,
                false_dest,
                true_prob
            } => {
                gen_instr_branch2(
                    llvm_internal_context,
                    store,
                    &mut ins_args,
                    cond,
                    true_dest,
                    false_dest,
                    true_prob,
                    ins
                );
            }
            mu::ast::inst::Instruction_::Return(idx) => {
                gen_instr_return(llvm_internal_context, store, idx, ins);
            }
            mu::ast::inst::Instruction_::BinOp(bop, op1, op2) => {
                gen_instr_binop(
                    llvm_internal_context,
                    store,
                    ins,
                    bop,
                    op1,
                    op2
                );
            }
            mu::ast::inst::Instruction_::CmpOp(cop, op1, op2) => {
                gen_instr_cmpop(
                    llvm_internal_context,
                    store,
                    ins,
                    cop,
                    op1,
                    op2
                );
            }
            mu::ast::inst::Instruction_::Call { data, resume } => {
                gen_instr_call(
                    llvm_internal_context,
                    store,
                    &mut ins_args,
                    ins,
                    data,
                    resume,
                    false
                );
            }
            mu::ast::inst::Instruction_::ExprCall { data, is_abort } => {
                gen_instr_exprcall(
                    llvm_internal_context,
                    store,
                    ins,
                    data,
                    is_abort,
                    false
                );
            }
            mu::ast::inst::Instruction_::ExprCCall { data, is_abort } => {
                gen_instr_exprccall(
                    llvm_internal_context,
                    store,
                    ins,
                    data,
                    is_abort
                );
            }
            mu::ast::inst::Instruction_::New(mu_type) => {
                let llvm_type = gen_llvm_type(llvm_internal_context, mu_type);
                gen_instr_new(
                    llvm_internal_context,
                    store,
                    ins,
                    llvm_type,
                    mu_type
                );
                LLVMSetGC(
                    llvm_internal_context.curr_fn.unwrap(),
                    "statepoint-example\0".as_ptr() as *const c_char
                );
            }
            mu::ast::inst::Instruction_::AllocA(mu_type) => {
                let llvm_type = gen_llvm_type(llvm_internal_context, mu_type);
                gen_instr_alloca(llvm_internal_context, store, ins, llvm_type);
            }
            mu::ast::inst::Instruction_::GetVarPartIRef { is_ptr, base } => {
                gen_instr_get_varpart_iref(
                    llvm_internal_context,
                    store,
                    ins,
                    base,
                    is_ptr
                );
            }
            mu::ast::inst::Instruction_::GetIRef(op) => {
                gen_instr_get_iref(llvm_internal_context, store, ins, op);
            }
            mu::ast::inst::Instruction_::GetFieldIRef {
                is_ptr,
                base,
                index
            } => {
                gen_instr_get_field_iref(
                    llvm_internal_context,
                    store,
                    ins,
                    base,
                    index,
                    *is_ptr
                );
            }
            mu::ast::inst::Instruction_::GetElementIRef {
                is_ptr,
                base,
                index
            } => {
                gen_instr_get_element_iref(
                    llvm_internal_context,
                    store,
                    ins,
                    base,
                    index,
                    *is_ptr
                );
            }
            mu::ast::inst::Instruction_::ShiftIRef {
                is_ptr,
                base,
                offset
            } => {
                gen_instr_shift_iref(
                    llvm_internal_context,
                    store,
                    ins,
                    base,
                    offset,
                    *is_ptr
                );
            }
            mu::ast::inst::Instruction_::Store {
                is_ptr,
                order,
                mem_loc,
                value
            } => {
                gen_instr_store(
                    llvm_internal_context,
                    store,
                    ins,
                    mem_loc,
                    value,
                    *is_ptr,
                    *order
                );
            }
            mu::ast::inst::Instruction_::Load {
                is_ptr,
                order,
                mem_loc
            } => {
                gen_instr_load(
                    llvm_internal_context,
                    store,
                    ins,
                    mem_loc,
                    *is_ptr,
                    *order
                );
            }
            mu::ast::inst::Instruction_::Select {
                cond,
                true_val,
                false_val
            } => {
                gen_instr_select(
                    llvm_internal_context,
                    store,
                    ins,
                    cond,
                    true_val,
                    false_val
                );
            }
            mu::ast::inst::Instruction_::SetRetval(ret_val) => {
                gen_instr_set_ret_val(
                    llvm_internal_context,
                    store,
                    ins,
                    ret_val
                );
            }
            mu::ast::inst::Instruction_::ThreadExit => {
                gen_instr_thread_exit(llvm_internal_context, store, ins);
            }
            mu::ast::inst::Instruction_::NewThread {
                stack,
                thread_local,
                is_exception,
                args
            } => {
                gen_instr_new_thread(
                    llvm_internal_context,
                    store,
                    ins,
                    stack,
                    thread_local,
                    is_exception,
                    args
                );
            }
            mu::ast::inst::Instruction_::BinOpWithStatus(
                bop,
                bop_status,
                op1,
                op2
            ) => {
                gen_instr_binop_with_status(
                    llvm_internal_context,
                    store,
                    ins,
                    bop,
                    bop_status,
                    op1,
                    op2
                );
            }
            mu::ast::inst::Instruction_::ConvOp {
                operation,
                from_ty,
                to_ty,
                operand
            } => {
                gen_instr_convop(
                    llvm_internal_context,
                    store,
                    ins,
                    operation,
                    from_ty,
                    to_ty,
                    operand
                );
            }
            mu::ast::inst::Instruction_::Switch {
                cond,
                default,
                branches
            } => {
                gen_instr_switch(
                    llvm_internal_context,
                    store,
                    label,
                    &mut block_arguments,
                    &mut ins_args,
                    ins,
                    cond,
                    default,
                    branches
                );
            }
            mu::ast::inst::Instruction_::NewHybrid(mu_type, length) => {
                gen_instr_new_hybrid(
                    llvm_internal_context,
                    store,
                    ins,
                    mu_type,
                    length
                );
            }
            mu::ast::inst::Instruction_::Move(op) => {
                gen_instr_move(llvm_internal_context, store, ins, op);
            }
            mu::ast::inst::Instruction_::PrintHex(op) => {
                gen_instr_printhex(llvm_internal_context, store, ins, op);
            }
            mu::ast::inst::Instruction_::Throw(op) => {
                gen_instr_throw(llvm_internal_context, store, ins, op);
            }
            mu::ast::inst::Instruction_::NewStack(op) => {
                gen_instr_new_stack(llvm_internal_context, store, ins, op);
            }
            mu::ast::inst::Instruction_::CommonInst_GetThreadLocal => {
                gen_instr_get_thread_local(llvm_internal_context, store, ins);
            }
            mu::ast::inst::Instruction_::CommonInst_SetThreadLocal(op) => {
                //FIXME implement set thread local
                //
                // gen_instr_get_thread_local(llvm_internal_context, store,
                // ins);
            }
            // FIXME pinning and unpinning are not available without an
            // appropriate GC
            mu::ast::inst::Instruction_::CommonInst_Unpin(op) => {}
            mu::ast::inst::Instruction_::CommonInst_Pin(op) => {
                let arg = ins.ops.get(*op).unwrap().clone();
                let llvm_val =
                    gen_llvm_value(llvm_internal_context, &arg, store);

                let ins_value = ins.value.clone().unwrap();
                let mut name = String::from("\0");
                let mut id = 0;
                if ins_value.len() > 1 {
                    panic!("Unsupported operation.");
                } else if ins_value.len() == 1 {
                    id = ins_value.get(0).unwrap().hdr.id();
                    name =
                        format!("{}\0", ins_value.get(0).unwrap().hdr.name());
                }

                let llvm_val_ty = LLVMTypeOf(llvm_val);
                let elem_ty = LLVMGetElementType(llvm_val_ty);

                let addrspace_cast = LLVMBuildAddrSpaceCast(
                    llvm_internal_context.builder,
                    llvm_val,
                    LLVMPointerType(elem_ty, 0),
                    name.as_str().as_ptr() as *const c_char
                );

                store.insert(id, addrspace_cast);
            }
            _ => {
                println!("{:?}", ins);
                unimplemented!()
            }
        }
    }

    ins_args
}

pub unsafe fn gen_llvm_value(
    llvm_internal_context: &mut AOTLLVMInternalContext,
    mu_value: &P<TreeNode>,
    store: &mut HashMap<MuID, LLVMValueRef>
) -> LLVMValueRef {
    let v = match &mu_value.v {
        TreeNode_::Value(value) => {
            info!("operand value {:?}", value.v);
            match &value.v {
                Value_::Constant(c) => match c {
                    Constant::Float(num) => {
                        return LLVMConstReal(
                            LLVMFloatType(),
                            *num as c_double
                        );
                    }
                    Constant::Double(num) => {
                        return LLVMConstReal(
                            LLVMDoubleType(),
                            *num as c_double
                        );
                    }
                    Constant::Int(num) => {
                        // treat values that are <uptr<void>>0
                        match &value.ty.v {
                            MuType_::Int(_) => {
                                let llvm_type =
                                    gen_llvm_type(llvm_internal_context, &value.ty);

                                return LLVMConstInt(
                                    llvm_type,
                                    *num as c_ulong,
                                    false as LLVMBool
                                );
                            }
                            MuType_::UPtr(_) => {
                                let llvm_type =
                                    gen_llvm_type(llvm_internal_context, &value.ty);

                                assert_eq!(*num, 0);
                                return LLVMConstNull(llvm_type);
                            }
                            _ => panic!("Cannot automatically convert other values from integers")
                        }
                    }
                    Constant::FuncRef(f) => {
                        let name = format!("{}\0", mangle_name(f.name()));

                        let function = LLVMGetNamedFunction(
                            llvm_internal_context.module,
                            name.as_str().as_ptr() as *const c_char
                        );

                        if function.is_null() {
                            panic!("Could not find function.");
                        }

                        return function;
                    }
                    Constant::IntEx(v) => {
                        let mut res = format!("");
                        // Stored in little-endian order, but we need to display
                        // it in big-endian order
                        for i in 1..v.len() + 1 {
                            res.push_str(
                                format!("{:016X}", v[v.len() - i])
                                    .to_string()
                                    .as_str()
                            );
                        }
                        let number = format!("0x{}\0", res);

                        match value.ty.v {
                            MuType_::Int(_) => {
                                let value_type = gen_llvm_type(
                                    llvm_internal_context,
                                    &value.ty
                                );
                                LLVMConstIntOfString(
                                    value_type,
                                    number.as_str().as_ptr() as *const c_char,
                                    16
                                )
                            }
                            _ => panic!("Expected Integer type.")
                        }
                    }
                    Constant::NullRef => {
                        let llvm_type =
                            gen_llvm_type(llvm_internal_context, &value.ty);

                        LLVMConstNull(llvm_type)
                    }
                    _ => {
                        println!("{:?}", c);
                        unimplemented!()
                    }
                },
                Value_::SSAVar(var) => match store.get(&var) {
                    Some(var) => *var,
                    None => panic!(
                        "Reference to undefined variable\n{:?}.",
                        mu_value
                    )
                },
                Value_::Global(_) => {
                    match llvm_internal_context
                        .global_store
                        .get(&value.hdr.id())
                    {
                        Some(var) => *var,
                        None => panic!("Reference to undefined variable.")
                    }
                }
                _ => {
                    println!("{:?}", value.v);
                    unimplemented!()
                }
            }
        }
        TreeNode_::Instruction(_) => unimplemented!()
    };

    v
}

unsafe fn generate_llvm_ir(
    llvm_internal_context: &AOTLLVMInternalContext,
    func: &mut MuFunctionVersion,
    vm: &VM
) {
    let function_name = vm.get_name_for_func(func.func_id);

    // create 'emit' directory
    mu::vm::uir_output::create_emit_directory(vm);

    let function_name = format!("{}.ll", function_name);

    // create emit file
    let mut file_path = path::PathBuf::new();
    file_path.push(&vm.vm_options.flag_aot_emit_dir);
    file_path.push(&function_name);

    let path = file_path.as_path().to_str().unwrap();

    let name = CString::new(path).unwrap();

    llvm_sys::core::LLVMPrintModuleToFile(
        llvm_internal_context.module,
        name.as_ptr(),
        ptr::null_mut()
    );

    //        optimize_ir(llvm_internal_context, func, vm);
}

unsafe fn optimize_ir(
    llvm_internal_context: &AOTLLVMInternalContext,
    func: &mut MuFunctionVersion,
    vm: &VM
) {
    use crate::aot_mu_ext::exec_cmd;

    let function_name = vm.get_name_for_func(func.func_id);

    // create 'emit' directory
    mu::vm::uir_output::create_emit_directory(vm);

    let input = format!("{}.ll", function_name);
    let mut input_path = path::PathBuf::new();
    input_path.push(&vm.vm_options.flag_aot_emit_dir);
    input_path.push(&input);

    let input_path = input_path.as_path().to_str().unwrap();

    let function_name = format!("{}.ll", function_name);

    // create emit file
    let mut file_path = path::PathBuf::new();
    file_path.push(&vm.vm_options.flag_aot_emit_dir);
    file_path.push(&function_name);

    let path = file_path.as_path().to_str().unwrap();

    let output_command = format!("-o={}", path);

    let mut llc = Command::new(get_llvm_optimizer());

    // print as IR
    llc.arg("-S");

    llc.arg(output_command);

    llc.arg(input_path);

    println!("{:?}", llc);

    exec_cmd(llc);
}

/* FIXME for some reason, I'm getting an error
`LLVM ERROR: unsupported GC: statepoint-example (did you remember to link and initialize the CodeGen library?)`
 when generating the ASM with MCJIT. For that reason I'm just going to call llc explicitly */

unsafe fn generate_llvm_asm(func: &mut MuFunctionVersion, vm: &VM) {
    use crate::aot_mu_ext::exec_cmd;

    let function_name = vm.get_name_for_func(func.func_id);

    // create 'emit' directory
    mu::vm::uir_output::create_emit_directory(vm);

    let input = format!("{}.ll", function_name);
    let mut input_path = path::PathBuf::new();
    input_path.push(&vm.vm_options.flag_aot_emit_dir);
    input_path.push(&input);

    let input_path = input_path.as_path().to_str().unwrap();

    let function_name = format!("{}_llvm.S", function_name);

    // create emit file
    let mut file_path = path::PathBuf::new();
    file_path.push(&vm.vm_options.flag_aot_emit_dir);
    file_path.push(&function_name);

    let path = file_path.as_path().to_str().unwrap();

    let output_command = format!("-o={}", path);

    let mut llc = Command::new(get_llvm_compiler());

    // position independent code
    llc.arg("-relocation-model=pic");

    // disable frame pointer elimination
    //    llc.arg("-disable-fp-elim");

    // disable frame pointer elimination
    llc.arg("-frame-pointer=all");

    // emit functions into separate sections
    llc.arg("-function-sections");

    // emit data into separate sections
    llc.arg("-data-sections");

    llc.arg("-filetype=asm");

    llc.arg(output_command);

    llc.arg(input_path);

    println!("{:?}", llc);

    exec_cmd(llc);
}

fn get_llvm_compiler() -> String {
    use std::env;

    match env::var("LLVM_LLC") {
        Ok(val) => val,
        Err(_) => "llc".to_string()
    }
}

fn get_llvm_optimizer() -> String {
    use std::env;

    match env::var("OPT") {
        Ok(val) => val,
        Err(_) => "opt".to_string()
    }
}

//unsafe fn generate_llvm_asm(
//    llvm_internal_context: &AOTLLVMInternalContext,
//    func: &mut MuFunctionVersion,
//    vm: &VM
//) {
//    let mut ee = 0 as LLVMExecutionEngineRef;
//    let mut error = 0 as *mut c_char;
//    let mut options = std::mem::MaybeUninit::uninit().assume_init();
// //std::mem::uninitialized();
//
//    let module_provider =
// LLVMCreateModuleProviderForExistingModule(llvm_internal_context.module);
//    let func_pass_manager = LLVMCreateFunctionPassManager(module_provider);
//
//    if LLVMVerifyModule(
//        llvm_internal_context.module,
//        LLVMAbortProcessAction,
//        &mut error
//    ) > 0
//    {
//        let message = LLVMCreateMessage(error);
//        let message = CString::from_raw(message);
//        println!("{:?}", message);
//        unimplemented!();
//    }
//
//    LLVMRunFunctionPassManager(func_pass_manager,
// llvm_internal_context.curr_fn.unwrap());
//
//
//    LLVMInitializeMCJITCompilerOptions(
//        &mut options,
//        std::mem::size_of::<LLVMMCJITCompilerOptions>()
//    );
//
//
//    LLVMLinkInMCJIT();
//    LLVM_InitializeNativeTarget();
//    LLVM_InitializeNativeAsmPrinter();
//    LLVM_InitializeNativeTarget();
//
//    let llvm_registry = LLVMGetGlobalPassRegistry();
//    LLVMInitializeCore(llvm_registry);
//    LLVMInitializeScalarOpts(llvm_registry);
//    LLVMInitializeCodeGen(llvm_registry);
//
//
//    if LLVMCreateJITCompilerForModule(
//        &mut ee,
//        llvm_internal_context.module,
//        options.OptLevel,
//        &mut error
//    ) > 0
//    {
//        let message = LLVMCreateMessage(error);
//        let message = CString::from_raw(message);
//        println!("{:?}", message);
//        unimplemented!();
//    }
//
//    // TODO get CPU from system / rust
//    let triple = LLVMGetDefaultTargetTriple();
//    let target = LLVMGetFirstTarget();
//    let cpu = CString::new("x86-64").expect("invalid cpu");
//    let feature = CString::new("").expect("invalid feature");
//    let opt_level = LLVMCodeGenOptLevel::LLVMCodeGenLevelNone;
//    let reloc_mode = LLVMRelocMode::LLVMRelocPIC;
//    let code_model = LLVMCodeModel::LLVMCodeModelDefault;
//    let target_machine = LLVMCreateTargetMachine(
//        target,
//        triple,
//        cpu.as_ptr(),
//        feature.as_ptr(),
//        opt_level,
//        reloc_mode,
//        code_model
//    );
//    let file_type = LLVMCodeGenFileType::LLVMAssemblyFile;
//
//    let mut error_str = ptr::null_mut();
//
//    let function_name = vm.get_name_for_func(func.func_id);
//
//    // create 'emit' directory
//    mu::vm::uir_output::create_emit_directory(vm);
//
//    let function_name = format!("{}_llvm.S", function_name);
//
//    // create emit file
//    let mut file_path = path::PathBuf::new();
//    file_path.push(&vm.vm_options.flag_aot_emit_dir);
//    file_path.push(&function_name);
//
//    let path = file_path.as_path().to_str().unwrap();
//
//    let output_file = CString::new(path).unwrap();
//
//
//    let res = LLVMTargetMachineEmitToFile(
//        target_machine,
//        llvm_internal_context.module,
//        output_file.as_ptr() as *mut i8,
//        file_type,
//        &mut error_str
//    );
//
//    if res == 1 {
//        let message = LLVMCreateMessage(error_str);
//        let message = CString::from_raw(message);
//        let error = format!("{:?}", message);
//        panic!(error);
//    }
//}
