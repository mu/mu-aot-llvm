//
// Created by Luis Eduardo de Souza Amorim on 1/4/20.
//

#ifndef _TESTLIB_H
#define _TESTLIB_H

#ifdef __cplusplus
extern "C"{
#endif

void compile_to_llvm_sharedlib(void*, void*, void*, void*);
void compile_to_llvm_sharedlib_given_context(void*, void*, void*, void*, void*, void*);


#ifdef __cplusplus
}
#endif
#endif