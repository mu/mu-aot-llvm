# LLVM Zebu (an AOT compiler for Mu/Zebu using LLVM)

For more information about Zebu, click [here](https://gitlab.anu.edu.au/mu/mu-impl-fast).

For more information about Mu, click [here](http://microvm.github.io). 

For more information about LLVM, click [here](https://llvm.org/docs/). 