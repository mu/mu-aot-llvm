if test $# -lt 1  || test $# -gt 3; then
	echo "Usage: ./init_test_env.sh <release|debug> [zebu-dir]"
	echo "   Where zebu-dir defaults to $PWD"
else
	export ZEBU_BUILD=$1

	cd

	if [ ! -d ~/mu-impl-fast ]; then
		git clone -b rust-2018 https://gitlab.anu.edu.au/mu/mu-impl-fast.git
		cd mu-impl-fast
		sed -i 's/https:\/\/github.com\/udesou\/mmtk-core.git/https:\/\/udesou:'"$CI_ACCESS_TOKEN"'@github.com\/udesou\/mmtk-core.git/g' src/mmtk-gc/Cargo.toml
	else
		cd mu-impl-fast
		git checkout -f rust-2018
	fi

	export MU_ZEBU=$PWD

	cd ~/mu-aot-llvm

	export MU_LLVM_ZEBU=$PWD

	export CI_HOME=~/mu-ci # Where to put all the depencies
	mkdir -p $CI_HOME/llvm # Put all dependencies specific to LLVM (eg mu-client-pypy)
	mkdir -p $CI_HOME/llvm/results # Where performance results will go
	export RUSTUP_HOME=$CI_HOME/.rustup # rust will use this instead of ~/.rustup
	export CARGO_HOME=$CI_HOME/.cargo # rust will use this instead of ~/.cargo
	export LLVM_SYS_100_PREFIX=$CI_HOME/llvm/my-llvm-build # set llvm env variables
	export OPT=$CI_HOME/llvm/my-llvm-build/bin/opt
	export LLVM_LINK=$CI_HOME/llvm/my-llvm-build/bin/llvm-link
	export LLC=$CI_HOME/llvm/my-llvm-build/bin/llc
	export GC_FEATURE=gencopy

	# download llvm benchmarks if they are not there yet
	if [ ! -d $CI_HOME/llvm/mu-perf-benchmarks ]; then
		cd $CI_HOME/llvm
		git clone https://gitlab.anu.edu.au/mu/mu-perf-benchmarks.git -b mu-aot-llvm
	else
		cd $CI_HOME/llvm/mu-perf-benchmarks
		git checkout -f mu-aot-llvm
		git pull
	fi

	cd $MU_LLVM_ZEBU

	# Install rustup if we haven't yet
	if [ ! -f $CARGO_HOME/bin/rustup ]; then curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh -s -- -y --default-toolchain none; fi
	source $CARGO_HOME/env  # Set up $PATH appropriately

	export LD_LIBRARY_PATH=$(echo $LD_LIBRARY_PATH | sed 's!/mu-impl-fast/target/[^/]*/deps/!/REMOVED/!g') # Delete any existing zebu's (so they don't accidently get used)
	export LD_LIBRARY_PATH=./emit:$MU_ZEBU/target/$ZEBU_BUILD/deps/:$MU_LLVM_ZEBU/target/$ZEBU_BUILD/deps/:$CI_HOME/antlr4/usr/local/lib:$LD_LIBRARY_PATH

	export CC=clang
	export CXX=clang++
	export RUST_TEST_THREADS=1
	export RUST_BACKTRACE=1

	export PYTHONPATH=$CI_HOME/aot-mu/mu-client-pypy:$CI_HOME/RPySOM/src
	export RPYSOM=$CI_HOME/RPySOM
	export PYPY=$CI_HOME/aot-mu/mu-client-pypy
	export MUC=$CI_HOME/mu-tool-compiler/muc

	if [ ! -d $CI_HOME/venv2 ]; then
		virtualenv -p python2 $CI_HOME/venv2
		source $CI_HOME/venv2/bin/activate
		pip install pytest
	fi

	source $CI_HOME/venv2/bin/activate
	if [ ! -d $CI_HOME/venv3 ]; then virtualenv -p python3 $CI_HOME/venv3; fi
fi
